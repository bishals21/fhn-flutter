import 'package:fhn_flutter/model/imagingDetailItem.dart';
import 'package:flutter/material.dart';

class ImageViewerPage extends StatefulWidget {
  final List<ImageAttachment> list;
  final int position;

  ImageViewerPage({Key key, @required this.list, @required this.position})
      : super(key: key);

  @override
  _ImageViewerPageState createState() => _ImageViewerPageState();
}

class _ImageViewerPageState extends State<ImageViewerPage> {
  PageController _pageController;

  @override
  void initState() {
    _pageController = new PageController(initialPage: widget.position);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.black,
      appBar: new AppBar(
        backgroundColor: Colors.black,
      ),
      body: new PageView.builder(
        controller: _pageController,
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          return new Center(
            child: new Hero(
                tag: "imaging" +
                    widget.list[index].id.toString() +
                    index.toString(),
                child: new Image.network(widget.list[index].image)),
          );
        },
      ),
    );
  }
}
