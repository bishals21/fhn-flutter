import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/galleryImagePage.dart';
import 'package:fhn_flutter/galleryVideoPage.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/galleryImageItem.dart';
import 'package:fhn_flutter/model/galleryVideoItem.dart';
import 'package:flutter/material.dart';
import 'database.dart';

import 'package:http/http.dart' as http;

class GalleryPage extends StatefulWidget {
  final String accessToken;
  GalleryPage({Key key, @required this.accessToken}) : super(key: key);
  @override
  _GalleryPageState createState() => _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage> {
  var db = DatabaseHelper();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<GalleryImageItem> galleryImageItemList = new List();
  List<GalleryVideoItem> galleryVideoItemList = new List();

  Future<Null> fetchData() async {
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get("http://familyhealthnepal.com/api/gallery?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          parsePhotos(responseBody["photos"]);
          parseVideos(responseBody["videos"]);
        }
      });
    } else {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(AppLocalization.of(context).noInternetConnection),
      ));
    }
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  parsePhotos(var photos) {
    List<GalleryImageItem> imageItem = new List();
    for (var item in photos) {
      List<GalleryImages> imageList = new List();
      String albumName = item["album_name"];
      for (var image in item["images"]) {
        imageList.add(new GalleryImages(image["id"], image["hospital"],
            image["date"], image["title"], image["image"]));
      }

      imageItem.add(new GalleryImageItem(albumName, imageList));
    }

    setState(() {
      galleryImageItemList = imageItem;
    });
  }

  parseVideos(var videos) {
    List<GalleryVideoItem> videoItem = new List();
    for (var item in videos) {
      List<GalleryVideo> videoList = new List();
      String investigationDate = item["investigation_date"];
      String hospital = item["hospital "];
      String name = item["name"];
      String description = item["description"];
      for (var video in item["videos"]) {
        videoList.add(
            new GalleryVideo(video["video"], video["title"], video["thumb"]));
      }

      videoItem.add(new GalleryVideoItem(
          investigationDate, hospital, name, description, videoList));
    }

    setState(() {
      galleryVideoItemList = videoItem;
    });
  }

  @override
  void initState() {
    fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 2,
      child: new Scaffold(
        key: _scaffoldKey,
        body: Column(
          children: <Widget>[
            new GradientAppBarWithBack("Gallery"),
            new Container(
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                    colors: [const Color(0xFF9E30BF), const Color(0xFF262BA0)],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 0.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp),
              ),
              child: new TabBar(
                indicatorColor: Colors.white,
                tabs: <Widget>[
                  new Tab(
                    child: new Text("PHOTOS"),
                  ),
                  new Tab(
                    child: new Text("VIDEOS"),
                  ),
                ],
              ),
            ),
            new Expanded(
              child: new TabBarView(
                children: <Widget>[
                  galleryImageItemList.isNotEmpty
                      ? new GalleryImagePage(
                          imageItem: galleryImageItemList,
                        )
                      : new Center(
                          child: new CircularProgressIndicator(),
                        ),
                  galleryVideoItemList.isNotEmpty
                      ? new GalleryVideoPage(
                          videoItem: galleryVideoItemList,
                        )
                      : new Center(
                          child: new CircularProgressIndicator(),
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
