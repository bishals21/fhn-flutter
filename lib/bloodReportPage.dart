import 'dart:async';

import 'package:fhn_flutter/model/bloodPressureItem.dart';
import 'package:flutter/material.dart';
import 'database.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class BloodReportPage extends StatefulWidget {
  @override
  _BloodReportPageState createState() => _BloodReportPageState();
}

class _BloodReportPageState extends State<BloodReportPage> {
  var db = DatabaseHelper();
  List<BloodPressureItem> list = new List();

  List<charts.Series<LinearData, int>> dataSets = new List();

  List<LinearData> highSysList = new List();
  List<LinearData> lowSysList = new List();
  List<LinearData> pulseList = new List();

  Future getListFromDB() async {
    list = await db.getAllBloodPressure();

    for (int i = 0; i < list.length; i++) {
      // if (i == 0 || i == (list.length - 1)) {
      BloodPressureItem item = list[i];
      highSysList.add(new LinearData(item.id, int.parse(item.highSys)));
      lowSysList.add(new LinearData(item.id, int.parse(item.lowSys)));
      pulseList.add(new LinearData(item.id, int.parse(item.pulse)));
      //   } else {
      //     BloodPressureItem item = list[i];
      //     highSysList.add(new LinearData("", int.parse(item.highSys)));
      //     lowSysList.add(new LinearData("", int.parse(item.lowSys)));
      //     pulseList.add(new LinearData("", int.parse(item.pulse)));
      //   }
    }

    dataSets.add(new charts.Series<LinearData, int>(
      id: 'SYSTOLIC',
      colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
      domainFn: (LinearData dataItem, _) => dataItem.date,
      measureFn: (LinearData dataItem, _) => dataItem.value,
      data: highSysList,
    ));

    dataSets.add(new charts.Series<LinearData, int>(
      id: 'DIASTOLIC',
      colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
      domainFn: (LinearData dataItem, _) => dataItem.date,
      measureFn: (LinearData dataItem, _) => dataItem.value,
      data: lowSysList,
    ));

    dataSets.add(new charts.Series<LinearData, int>(
      id: 'PULSE',
      colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
      domainFn: (LinearData dataItem, _) => dataItem.date,
      measureFn: (LinearData dataItem, _) => dataItem.value,
      data: pulseList,
    ));

    setState(() {});
  }

  @override
  void initState() {
    getListFromDB();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return dataSets.isNotEmpty
        ? new Container(
            padding: new EdgeInsets.all(8.0),
            child: new charts.LineChart(
              dataSets,
              animate: true,
              defaultRenderer:
                  new charts.LineRendererConfig(includePoints: true),
              behaviors: [
                new charts.SeriesLegend(
                    position: charts.BehaviorPosition.bottom)
              ],
            ),
          )
        : new Center(
            child: new CircularProgressIndicator(),
          );
  }
}

class LinearData {
  final int date;
  final int value;

  LinearData(this.date, this.value);
}
