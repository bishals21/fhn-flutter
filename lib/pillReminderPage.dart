import 'dart:async';

import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:flutter/material.dart';

import 'database.dart';
import 'model/addPillModel.dart';
import 'model/pillReminderItem.dart';
import 'pillAddPage.dart';

class PillReminderPage extends StatefulWidget {
  @override
  _PillReminderPageState createState() => new _PillReminderPageState();
}

class _PillReminderPageState extends State<PillReminderPage> {
  var db = DatabaseHelper();
  List<PillReminderItem> list = new List();

  Future getListFromDB() async {
    list = await db.getAllPillReminder();
    this.setState(() {});
  }

  Future deleteFromDB(PillReminderItem item) async {
    await db.deletePillReminder(item.id);
    list.remove(item);
    this.setState(() {});
  }

  @override
  void initState() {
    getListFromDB();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Column(
        children: <Widget>[
          new GradientAppBarWithBack(
              AppLocalization.of(context).pillReminderTitle),
          new Expanded(
            child: new Container(
              child: list.isNotEmpty
                  ? new ListView(
                      padding: new EdgeInsets.all(8.0),
                      children: list.map((PillReminderItem item) {
                        return new Card(
                          child: new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new ListTile(
                                  title: new Text(item.medicineName),
                                  subtitle: new Text(item.medicineDose),
                                  trailing: new IconButton(
                                    icon: new Icon(Icons.delete),
                                    onPressed: () {
                                      deleteFromDB(item);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }).toList(),
                    )
                  : new Center(
                      child: new Image.asset(
                        "assets/empty.png",
                        height: 200.0,
                        width: 200.0,
                      ),
                    ),
            ),
          ),
        ],
      ),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        backgroundColor: Color(0xFF9E30BF),
        onPressed: () {
          _navigateToAddPill(context);
        },
      ),
    );
  }

  _navigateToAddPill(BuildContext context) async {
    final AddPillItem result = await Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new AddPillReminder(),
        fullscreenDialog: true,
      ),
    );

    if (result != null) {
      List<String> timeList = result.timeList;
      for (String item in timeList) {
        await db.savePillReminder(new PillReminderItem(
            medicineName: result.medicine_name,
            fromDate: result.from_date,
            toDate: result.to_date,
            medicineDose: result.medicine_dose,
            time: item));

        this.setState(() {
          list.add(new PillReminderItem(
              medicineName: result.medicine_name,
              fromDate: result.from_date,
              toDate: result.to_date,
              medicineDose: result.medicine_dose,
              time: item));
        });
      }
    }
  }
}
