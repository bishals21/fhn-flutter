import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/model/videoItem.dart';
import 'package:flutter/material.dart';

class VideoDetailPage extends StatefulWidget {
  final List<VideoAttachment> list;
  final String title;
  VideoDetailPage({Key key, @required this.title, @required this.list})
      : super(key: key);
  @override
  _VideoDetailPageState createState() => _VideoDetailPageState();
}

class _VideoDetailPageState extends State<VideoDetailPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Column(
        children: <Widget>[
          new GradientAppBarWithBack(widget.title),
          new Expanded(
            child: new Container(
              child: new GridView.builder(
                padding: new EdgeInsets.all(8.0),
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 8.0,
                  mainAxisSpacing: 8.0,
                ),
                itemCount: widget.list.length,
                itemBuilder: (context, index) {
                  return new GestureDetector(
                    onTap: () {},
                    child: new Container(
                      color: Colors.grey,
                      child: new Image.network(
                        widget.list[index].thumb,
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
