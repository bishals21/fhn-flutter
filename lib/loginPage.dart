import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/locale/locales.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'database.dart';
import 'model/userAccessToken.dart';
import 'package:flutter_html_view/flutter_html_view.dart';

class LoginData {
  String username = "";
  String password = "";
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var db = DatabaseHelper();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  LoginData loginData = new LoginData();
  bool _obscureText = true;

  String _validateUsername(String value) {
    if (value.length == 0) {
      return AppLocalization.of(context).usernameIsRequired;
    }

    return null;
  }

  String _validatePassword(String value) {
    if (value.length == 0) {
      return AppLocalization.of(context).passwordIsRequired;
    }

    return null;
  }

  Future getUserAccessToken() async {
    UserAccessToken userAccessToken = await db.getUserAccessToken();
    if (userAccessToken != null) {
      print(userAccessToken.accessToken);
    }
  }

  void submit() {
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save();
      postLogin();
    }
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  postLogin() async {
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.post("http://familyhealthnepal.com/api/login", body: {
        "username": "${loginData.username}",
        "password": "${loginData.password}",
      }, headers: {
        "api-key": "testapikey"
      }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          if (responseBody['status_code'] == 1000) {
            _showSnackBar(responseBody["message"]);
            var data = responseBody["data"];
            saveUser(new UserAccessToken(
                accessToken: data["accessToken"],
                fullName: data["fullname"],
                fhnId: data["fhnId"],
                image: data["image"]));
          } else {
            _showSnackBar(
                responseBody["message"] + " " + responseBody["description"]);
          }
        }
      });
    } else {
      _showSnackBar(AppLocalization.of(context).noInternetConnection);
    }
  }

  Future saveUser(UserAccessToken userAccessToken) async {
    await db.saveUser(userAccessToken);
    Navigator.pop(context, "LoggedIn");
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  void initState() {
    getUserAccessToken();
    super.initState();
  }

  String msg = "<html>" +
      "<body>" +
      "<p>" +
      "By accessing or using this app, you signify your assent to these terms and conditions. This application is the property of Family Health Nepal (FHN). Family Health Nepal (FHN) may revise and/or update these terms and conditions at any time. Your continued usage of Mobile Health Record app means that you accept such changes. The contents of this app, including without limitation, all data, information, text, graphics, links and other materials are provided as a convenience to our app users and are meant to be used for informational purposes only.\n</p>" +
      "<p>" +
      "We include entire medical history of our app users to facilitate them carry their medical record in their pocket wherever they go. The information provided by our app on the medicinal part are the medication and suggestions provided by the health professional to the user at the time of checkup, we are not liable for the update, edit, delete or misuse of those information. The user should renew their membership after five years other than that Mobile Health Record would not be liable to store the data of the users. We maintain privacy of all the medical record provided by our users in our app and even in our website providing our users with a unique user id and password, other than that we do not allow anyone to view the medical record of our users.\n</p>" +
      "<p>" +
      "Mobile Health Record is not liable towards the use and misuse of the medical record of any users. The user is solely responsible for the use and misuse of the medical record if they don’t maintain privacy towards their user id and password.\n</p>" +
      "<p>" +
      "Except as expressly provided by Mobile Health Record, none of the content may be copied,\n" +
      "reproduced, republished, downloaded, uploaded, posted, displayed, transmitted or\n" +
      "distributed in any way and nothing on the app shall be construed to confer any license\n" +
      "under any of Mobile Health Record’s  intellectual property rights, whether by estoppels,\n" +
      "implication or otherwise. Unless otherwise indicated, the trademarks, logos and service marks displayed on this app are the property of Mobile Health Record. Users are not permitted to use these marks without the prior written consent of Mobile Health Record. \n</p>" +
      "<p>" +
      "By using the app you agree to be legally bound by the above terms and conditions, which shall take effect immediately on your first use of the app.\n</p>" +
      "</body>" +
      "</html>";

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
        hintColor: Colors.white,
        primaryColor: Colors.white,
        errorColor: Colors.white,
        splashColor: Colors.white24,
        inputDecorationTheme: new InputDecorationTheme(
            labelStyle: TextStyle(
          color: Colors.white,
        )));
  }

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    return new Theme(
      data: buildTheme(),
      child: new Scaffold(
        key: _scaffoldKey,
        body: Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: AssetImage("assets/medicalImage.jpg"),
                    fit: BoxFit.cover),
              ),
              child: new Container(
                decoration: new BoxDecoration(
                  gradient: new LinearGradient(
                      colors: [
                        const Color(0xB39E30BF),
                        const Color(0xB3262BA0)
                      ],
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(1.0, 0.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: new Container(
                  margin: new EdgeInsets.only(top: 70.0),
                  child: new Form(
                    key: _formKey,
                    child: new ListView(
                      padding: new EdgeInsets.symmetric(
                        horizontal: 8.0,
                        vertical: 8.0,
                      ),
                      children: <Widget>[
                        new Padding(
                          padding: new EdgeInsets.all(16.0),
                          child: new Text(
                            "Electronic Patient Record",
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 22.0,
                            ),
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 8.0),
                          child: new TextFormField(
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                            decoration: new InputDecoration(
                              hintText:
                                  AppLocalization.of(context).enterUsername,
                              labelText:
                                  AppLocalization.of(context).usernameText,
                              hintStyle: new TextStyle(color: Colors.white),
                              border: new OutlineInputBorder(),
                            ),
                            validator: this._validateUsername,
                            onSaved: (String value) {
                              this.loginData.username = value;
                            },
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 24.0),
                          child: new TextFormField(
                            keyboardType: TextInputType.text,
                            obscureText: _obscureText,
                            style: new TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                            decoration: new InputDecoration(
                                hintText:
                                    AppLocalization.of(context).enterPassword,
                                labelText:
                                    AppLocalization.of(context).passwordText,
                                hintStyle: new TextStyle(color: Colors.white),
                                border: new OutlineInputBorder(),
                                suffixIcon: new GestureDetector(
                                  child: new Icon(
                                    _obscureText
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                    color: Colors.white,
                                  ),
                                  onTap: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                )),
                            validator: this._validatePassword,
                            onSaved: (String value) {
                              this.loginData.password = value;
                            },
                          ),
                        ),
                        // new Padding(
                        //   padding: new EdgeInsets.only(top: 24.0),
                        //   child: new ListTile(
                        //     title: new Text(
                        //       AppLocalization.of(context).signingText,
                        //       style: new TextStyle(
                        //         color: Colors.white,
                        //       ),
                        //     ),
                        //     onTap: () {
                        //       showModalBottomSheet(
                        //           context: context,
                        //           builder: (context) => SingleChildScrollView(
                        //                 child: Column(
                        //                   children: <Widget>[
                        //                     Padding(
                        //                       padding:
                        //                           const EdgeInsets.all(8.0),
                        //                       child: new Text(
                        //                         "Terms and Conditions",
                        //                         style: new TextStyle(
                        //                           color: Colors.black,
                        //                           fontSize: 22.0,
                        //                         ),
                        //                       ),
                        //                     ),
                        //                     new Container(
                        //                       child: new HtmlView(
                        //                         data: msg,
                        //                       ),
                        //                     ),
                        //                   ],
                        //                 ),
                        //               ));
                        //     },
                        //   ),
                        // ),
                        new Padding(
                            padding: new EdgeInsets.only(top: 24.0),
                            child: new MaterialButton(
                              height: 50.0,
                              onPressed: submit,
                              color: Color(0x80FFFFFF),
                              child: new Text(
                                AppLocalization.of(context).loginTitle,
                                style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                ),
                              ),
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            new Padding(
              padding: new EdgeInsets.only(top: statusBarHeight),
              child: new Row(
                children: <Widget>[
                  new IconButton(
                    color: Colors.white,
                    icon: new Icon(Icons.close),
                    onPressed: () => Navigator.maybePop(context),
                  ),
                  new Container(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0),
                    child: new Text(
                      AppLocalization.of(context).loginTitle,
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 20.0),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
