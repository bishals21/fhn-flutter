import 'package:fhn_flutter/galleryVideoDetailPage.dart';
import 'package:fhn_flutter/model/galleryVideoItem.dart';
import 'package:flutter/material.dart';

class GalleryVideoPage extends StatefulWidget {
  final List<GalleryVideoItem> videoItem;

  GalleryVideoPage({Key key, @required this.videoItem}) : super(key: key);
  @override
  _GalleryVideoPageState createState() => _GalleryVideoPageState();
}

class _GalleryVideoPageState extends State<GalleryVideoPage> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new GridView.builder(
        padding: new EdgeInsets.all(8.0),
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 8.0,
          mainAxisSpacing: 8.0,
        ),
        itemCount: widget.videoItem.length,
        itemBuilder: (context, index) {
          return new GestureDetector(
            onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                  builder: (context) => new GalleryVideoDetailPage(
                        name: widget.videoItem[index].name,
                        videoItem: widget.videoItem[index].galleryVideos,
                      ),
                )),
            child: new Container(
              child: new Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  new Container(
                    color: Colors.grey,
                  ),
                  new Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: new Container(
                        padding: new EdgeInsets.all(8.0),
                        color: Colors.black54,
                        child: new Text(
                          widget.videoItem[index].name,
                          textAlign: TextAlign.center,
                          style: new TextStyle(
                              color: Colors.white, fontSize: 15.0),
                        ),
                      ))
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
