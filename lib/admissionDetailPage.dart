import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/model/admissionDetailItem.dart';
import 'package:fhn_flutter/model/admissionDetailItemC.dart';
import 'package:flutter/material.dart';
import 'package:fhn_flutter/database.dart';
import 'package:http/http.dart' as http;

class AdmissionDetailPage extends StatefulWidget {
  final String from, accessToken;
  final int recordId;

  AdmissionDetailPage(
      {Key key,
      @required this.accessToken,
      @required this.from,
      @required this.recordId})
      : super(key: key);

  @override
  _AdmissionDetailPageState createState() => _AdmissionDetailPageState();
}

class _AdmissionDetailPageState extends State<AdmissionDetailPage> {
  var db = DatabaseHelper();
  AdmissionDetailItem admissionDetailItem;
  AdmissionDetailItemC admissionDetailItemC;

  Future<Null> _handleAdmissionDetailRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          "http://familyhealthnepal.com/api/admission/" +
              widget.recordId.toString() +
              "?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var item = responseBody["data"]["_embedded"]["item"];
          deleteAFromDB(widget.recordId);
          saveAToDB(new AdmissionDetailItem(
              recordId: widget.recordId,
              admissionDate: item["admission_date"],
              indexNumber: item["index_number"],
              ipNumber: item["ip_number"],
              ward: item["ward"],
              hospital: item["hospital"],
              chiefComplain: item["chief_complain"],
              history: item["history_of_present_illness"],
              vitals: item["vitals"],
              generalExamination: item["general_examination"],
              investigation: item["investigation"],
              provisionalDiagnosis: item["provisional_diagnosis"],
              plan: item["plan"],
              note: item["note"]));

          if (mounted) {
            this.setState(() {
              admissionDetailItem = new AdmissionDetailItem(
                  recordId: widget.recordId,
                  admissionDate: item["admission_date"],
                  indexNumber: item["index_number"],
                  ipNumber: item["ip_number"],
                  ward: item["ward"],
                  hospital: item["hospital"],
                  chiefComplain: item["chief_complain"],
                  history: item["history_of_present_illness"],
                  vitals: item["vitals"],
                  generalExamination: item["general_examination"],
                  investigation: item["investigation"],
                  provisionalDiagnosis: item["provisional_diagnosis"],
                  plan: item["plan"],
                  note: item["note"]);
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text("No Internet Connection"),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getAFromDB();
      return completer.future;
    }
  }

  Future<Null> _handleCheckupDetailRefresh() async {
    
    final Completer<Null> completer = new Completer<Null>();
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          "http://familyhealthnepal.com/api/checkup/" +
              widget.recordId.toString() +
              "?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var item = responseBody["data"]["_embedded"]["item"];
          deleteBFromDB(widget.recordId);
          saveCToDB(new AdmissionDetailItemC(
            recordId: widget.recordId,
            indexNumber: item["index_number"],
            followupNote: item["followup_note"],
            hospital: item["hospital"],
            department: item["department"],
            doctor: item["doctor"],
            history: item["patient_history"],
            advise: item["advise"],
            provisionalDiagnosis: item["provisional_diagnosis"],
            checkupDate: item["checkup_date"],
            followupDate: item["followup_date"],
          ));
          if (mounted) {
            this.setState(() {
              admissionDetailItemC = new AdmissionDetailItemC(
                recordId: widget.recordId,
                indexNumber: item["index_number"],
                followupNote: item["followup_note"],
                hospital: item["hospital"],
                department: item["department"],
                doctor: item["doctor"],
                history: item["patient_history"],
                advise: item["advise"],
                provisionalDiagnosis: item["provisional_diagnosis"],
                checkupDate: item["checkup_date"],
                followupDate: item["followup_date"],
              );
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text("No Internet Connection"),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getCFromDB();
      return completer.future;
    }
  }

  Future getAFromDB() async {
    admissionDetailItem = await db.getAdmissionDetailItemA(widget.recordId);
    this.setState(() {});
  }

  Future getCFromDB() async {
    admissionDetailItemC = await db.getAdmissionDetailItemC(widget.recordId);
    this.setState(() {});
  }

  Future saveAToDB(AdmissionDetailItem item) async {
    await db.saveAdmissionDetailItemA(item);
  }

  Future saveCToDB(AdmissionDetailItemC item) async {
    await db.saveAdmissionDetailItemC(item);
  }

  Future deleteAFromDB(int recordId) async {
    await db.deleteAdmissionDetailItemA(recordId);
  }

  Future deleteBFromDB(int recordId) async {
    await db.deleteAdmissionDetailItemC(recordId);
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    widget.from == "admission"
        ? _handleAdmissionDetailRefresh()
        : _handleCheckupDetailRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new RefreshIndicator(
      onRefresh: widget.from == "admission"
          ? _handleAdmissionDetailRefresh
          : _handleCheckupDetailRefresh,
      child: widget.from == "admission" && admissionDetailItem != null
          ? new ListView(
              children: <Widget>[
                new Card(
                    elevation: 5.0,
                    margin: new EdgeInsets.all(8.0),
                    child: new Container(
                      padding: new EdgeInsets.all(8.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Center(
                            child: new Text(
                              admissionDetailItem.hospital,
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                color: Colors.blueAccent,
                                fontSize: 22.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          new Center(
                            child: new Text(
                              admissionDetailItem.ward,
                              textAlign: TextAlign.center,
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 8.0),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                new RichText(
                                  text: new TextSpan(children: <TextSpan>[
                                    new TextSpan(
                                      text: "INDEX NUMBER: ",
                                      style: new TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    ),
                                    new TextSpan(
                                      text: admissionDetailItem.indexNumber,
                                      style: new TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black,
                                      ),
                                    )
                                  ]),
                                ),
                                new RichText(
                                  text: new TextSpan(children: <TextSpan>[
                                    new TextSpan(
                                      text: "IP NUMBER: ",
                                      style: new TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    ),
                                    new TextSpan(
                                      text: admissionDetailItem.ipNumber,
                                      style: new TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black,
                                      ),
                                    )
                                  ]),
                                ),
                              ],
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 8.0),
                            child: new Divider(
                              height: 10.0,
                              color: Colors.black,
                            ),
                          ),
                          new RichText(
                            text: new TextSpan(children: <TextSpan>[
                              new TextSpan(
                                text: "ADMISSION DATE: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              new TextSpan(
                                text: admissionDetailItem.admissionDate,
                                style: new TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black,
                                ),
                              )
                            ]),
                          ),
                        ],
                      ),
                    )),
                new Card(
                  elevation: 5.0,
                  margin: new EdgeInsets.all(8.0),
                  child: new Container(
                    padding: new EdgeInsets.all(8.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          "CHIEF COMPLAIN",
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        new Text(
                          admissionDetailItem.chiefComplain,
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 16.0),
                        ),
                        new Text(
                          "VITALS",
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        new Text(
                          admissionDetailItem.vitals,
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 16.0),
                        ),
                        new Text(
                          "HISTORY OF PRESENT ILLNESS",
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        new Text(
                          admissionDetailItem.history,
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 16.0),
                        ),
                        new Text(
                          "GENERAL EXAMINATION",
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        new Text(
                          admissionDetailItem.generalExamination,
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 16.0),
                        ),
                        new Text(
                          "PROVISIONAL DIAGNOSIS",
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        new Text(
                          admissionDetailItem.provisionalDiagnosis,
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 16.0),
                        ),
                        new Text(
                          "PLAN",
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        new Text(
                          admissionDetailItem.plan,
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 16.0),
                        ),
                        new Text(
                          "NOTE",
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        new Text(
                          admissionDetailItem.note,
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            )
          : admissionDetailItemC != null
              ? new ListView(
                  children: <Widget>[
                    new Card(
                        elevation: 5.0,
                        margin: new EdgeInsets.all(8.0),
                        child: new Container(
                          padding: new EdgeInsets.all(8.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Center(
                                child: new Text(
                                  admissionDetailItemC.hospital,
                                  textAlign: TextAlign.center,
                                  style: new TextStyle(
                                    color: Colors.blueAccent,
                                    fontSize: 22.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              new Center(
                                child: new Text(
                                  admissionDetailItemC.department,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                              new Padding(
                                padding: new EdgeInsets.only(top: 8.0),
                                child: new RichText(
                                  text: new TextSpan(children: <TextSpan>[
                                    new TextSpan(
                                      text: "INDEX NUMBER: ",
                                      style: new TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    ),
                                    new TextSpan(
                                      text: admissionDetailItemC.indexNumber,
                                      style: new TextStyle(
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black,
                                      ),
                                    )
                                  ]),
                                ),
                              ),
                              new Padding(
                                padding:
                                    new EdgeInsets.only(top: 8.0, bottom: 8.0),
                                child: new Divider(
                                  height: 10.0,
                                  color: Colors.black,
                                ),
                              ),
                              new RichText(
                                text: new TextSpan(children: <TextSpan>[
                                  new TextSpan(
                                    text: "CHECKUP DATE: ",
                                    style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  new TextSpan(
                                    text: admissionDetailItemC.checkupDate,
                                    style: new TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black,
                                    ),
                                  )
                                ]),
                              ),
                              new Padding(
                                padding: new EdgeInsets.only(top: 8.0),
                              ),
                              new RichText(
                                text: new TextSpan(children: <TextSpan>[
                                  new TextSpan(
                                    text: "FOLLOWUP DATE: ",
                                    style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  new TextSpan(
                                    text: admissionDetailItemC.followupDate,
                                    style: new TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black,
                                    ),
                                  )
                                ]),
                              ),
                            ],
                          ),
                        )),
                    new Card(
                      elevation: 5.0,
                      margin: new EdgeInsets.all(8.0),
                      child: new Container(
                        padding: new EdgeInsets.all(8.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Text(
                              "DOCTOR",
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                            new Text(
                              admissionDetailItemC.doctor,
                              style: new TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            ),
                            new Padding(
                              padding: new EdgeInsets.only(top: 16.0),
                            ),
                            new Text(
                              "DEPARTMENT",
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                            new Text(
                              admissionDetailItemC.department,
                              style: new TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            ),
                            new Padding(
                              padding: new EdgeInsets.only(top: 16.0),
                            ),
                            new Text(
                              "FOLLOWUP NOTES",
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                            new Text(
                              admissionDetailItemC.followupNote,
                              style: new TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            ),
                            new Padding(
                              padding: new EdgeInsets.only(top: 16.0),
                            ),
                            new Text(
                              "HISTORY OF PRESENT ILLNESS",
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                            new Text(
                              admissionDetailItemC.history,
                              style: new TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )
              : new Center(),
    );
  }
}
