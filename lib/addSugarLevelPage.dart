import 'package:fhn_flutter/appbar/gradientAppBarWithCross.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/sugarLevelItem.dart';
import 'package:flutter/material.dart';

class AddSugarLevelPage extends StatefulWidget {
  final String name;
  final String sugarType;
  final String unit;
  final String range;
  final num sugarLevel;
  final String date;

  AddSugarLevelPage(
      {Key key,
      @required this.name,
      @required this.sugarType,
      @required this.unit,
      @required this.range,
      @required this.sugarLevel,
      @required this.date})
      : super(key: key);
  @override
  _AddSugarLevelPageState createState() => _AddSugarLevelPageState();
}

class _AddSugarLevelPageState extends State<AddSugarLevelPage> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

//   List<String> radioList = ['Fasting (FBS)', 'Random (RBS)', 'PPBS'];

  String name;
  String sugarType;
  String unit;
  String range;
  num sugarLevel;
  String date;
  int sugarTypeIndex = 0;

  String _validateName(String value) {
    if (value.length == 0) {
      return AppLocalization.of(context).nameIsRequired;
    }

    return null;
  }

  String _validateSugarLevel(String value) {
    if (value.length == 0) {
      return AppLocalization.of(context).addSugarLevelText;
    }

    return null;
  }

  @override
  void initState() {
    name = widget.name;
    sugarType = widget.sugarType;
    unit = widget.unit;
    range = widget.range;
    sugarLevel = widget.sugarLevel;
    date = widget.date;
    super.initState();
  }

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
        hintColor: Color(0xF29E30BF),
        primaryColor: Color(0xF29E30BF),
        toggleableActiveColor: Color(0xF29E30BF),
        inputDecorationTheme: new InputDecorationTheme(
            labelStyle: TextStyle(
          color: Color(0xF29E30BF),
        )));
  }

  @override
  Widget build(BuildContext context) {
    List<SugarType> radioList = <SugarType>[
      new SugarType(
          AppLocalization.of(context).fastingText,
          AppLocalization.of(context).fastingRange1,
          AppLocalization.of(context).fastingRange2),
      new SugarType(
          AppLocalization.of(context).randomText,
          AppLocalization.of(context).randomRange1,
          AppLocalization.of(context).randomRange2),
      new SugarType(
          AppLocalization.of(context).ppbsText,
          AppLocalization.of(context).ppbsRange1,
          AppLocalization.of(context).ppbsRange2)
    ];

    _onButtonSubmit() {
      if (this._formKey.currentState.validate()) {
        _formKey.currentState.save();

        range = unit == "mg/dL"
            ? radioList[sugarTypeIndex].range1
            : radioList[sugarTypeIndex].range2;
        String date = new DateTime.now().toString();

        Navigator.pop(
            context,
            new SugarLevelItem(
                name: name,
                sugarType: sugarType,
                unit: unit,
                range: range,
                sugarLevel: sugarLevel,
                date: date.substring(0, date.lastIndexOf("."))));
      }
    }

    return new Theme(
      data: buildTheme(),
      child: new Scaffold(
        key: scaffoldKey,
        body: Column(
          children: <Widget>[
            new GradientAppBarWithCross(
                AppLocalization.of(context).setSugarLevel, false),
            new Expanded(
              child: new Container(
                padding: new EdgeInsets.all(16.0),
                child: new Form(
                  key: _formKey,
                  child: new ListView(
                    children: <Widget>[
                      new TextFormField(
                        keyboardType: TextInputType.text,
                        initialValue: name,
                        decoration: new InputDecoration(
                          hintText: AppLocalization.of(context).enterName,
                          labelText: AppLocalization.of(context).nameText,
                          contentPadding: new EdgeInsets.symmetric(
                              horizontal: 16.0, vertical: 16.0),
                          hintStyle: new TextStyle(color: Colors.grey),
                          border: new OutlineInputBorder(),
                        ),
                        validator: this._validateName,
                        onSaved: (String value) {
                          name = value;
                        },
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 16.0),
                      ),
                      new Text(
                        AppLocalization.of(context).chooseGlucosePlasma,
                        style: new TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new ListTile(
                        title: new Text(
                            AppLocalization.of(context).sugarLevelSelectUnit),
                        trailing: new DropdownButton<String>(
                          value: unit,
                          onChanged: (value) {
                            setState(() {
                              unit = value;
                            });
                          },
                          items:
                              <String>['mg/dL', 'mmol/L'].map((String value) {
                            return new DropdownMenuItem<String>(
                              value: value,
                              child: new Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new Column(
                          children: radioList.map((SugarType item) {
                        return new RadioListTile(
                          title: new Text(item.value),
                          //activeColor: Color(0xF29E30BF),
                          secondary: new Text(
                              unit == "mg/dL" ? item.range1 : item.range2),
                          value: item.value,
                          groupValue: sugarType,
                          onChanged: (String value) {
                            setState(() {
                              sugarType = value;
                              sugarTypeIndex = radioList.indexOf(item);
                            });
                          },
                        );
                      }).toList()),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new TextFormField(
                        keyboardType: TextInputType.number,
                        initialValue: sugarLevel.toString(),
                        decoration: new InputDecoration(
                          hintText:
                              AppLocalization.of(context).addSugarLevelText,
                          labelText: AppLocalization.of(context).sugarLevelText,
                          contentPadding: new EdgeInsets.symmetric(
                              horizontal: 16.0, vertical: 16.0),
                          hintStyle: new TextStyle(color: Colors.grey),
                          border: new OutlineInputBorder(),
                        ),
                        validator: this._validateSugarLevel,
                        onSaved: (String value) {
                          sugarLevel = num.parse(value);
                        },
                      ),
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Padding(
                            padding: new EdgeInsets.only(top: 24.0),
                            child: new MaterialButton(
                              height: 45.0,
                              child: new Text(
                                AppLocalization.of(context).cancelText,
                                style: new TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18.0,
                                ),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 24.0),
                            child: new MaterialButton(
                              height: 45.0,
                              child: new Text(
                                AppLocalization.of(context).okText,
                                style: new TextStyle(
                                  color: Color(0xFF9E30BF),
                                  fontSize: 18.0,
                                ),
                              ),
                              onPressed: _onButtonSubmit,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SugarType {
  SugarType(this.value, this.range1, this.range2);
  String value;
  String range1;
  String range2;
}
