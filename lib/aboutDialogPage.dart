import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  var flutterWebViewPlugin = new FlutterWebviewPlugin();

  final String aboutString = "<html>" +
      "<body>" +
      "<p><b>About:</b>" +
      "<br>Family Health Nepal [FHN] is a service oriented health organization focused towards you and your loved ones personal health. " +
      "FHN is a hub organization that holds many well established specialized as well as general hospitals that will make the health service easily accessible at an affordable cost.</p>" +
      "<br>" +
      "<b>Benefits:</b>" +
      "<br>" +
      "<br>1.) Best treatment in the best hospital from best doctors." +
      "<br>2.) Time saving that reduces the burden of awaiting the date of appointment and availability of doctors." +
      "<br>3.) Reduction in out of pocket payment." +
      "<br>4.) Access to view ones medical record from anywhere at any time." +
      "<br>5.) Free from burden of carrying tremendous past history record for proper diagnosis of disease." +
      "<br><br>" +
      "<b>Contact</b>" +
      "<br>address : Maitighar 11, Kathmandu, Nepal." +
      "<br>" +
      "hot-line :+977-9801203380" +
      "<br>email1 : info@familyhealthnepal.com" +
      "<br>email2 : familyhealthnepal@gmail.com" +
      "</body>" +
      "</html>";

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("About"),
      ),
      body: new Center(
        child: new RaisedButton(
          onPressed: () {
            flutterWebViewPlugin.launch(
              "https://flutter.io/",
              rect: new Rect.fromLTWH(
                  0.0, 0.0, MediaQuery.of(context).size.width, 300.0),
            );
          },
          child: new Text("Open Webview (rect)"),
        ),
      ),
    );
  }
}
