import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/galleryImageViewPage.dart';
import 'package:fhn_flutter/model/galleryImageItem.dart';
import 'package:flutter/material.dart';

class GalleryImageDetailPage extends StatefulWidget {
  final List<GalleryImages> imageItem;
  final String albumName;
  GalleryImageDetailPage(
      {Key key, @required this.albumName, @required this.imageItem})
      : super(key: key);
  @override
  _GalleryImageDetailPageState createState() => _GalleryImageDetailPageState();
}

class _GalleryImageDetailPageState extends State<GalleryImageDetailPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Column(
      children: <Widget>[
        new GradientAppBarWithBack(widget.albumName),
        new Expanded(
          child: new Container(
            child: new GridView.builder(
              padding: new EdgeInsets.all(8.0),
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                crossAxisSpacing: 8.0,
                mainAxisSpacing: 8.0,
              ),
              itemCount: widget.imageItem.length,
              itemBuilder: (context, index) {
                return new GestureDetector(
                  onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                      builder: (context) => new GalleryImageViewerPage(
                            position: index,
                            list: widget.imageItem,
                          ),
                      fullscreenDialog: true)),
                  child: new Container(
                    child: new Hero(
                      tag: "galleryImage" +
                          widget.imageItem[index].id.toString() +
                          index.toString(),
                      child: new Image.network(
                        widget.imageItem[index].image,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    ));
  }
}
