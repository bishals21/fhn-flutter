import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/database.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/insideMedicationItem.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class InsideMedicationPage extends StatefulWidget {
  final String from, accessToken;
  final int recordId;

  InsideMedicationPage(
      {Key key,
      @required this.accessToken,
      @required this.from,
      @required this.recordId})
      : super(key: key);
  @override
  _InsideMedicationPageState createState() => _InsideMedicationPageState();
}

class _InsideMedicationPageState extends State<InsideMedicationPage> {
  var db = DatabaseHelper();

  List<InsideMedicationItem> list = new List();

  Future<Null> _handleRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    List<InsideMedicationItem> listOfItem = new List();
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          "http://familyhealthnepal.com/api/" +
              widget.from +
              "/" +
              widget.recordId.toString() +
              "/medication?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"]["_embedded"]["items"];
          deleteFromDB(widget.recordId, widget.from);
          for (var item in data) {
            StringBuffer timeBuilder;
            String timeReady;
            timeBuilder = new StringBuffer();
            var timeArray = item["time"];

            for (var timeItem in timeArray) {
              String time = timeItem;
              timeBuilder.write(time + "#");
            }

            timeReady = timeBuilder
                .toString()
                .substring(0, timeBuilder.toString().lastIndexOf("#"));

            listOfItem.add(new InsideMedicationItem(
              recordId: widget.recordId,
              from: widget.from,
              medicationId: item["medication_id"],
              drug: item["drug"],
              dose: item["dose"],
              frequency: item["frequency"],
              route: item["route"],
              time: timeReady,
              fromDate: item["fromDate"],
              toDate: item["toDate"],
            ));

            saveToDB(new InsideMedicationItem(
              recordId: widget.recordId,
              from: widget.from,
              medicationId: item["medication_id"],
              drug: item["drug"],
              dose: item["dose"],
              frequency: item["frequency"],
              route: item["route"],
              time: timeReady,
              fromDate: item["fromDate"],
              toDate: item["toDate"],
            ));
          }

          if (mounted) {
            this.setState(() {
              list = listOfItem;
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(AppLocalization.of(context).noInternetConnection),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getFromDB();
      return completer.future;
    }
  }

  Future getFromDB() async {
    list = await db.getInsideMedication(widget.recordId, widget.from);
    this.setState(() {});
  }

  Future saveToDB(InsideMedicationItem item) async {
    await db.saveInsideMedication(item);
  }

  Future deleteFromDB(int recordId, String from) async {
    await db.deleteInsideMedication(recordId, from);
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    _handleRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new RefreshIndicator(
      onRefresh: _handleRefresh,
      child: new ListView.builder(
          padding: new EdgeInsets.all(8.0),
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            final InsideMedicationItem item = list[index];
            final List<String> timeList = item.time.split("#");

            return new Card(
              elevation: 5.0,
              child: new Container(
                padding: new EdgeInsets.all(8.0),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new RichText(
                          text: new TextSpan(children: <TextSpan>[
                            new TextSpan(
                              text: AppLocalization.of(context).medicationFrom,
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                            new TextSpan(
                              text: item.fromDate,
                              style: new TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            )
                          ]),
                        ),
                        new RichText(
                          text: new TextSpan(children: <TextSpan>[
                            new TextSpan(
                              text: AppLocalization.of(context).medicationTo,
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                            new TextSpan(
                              text: item.toDate,
                              style: new TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.black,
                              ),
                            )
                          ]),
                        ),
                      ],
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 12.0),
                    ),
                    new Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          AppLocalization.of(context).medicationTime,
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        new Flexible(
                          child: new Container(
                            padding: new EdgeInsets.only(left: 8.0),
                            child: new Wrap(
                              spacing: 5.0,
                              runSpacing: 4.0,
                              children: timeList
                                  .map((item) => new Container(
                                        padding: new EdgeInsets.all(1.0),
                                        decoration: new BoxDecoration(
                                            border: new Border.all(
                                                color: Colors.blueAccent)),
                                        child: new Text(
                                          item,
                                          style: new TextStyle(
                                            fontWeight: FontWeight.w600,
                                            color: Colors.black,
                                          ),
                                        ),
                                      ))
                                  .toList(),
                            ),
                          ),
                        ),
                      ],
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 12.0),
                    ),
                    new RichText(
                      text: new TextSpan(children: <TextSpan>[
                        new TextSpan(
                          text: AppLocalization.of(context).medicationDrug,
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        new TextSpan(
                          text: item.drug,
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        )
                      ]),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 12.0),
                    ),
                    new RichText(
                      text: new TextSpan(children: <TextSpan>[
                        new TextSpan(
                          text: AppLocalization.of(context).medicationDrug,
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                        new TextSpan(
                          text: item.dose,
                          style: new TextStyle(
                            fontWeight: FontWeight.w600,
                            color: Colors.black,
                          ),
                        )
                      ]),
                    ),
                  ],
                ),
              ),
            );
          }),
    );
  }
}
