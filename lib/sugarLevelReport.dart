import 'dart:async';

import 'package:fhn_flutter/model/sugarLevelItem.dart';
import 'package:flutter/material.dart';
import 'database.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class SugarLevelReport extends StatefulWidget {
  @override
  _SugarLevelReportState createState() => _SugarLevelReportState();
}

class _SugarLevelReportState extends State<SugarLevelReport> {
  var db = DatabaseHelper();

  List<SugarLevelItem> list = new List();

  List<charts.Series<LinearData, int>> dataSets1 = new List();
  List<charts.Series<LinearData, int>> dataSets2 = new List();
  List<charts.Series<LinearData, int>> dataSets3 = new List();

  List<LinearData> fasting = new List();
  List<LinearData> random = new List();
  List<LinearData> ppbs = new List();

  Future getListFromDB() async {
    list = await db.getAllSugarLevel();

    for (int i = 0; i < list.length; i++) {
      SugarLevelItem item = list[i];

      num sugarLevel = item.sugarLevel;

      if (item.unit == "mmol/L") {
        sugarLevel = (18.018018) * sugarLevel;
      }

      if (item.sugarType == "Fasting (FBS)") {
        fasting.add(new LinearData(item.id, sugarLevel));
      } else if (item.sugarType == "Random (RBS)") {
        random.add(new LinearData(item.id, sugarLevel));
      } else {
        ppbs.add(new LinearData(item.id, sugarLevel));
      }
    }

    dataSets1.add(new charts.Series<LinearData, int>(
        id: 'SUGAR LEVEL in mg/dL',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (LinearData dataItem, _) => dataItem.date,
        measureFn: (LinearData dataItem, _) => dataItem.value,
        data: fasting));

    dataSets2.add(new charts.Series<LinearData, int>(
        id: 'SUGAR LEVEL in mg/dL',
        colorFn: (_, __) => charts.MaterialPalette.green.shadeDefault,
        domainFn: (LinearData dataItem, _) => dataItem.date,
        measureFn: (LinearData dataItem, _) => dataItem.value,
        data: random));

    dataSets3.add(new charts.Series<LinearData, int>(
        id: 'SUGAR LEVEL in mg/dL',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (LinearData dataItem, _) => dataItem.date,
        measureFn: (LinearData dataItem, _) => dataItem.value,
        data: ppbs));

    setState(() {});
  }

  @override
  void initState() {
    getListFromDB();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new ListView(
        children: <Widget>[
          new Container(
            color: Colors.grey,
            padding: new EdgeInsets.all(12.0),
            child: new Text(
              "TEST PPBS",
              style: new TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
          ),
          dataSets3.isNotEmpty
              ? new Container(
                  height: 300.0,
                  padding: new EdgeInsets.all(8.0),
                  child: new charts.LineChart(
                    dataSets3,
                    animate: true,
                    defaultRenderer:
                        new charts.LineRendererConfig(includePoints: true),
                    behaviors: [
                      new charts.SeriesLegend(
                          position: charts.BehaviorPosition.bottom)
                    ],
                  ),
                )
              : new Center(
                  child: new CircularProgressIndicator(),
                ),
          new Container(
            padding: new EdgeInsets.all(12.0),
            color: Colors.grey,
            child: new Text(
              "TEST FASTING",
              style: new TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
          ),
          dataSets1.isNotEmpty
              ? new Container(
                  height: 300.0,
                  padding: new EdgeInsets.all(8.0),
                  child: new charts.LineChart(
                    dataSets1,
                    animate: true,
                    defaultRenderer:
                        new charts.LineRendererConfig(includePoints: true),
                    behaviors: [
                      new charts.SeriesLegend(
                          position: charts.BehaviorPosition.bottom)
                    ],
                  ),
                )
              : new Center(
                  child: new CircularProgressIndicator(),
                ),
          new Container(
            padding: new EdgeInsets.all(12.0),
            color: Colors.grey,
            child: new Text(
              "TEST RANDOM",
              style: new TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
          ),
          dataSets2.isNotEmpty
              ? new Container(
                  height: 300.0,
                  padding: new EdgeInsets.all(8.0),
                  child: new charts.LineChart(
                    dataSets2,
                    animate: true,
                    defaultRenderer:
                        new charts.LineRendererConfig(includePoints: true),
                    behaviors: [
                      new charts.SeriesLegend(
                          position: charts.BehaviorPosition.bottom)
                    ],
                  ),
                )
              : new Center(
                  child: new CircularProgressIndicator(),
                ),
        ],
      ),
    );
  }
}

class LinearData {
  final int date;
  final num value;

  LinearData(this.date, this.value);
}
