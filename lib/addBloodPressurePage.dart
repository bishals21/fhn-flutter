import 'package:flutter/material.dart';

class AddBloodPressurePage extends StatefulWidget {
  @override
  _AddBloodPressurePageState createState() => _AddBloodPressurePageState();
}

class _AddBloodPressurePageState extends State<AddBloodPressurePage> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  final TextEditingController nameController = new TextEditingController();

  bool validateFields() {
    bool isValidate = true;
    if (nameController.text.isEmpty) {
      isValidate = false;
      _showSnackBar("Please enter medicine name");
      return isValidate;
    }

    return isValidate;
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(),
      body: new Container(
        padding: new EdgeInsets.all(16.0),
        child: new ListView(
          children: <Widget>[
            new TextField(
              keyboardType: TextInputType.text,
              controller: nameController,
              decoration: new InputDecoration(
                hintText: "Enter your name",
                labelText: "Name",
                hintStyle: new TextStyle(color: Colors.grey),
                border: new OutlineInputBorder(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
