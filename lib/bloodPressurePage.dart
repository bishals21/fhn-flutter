import 'dart:async';

import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/bloodPressureItem.dart';
import 'package:flutter/material.dart';
import 'database.dart';
import 'package:numberpicker/numberpicker.dart';

class BloodPressurePage extends StatefulWidget {
  @override
  _BloodPressurePageState createState() => _BloodPressurePageState();
}

class _BloodPressurePageState extends State<BloodPressurePage> {
  var db = DatabaseHelper();
  List<BloodPressureItem> list = new List();

  Future getListFromDB() async {
    list = await db.getAllBloodPressure();
    this.setState(() {});
  }

  Future updateDB(BloodPressureItem item, int index) async {
    int res = await db.updateBloodPressure(item);
    list.removeAt(index);
    list.insert(index, item);
    this.setState(() {});
  }

  Future saveToDB(BloodPressureItem item) async {
    await db.saveBloodPressure(item);
    list.add(item);
    setState(() {});
  }

  @override
  void initState() {
    getListFromDB();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Column(
        children: <Widget>[
          new GradientAppBarWithBack(
              AppLocalization.of(context).bloodPressureTitle),
          new Expanded(
            child: new Container(
              child: list.isNotEmpty
                  ? new ListView.builder(
                      padding: new EdgeInsets.all(8.0),
                      itemCount: list.length,
                      itemBuilder: (context, index) {
                        final BloodPressureItem item = list[index];

                        return new GestureDetector(
                          onTap: () {
                            _navigateToUpdateBloddPressure(context, index);
                          },
                          child: new Card(
                            elevation: 5.0,
                            child: new Container(
                              padding: new EdgeInsets.all(8.0),
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                    item.name.toUpperCase(),
                                    style: new TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.only(top: 10.0),
                                  ),
                                  new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      new Column(
                                        children: <Widget>[
                                          new Text(
                                            "SYSTOLIC",
                                            textAlign: TextAlign.center,
                                            style: new TextStyle(
                                                fontWeight: FontWeight.w600),
                                          ),
                                          new Padding(
                                            padding: new EdgeInsets.symmetric(
                                                vertical: 4.0),
                                          ),
                                          new Text(
                                            item.highSys,
                                            textAlign: TextAlign.center,
                                            style: new TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                      new Column(
                                        children: <Widget>[
                                          new Text(
                                            "DIASTOLIC",
                                            textAlign: TextAlign.center,
                                            style: new TextStyle(
                                                fontWeight: FontWeight.w600),
                                          ),
                                          new Padding(
                                            padding: new EdgeInsets.symmetric(
                                                vertical: 4.0),
                                          ),
                                          new Text(
                                            item.lowSys,
                                            textAlign: TextAlign.center,
                                            style: new TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                      new Column(
                                        children: <Widget>[
                                          new Text(
                                            "PULSE",
                                            textAlign: TextAlign.center,
                                            style: new TextStyle(
                                                fontWeight: FontWeight.w600),
                                          ),
                                          new Padding(
                                            padding: new EdgeInsets.symmetric(
                                                vertical: 4.0),
                                          ),
                                          new Text(
                                            item.pulse,
                                            textAlign: TextAlign.center,
                                            style: new TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.only(top: 10.0),
                                  ),
                                  new Text(
                                    AppLocalization.of(context).dateText +
                                        " " +
                                        item.date,
                                    style: new TextStyle(
                                      fontSize: 16.0,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    )
                  : new Center(
                      child: new Image.asset(
                        "assets/empty.png",
                        height: 200.0,
                        width: 200.0,
                      ),
                    ),
            ),
          ),
        ],
      ),
      floatingActionButton: new FloatingActionButton(
        backgroundColor: Color(0xFF9E30BF),
        child: new Icon(Icons.add),
        onPressed: () {
          _navigateToAddBloddPressure(context);
        },
      ),
    );
  }

  _navigateToAddBloddPressure(BuildContext context) async {
    final BloodPressureItem result = await showDialog(
        context: context,
        builder: (context) {
          return new AlertDialog(
              content: new BloodPressureAdd(
            name: "",
            currentHighSys: 120,
            currentLowSys: 80,
            currentPulse: 75,
          ));
        });

    if (result != null) {
      saveToDB(result);
    }
  }

  _navigateToUpdateBloddPressure(BuildContext context, int index) async {
    BloodPressureItem updateItem = list[index];
    final BloodPressureItem result = await showDialog(
        context: context,
        builder: (context) {
          return new AlertDialog(
              content: new BloodPressureAdd(
            name: updateItem.name,
            currentHighSys: int.parse(updateItem.highSys),
            currentLowSys: int.parse(updateItem.lowSys),
            currentPulse: int.parse(updateItem.pulse),
          ));
        });

    if (result != null) {
      BloodPressureItem item = new BloodPressureItem(
          id: updateItem.id,
          name: result.name,
          highSys: result.highSys,
          lowSys: result.lowSys,
          pulse: result.pulse,
          date: result.date);
      updateDB(item, index);
    }
  }
}

class BloodPressureAdd extends StatefulWidget {
  final int currentHighSys;
  final int currentLowSys;
  final int currentPulse;
  final String name;

  BloodPressureAdd({
    Key key,
    @required this.name,
    @required this.currentHighSys,
    @required this.currentLowSys,
    @required this.currentPulse,
  }) : super(key: key);
  @override
  _BloodPressureAddState createState() => _BloodPressureAddState();
}

class _BloodPressureAddState extends State<BloodPressureAdd> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  int currentHighSys;
  int currentLowSys;
  int currentPulse;
  String name;

  String _validateName(String value) {
    if (value.length == 0) {
      return AppLocalization.of(context).nameIsRequired;
    }

    return null;
  }

  @override
  void initState() {
    currentHighSys = widget.currentHighSys;
    currentLowSys = widget.currentLowSys;
    currentPulse = widget.currentPulse;
    name = widget.name;
    super.initState();
  }

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
        hintColor: Color(0xF29E30BF),
        primaryColor: Color(0xF29E30BF),
        accentColor: Color(0xF29E30BF),
        inputDecorationTheme: new InputDecorationTheme(
            labelStyle: TextStyle(
          color: Color(0xF29E30BF),
        )));
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    void sendValue() {
      if (this._formKey.currentState.validate()) {
        _formKey.currentState.save();

        String date = new DateTime.now().toString();
        Navigator.pop(
          context,
          new BloodPressureItem(
              name: name,
              highSys: currentHighSys.toString(),
              lowSys: currentLowSys.toString(),
              pulse: currentPulse.toString(),
              date: date.substring(0, date.lastIndexOf("."))),
        );
      }
    }

    return new Theme(
      data: buildTheme(),
      child: new Container(
        height: 290.0,
        width: width,
        child: new Form(
          key: _formKey,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new TextFormField(
                keyboardType: TextInputType.text,
                initialValue: name,
                decoration: new InputDecoration(
                  hintText: AppLocalization.of(context).enterName,
                  labelText: AppLocalization.of(context).nameText,
                  contentPadding: new EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 16.0),
                  hintStyle: new TextStyle(color: Colors.grey),
                  border: new OutlineInputBorder(),
                ),
                validator: this._validateName,
                onSaved: (String value) {
                  name = value;
                },
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 16.0),
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "SYSTOLIC",
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      new NumberPicker.integer(
                        maxValue: 200,
                        minValue: 0,
                        itemExtent: 40.0,
                        listViewWidth: 50.0,
                        initialValue: currentHighSys,
                        onChanged: (newValue) {
                          setState(() {
                            currentHighSys = newValue;
                          });
                        },
                      )
                    ],
                  ),
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "DIASTOLIC",
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      new NumberPicker.integer(
                        maxValue: 200,
                        minValue: 0,
                        itemExtent: 40.0,
                        listViewWidth: 50.0,
                        initialValue: currentLowSys,
                        onChanged: (newValue) {
                          setState(() {
                            currentLowSys = newValue;
                          });
                        },
                      )
                    ],
                  ),
                  new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "PULSE",
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      new NumberPicker.integer(
                        maxValue: 200,
                        minValue: 0,
                        itemExtent: 40.0,
                        listViewWidth: 50.0,
                        initialValue: currentPulse,
                        onChanged: (newValue) {
                          setState(() {
                            currentPulse = newValue;
                          });
                        },
                      )
                    ],
                  ),
                ],
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 16.0),
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  new FlatButton(
                    child: new Text(
                      AppLocalization.of(context).cancelText,
                      style: new TextStyle(color: Colors.red),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  new FlatButton(
                    child: new Text(
                      AppLocalization.of(context).setText,
                      style: new TextStyle(color: Color(0xF29E30BF)),
                    ),
                    onPressed: sendValue,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

// builder: (context) {
//           int currentSys = 120;
//           return new Container(
//             height: 400.0,
//             child: new Column(
//               children: <Widget>[
//                 new NumberPicker.integer(
//                   maxValue: 200,
//                   minValue: 0,
//                   initialValue: currentSys,
//                   onChanged: (newValue) {
//                     setState(() {
//                       currentSys = newValue;
//                     });
//                   },
//                 )
//               ],
//             ),
//           );
//         }
