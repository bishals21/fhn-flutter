import 'package:fhn_flutter/galleryImageDetailPage.dart';
import 'package:fhn_flutter/model/galleryImageItem.dart';
import 'package:flutter/material.dart';

class GalleryImagePage extends StatefulWidget {
  final List<GalleryImageItem> imageItem;

  GalleryImagePage({Key key, @required this.imageItem}) : super(key: key);
  @override
  _GalleryImagePageState createState() => _GalleryImagePageState();
}

class _GalleryImagePageState extends State<GalleryImagePage> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new GridView.builder(
        padding: new EdgeInsets.all(8.0),
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 8.0,
          mainAxisSpacing: 8.0,
        ),
        itemCount: widget.imageItem.length,
        itemBuilder: (context, index) {
          return new GestureDetector(
            onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                  builder: (context) => new GalleryImageDetailPage(
                        albumName: widget.imageItem[index].albumName,
                        imageItem: widget.imageItem[index].galleryImages,
                      ),
                )),
            child: new Container(
              child: new Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  new Image.network(
                    widget.imageItem[index].galleryImages[0].image,
                    fit: BoxFit.cover,
                  ),
                  new Positioned(
                      bottom: 0.0,
                      left: 0.0,
                      right: 0.0,
                      child: new Container(
                        padding: new EdgeInsets.all(8.0),
                        color: Colors.black54,
                        child: new Text(
                          widget.imageItem[index].albumName,
                          textAlign: TextAlign.center,
                          style: new TextStyle(
                              color: Colors.white, fontSize: 15.0),
                        ),
                      ))
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
