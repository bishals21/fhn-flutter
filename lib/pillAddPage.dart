import 'dart:async';

import 'package:fhn_flutter/appbar/gradientAppBarWithCross.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:flutter/material.dart';

import 'model/addPillModel.dart';

class AddPillReminder extends StatefulWidget {
  @override
  _AddPillReminderState createState() => new _AddPillReminderState();
}

class _AddPillReminderState extends State<AddPillReminder> {
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  DateTime _date = new DateTime.now();
  TimeOfDay _time = new TimeOfDay.now();
  String fromDatePicked = "";
  String toDatePicked = "";

  final TextEditingController fromController = new TextEditingController();
  final TextEditingController toController = new TextEditingController();
  final TextEditingController medicineController = new TextEditingController();
  final TextEditingController medicineDoseController =
      new TextEditingController();

  List<String> _timeList = new List();

  Future<Null> _selectDate(BuildContext context, String type) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2016),
        lastDate: new DateTime(2030));

    if (picked != null) {
      setState(() {
        type == "from"
            ? fromDatePicked = "${picked.year}-${picked.month}-${picked.day}"
            : toDatePicked = "${picked.year}-${picked.month}-${picked.day}";

        type == "from"
            ? fromController.text = fromDatePicked
            : toController.text = toDatePicked;
      });
    }
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: _time);

    if (picked != null) {
      setState(() {
        _timeList.add("${picked.hour}:${picked.minute}");
      });
    }
  }

  removeTimeChip(String name) {
    setState(() {
      _timeList.remove(name);
    });
  }

  bool validateFields() {
    bool isValidate = true;
    if (medicineController.text.isEmpty) {
      isValidate = false;
      _showSnackBar(AppLocalization.of(context).pillEnterMedicine);
      return isValidate;
    }

    if (fromController.text.isEmpty) {
      isValidate = false;
      _showSnackBar(AppLocalization.of(context).pillSelectFromDate);
      return isValidate;
    }

    if (toController.text.isEmpty) {
      isValidate = false;
      _showSnackBar(AppLocalization.of(context).pillSelectToDate);
      return isValidate;
    }

    if (medicineDoseController.text.isEmpty) {
      isValidate = false;
      _showSnackBar(AppLocalization.of(context).pillEnterMedicineDose);
      return isValidate;
    }

    if (_timeList.isEmpty) {
      isValidate = false;
      _showSnackBar(AppLocalization.of(context).pillChooseTime);
      return isValidate;
    }

    return isValidate;
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
        hintColor: Color(0xF29E30BF),
        primaryColor: Color(0xF29E30BF),
        inputDecorationTheme: new InputDecorationTheme(
            labelStyle: TextStyle(
          color: Color(0xF29E30BF),
        )));
  }

  @override
  Widget build(BuildContext context) {
    return new Theme(
      data: buildTheme(),
      child: new Scaffold(
        key: scaffoldKey,
        body: Column(
          children: <Widget>[
            new GradientAppBarWithCross(
                AppLocalization.of(context).setPillReminder, false),
            new Expanded(
              child: new Container(
                padding: new EdgeInsets.all(16.0),
                child: new ListView(
                  children: <Widget>[
                    new TextField(
                      keyboardType: TextInputType.text,
                      controller: fromController,
                      decoration: new InputDecoration(
                        hintText: AppLocalization.of(context).pillChooseDate,
                        labelText: AppLocalization.of(context).pillFromDate,
                        hintStyle: new TextStyle(color: Colors.grey),
                        border: new OutlineInputBorder(),
                        suffixIcon: new GestureDetector(
                          onTap: () {
                            _selectDate(context, "from");
                          },
                          child: new Icon(Icons.date_range),
                        ),
                      ),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 24.0),
                      child: new TextField(
                        keyboardType: TextInputType.text,
                        controller: toController,
                        decoration: new InputDecoration(
                          hintText: AppLocalization.of(context).pillChooseDate,
                          labelText: AppLocalization.of(context).pillToDate,
                          hintStyle: new TextStyle(color: Colors.grey),
                          border: new OutlineInputBorder(),
                          suffixIcon: new GestureDetector(
                            onTap: () {
                              _selectDate(context, "to");
                            },
                            child: new Icon(Icons.date_range),
                          ),
                        ),
                      ),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 24.0),
                      child: new TextField(
                        keyboardType: TextInputType.text,
                        controller: medicineController,
                        decoration: new InputDecoration(
                          hintText:
                              AppLocalization.of(context).pillMedicineName,
                          labelText:
                              AppLocalization.of(context).pillMedicineName,
                          hintStyle: new TextStyle(color: Colors.grey),
                          border: new OutlineInputBorder(),
                        ),
                      ),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 24.0),
                      child: new TextField(
                        keyboardType: TextInputType.text,
                        controller: medicineDoseController,
                        decoration: new InputDecoration(
                          hintText:
                              AppLocalization.of(context).pillMedicineDose,
                          labelText:
                              AppLocalization.of(context).pillMedicineDose,
                          hintStyle: new TextStyle(color: Colors.grey),
                          border: new OutlineInputBorder(),
                        ),
                        // validator: this._validateWeight,
                        // onSaved: (String value) {
                        //   this.weight = value;
                        // },
                      ),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 24.0),
                      child: new MaterialButton(
                        height: 45.0,
                        child: new Text(
                          AppLocalization.of(context).pillAddTime,
                          style: new TextStyle(
                            color: Color(0xFF9E30BF),
                            fontSize: 18.0,
                          ),
                        ),
                        onPressed: () {
                          _selectTime(context);
                        },
                      ),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 24.0),
                      child: new Container(
                        child: _timeList.isEmpty
                            ? new Center(
                                child: new Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: new Text(
                                    AppLocalization.of(context)
                                        .pillEmptyAddTime,
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption
                                        .copyWith(fontStyle: FontStyle.italic),
                                  ),
                                ),
                              )
                            : new Wrap(
                                children: _timeList
                                    .map((item) => new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Chip(
                                            label: new Text(item),
                                            onDeleted: () {
                                              removeTimeChip(item);
                                            },
                                          ),
                                        ))
                                    .toList(),
                              ),
                      ),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        new Padding(
                          padding: new EdgeInsets.only(top: 24.0),
                          child: new MaterialButton(
                            height: 45.0,
                            child: new Text(
                              AppLocalization.of(context).cancelText,
                              style: new TextStyle(
                                color: Colors.grey,
                                fontSize: 18.0,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 24.0),
                          child: new MaterialButton(
                            height: 45.0,
                            child: new Text(
                              AppLocalization.of(context).okText,
                              style: new TextStyle(
                                color: Color(0xFF9E30BF),
                                fontSize: 18.0,
                              ),
                            ),
                            onPressed: () {
                              if (validateFields()) {
                                Navigator.pop(
                                    context,
                                    new AddPillItem(
                                        medicineController.text,
                                        fromController.text,
                                        toController.text,
                                        medicineDoseController.text,
                                        _timeList));
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
