import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/appbar/gradientAppBarWithCross.dart';
import 'package:fhn_flutter/database.dart';
import 'package:fhn_flutter/model/followUpDetailItem.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class FollowUpDetailPage extends StatefulWidget {
  final String from, accessToken;
  final int recordId, followUpId;

  FollowUpDetailPage(
      {Key key,
      @required this.accessToken,
      @required this.from,
      @required this.recordId,
      @required this.followUpId})
      : super(key: key);
  @override
  _FollowUpDetailPageState createState() => _FollowUpDetailPageState();
}

class _FollowUpDetailPageState extends State<FollowUpDetailPage> {
  var db = DatabaseHelper();
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  FollowUpDetailItem followUpDetailItem;

  Future<Null> _handleFollowUpDetailRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          "http://familyhealthnepal.com/api/" +
              widget.from +
              "/" +
              widget.recordId.toString() +
              "/followup/" +
              widget.followUpId.toString() +
              "?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);

          var item = responseBody["data"]["_embedded"]["item"][0];

          deleteFromDB(widget.recordId, widget.followUpId);
          saveToDB(new FollowUpDetailItem(
            recordId: widget.recordId,
            from: widget.from,
            id: widget.followUpId,
            indexNumber: item["index_number"],
            ipNumber: item["ip_number"],
            history: item["patient_history"],
            hospital: item["hospital"],
            doctor: item["doctor"],
            department: item["department"],
            followUpDate: item["followup_date"],
            nextFollowUp: item["next_followup"],
            plan: item["plan"],
            note: item["note"],
          ));
          if (mounted) {
            this.setState(() {
              followUpDetailItem = new FollowUpDetailItem(
                recordId: widget.recordId,
                from: widget.from,
                id: widget.followUpId,
                indexNumber: item["index_number"],
                ipNumber: item["ip_number"],
                history: item["patient_history"],
                hospital: item["hospital"],
                doctor: item["doctor"],
                department: item["department"],
                followUpDate: item["followup_date"],
                nextFollowUp: item["next_followup"],
                plan: item["plan"],
                note: item["note"],
              );
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text("No Internet Connection"),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getFromDB();
      return completer.future;
    }
  }

  Future getFromDB() async {
    followUpDetailItem =
        await db.getFollowUpDetail(widget.recordId, widget.followUpId);
    this.setState(() {});
  }

  Future saveToDB(FollowUpDetailItem item) async {
    await db.saveFollowUpDetail(item);
  }

  Future deleteFromDB(int recordId, int id) async {
    await db.deleteFollowUpDetail(recordId, id);
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    _handleFollowUpDetailRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          new GradientAppBarWithCross("Followup Details", false),
          new Expanded(
            child: new RefreshIndicator(
              onRefresh: _handleFollowUpDetailRefresh,
              child: followUpDetailItem != null
                  ? new Container(
                      padding: new EdgeInsets.all(12.0),
                      child: new ListView(
                        children: <Widget>[
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              new RichText(
                                text: new TextSpan(children: <TextSpan>[
                                  new TextSpan(
                                    text: "INDEX NUMBER: ",
                                    style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  new TextSpan(
                                    text: followUpDetailItem.indexNumber,
                                    style: new TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black,
                                    ),
                                  )
                                ]),
                              ),
                              new RichText(
                                text: new TextSpan(children: <TextSpan>[
                                  new TextSpan(
                                    text: "IP NUMBER: ",
                                    style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  new TextSpan(
                                    text: followUpDetailItem.ipNumber,
                                    style: new TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black,
                                    ),
                                  )
                                ]),
                              ),
                            ],
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 4.0),
                            child: new Divider(
                              height: 10.0,
                              color: Colors.black,
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 8.0),
                          ),
                          new RichText(
                            text: new TextSpan(children: <TextSpan>[
                              new TextSpan(
                                text: "FOLLOWUP DATE: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              new TextSpan(
                                text: followUpDetailItem.followUpDate,
                                style: new TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black,
                                ),
                              )
                            ]),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 4.0),
                            child: new Divider(
                              height: 10.0,
                              color: Colors.black,
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 8.0),
                          ),
                          new Text(
                            followUpDetailItem.hospital.toUpperCase(),
                            style: new TextStyle(
                              color: Colors.blueAccent,
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0,
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 8.0),
                          ),
                          new Text(
                            "DEPARTMENT",
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          new Text(
                            followUpDetailItem.department,
                            style: new TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 8.0),
                          ),
                          new Text(
                            "HISTORY OF PRESENT ILLNESS",
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          new Text(
                            followUpDetailItem.history,
                            style: new TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 8.0),
                          ),
                          new Text(
                            "PLAN",
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          new Text(
                            followUpDetailItem.plan,
                            style: new TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 8.0),
                          ),
                          new Text(
                            "NOTE",
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          new Text(
                            followUpDetailItem.note,
                            style: new TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 12.0),
                            child: new Divider(
                              height: 10.0,
                              color: Colors.black,
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 8.0),
                          ),
                          new RichText(
                            text: new TextSpan(children: <TextSpan>[
                              new TextSpan(
                                text: "NEXT FOLLOWUP DATE: ",
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                              new TextSpan(
                                text: followUpDetailItem.nextFollowUp,
                                style: new TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.black,
                                ),
                              )
                            ]),
                          ),
                        ],
                      ),
                    )
                  : new Center(
                      child: new CircularProgressIndicator(),
                    ),
            ),
          ),
        ],
      ),
    );
  }
}
