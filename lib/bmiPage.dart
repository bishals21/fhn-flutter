import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:flutter/material.dart';

class BMIPage extends StatefulWidget {
  @override
  _BMIPageState createState() => new _BMIPageState();
}

class _BMIPageState extends State<BMIPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  String weight;
  String height;

  double heightFt = 0.0;
  double heightIn = 0.0;
  double heightInInches = 0.0;
  double heightInMeters = 0.0;
  double squareOfHeight = 0.0;
  double bmi = 0.0;

  String _validateWeight(String value) {
    if (value.length == 0) {
      return AppLocalization.of(context).bmiEnterWeight;
    }

    return null;
  }

  String _validateHeight(String value) {
    if (value.length == 0) {
      return AppLocalization.of(context).bmiEnterHeight;
    }

    return null;
  }

  void calculateBMI() {
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save();

      if (height.contains(".")) {
        var heightArray = height.split(".");

        if (heightArray.length > 0) {
          if (heightArray[0].isNotEmpty) {
            heightFt = double.parse(heightArray[0]);
          }
        }

        if (heightArray.length > 1) {
          if (heightArray[1].isNotEmpty) {
            heightIn = double.parse(heightArray[1]);
          }
        }

        heightInInches = (heightFt * 12) + heightIn;
      } else {
        heightFt = double.parse(height);
        heightInInches = (heightFt * 12);
      }

      heightInMeters = heightInInches * 0.025;
      squareOfHeight = heightInMeters * heightInMeters;

      //var bmiRough = double.parse(weight) / squareOfHeight;
      setState(() {
        bmi = double.parse(weight) / squareOfHeight;
      });

    }
  }

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
        hintColor: Color(0xF29E30BF),
        primaryColor: Color(0xF29E30BF),
        inputDecorationTheme: new InputDecorationTheme(
            labelStyle: TextStyle(
          color: Color(0xF29E30BF),
        )));
  }

  @override
  Widget build(BuildContext context) {
    return new Theme(
      data: buildTheme(),
      child: new Scaffold(
        body: Column(
          children: <Widget>[
            new GradientAppBarWithBack(AppLocalization.of(context).bmiTitle),
            new Expanded(
              child: new Container(
                padding: new EdgeInsets.all(16.0),
                child: new Form(
                  key: _formKey,
                  child: new ListView(
                    children: <Widget>[
                      new TextFormField(
                        keyboardType: TextInputType.number,
                        decoration: new InputDecoration(
                          hintText: AppLocalization.of(context).bmiWeightInKg,
                          labelText: AppLocalization.of(context).bmiWeight,
                          hintStyle: new TextStyle(color: Colors.grey),
                          border: new OutlineInputBorder(),
                        ),
                        validator: this._validateWeight,
                        onSaved: (String value) {
                          this.weight = value;
                        },
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 24.0),
                        child: new TextFormField(
                          keyboardType: TextInputType.number,
                          decoration: new InputDecoration(
                            hintText: AppLocalization.of(context).bmiHeightInFt,
                            labelText: AppLocalization.of(context).bmiHeight,
                            hintStyle: new TextStyle(color: Colors.grey),
                            border: new OutlineInputBorder(),
                          ),
                          validator: this._validateHeight,
                          onSaved: (String value) {
                            this.height = value;
                          },
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 24.0),
                        child: new InkWell(
                            splashColor: Colors.black,
                            onTap: calculateBMI,
                            child: new Container(
                                height: 45.0,
                                decoration: new BoxDecoration(
                                  gradient: new LinearGradient(
                                      colors: [
                                        const Color(0xF29E30BF),
                                        const Color(0xF2262BA0)
                                      ],
                                      begin: const FractionalOffset(0.0, 0.0),
                                      end: const FractionalOffset(1.0, 0.0),
                                      stops: [0.0, 1.0],
                                      tileMode: TileMode.clamp),
                                ),
                                child: new Center(
                                  child: new Text(
                                    AppLocalization.of(context).bmiCalculate,
                                    style: new TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                ))),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 24.0),
                        child: new Text(
                          "${bmi.toStringAsFixed(2)}",
                          textAlign: TextAlign.center,
                          style: new TextStyle(
                            fontSize: 26.0,
                            fontWeight: FontWeight.bold,
                            color: bmi >= 19 && bmi <= 25
                                ? Colors.green
                                : Colors.red,
                          ),
                        ),
                      ),
                      new Card(
                        elevation: 5.0,
                        margin: new EdgeInsets.only(top: 24.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            new Padding(
                              padding: new EdgeInsets.all(8.0),
                              child: new Text(
                                AppLocalization.of(context).bmiInfo,
                                style: new TextStyle(
                                  fontSize: 18.0,
                                ),
                              ),
                            ),
                            new Divider(
                              height: 10.0,
                              color: Colors.grey,
                            ),
                            new Padding(
                              padding: new EdgeInsets.all(8.0),
                              child: new Text(
                                AppLocalization.of(context).bmiUnderWeight,
                                style: new TextStyle(
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            new Padding(
                              padding: new EdgeInsets.all(8.0),
                              child: new Text(
                                AppLocalization.of(context).bmiNormalWeight,
                                style: new TextStyle(
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            new Padding(
                              padding: new EdgeInsets.all(8.0),
                              child: new Text(
                                AppLocalization.of(context).bmiOverWeight,
                                style: new TextStyle(
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                            new Padding(
                              padding: new EdgeInsets.all(8.0),
                              child: new Text(
                                AppLocalization.of(context).bmiObese,
                                style: new TextStyle(
                                  fontSize: 14.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
