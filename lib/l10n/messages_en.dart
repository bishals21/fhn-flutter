// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function>{
        "aboutTitle": MessageLookupByLibrary.simpleMessage("About"),
        "addSugarLevelText":
            MessageLookupByLibrary.simpleMessage("Add Sugar Level"),
        "admissionDate":
            MessageLookupByLibrary.simpleMessage("ADMISSION DATE: "),
        "admissionHistoryTitle":
            MessageLookupByLibrary.simpleMessage("Admission History"),
        "age": MessageLookupByLibrary.simpleMessage("AGE: "),
        "bloodPressureTitle":
            MessageLookupByLibrary.simpleMessage("Blood Pressure"),
        "bmiCalculate": MessageLookupByLibrary.simpleMessage("Calculate"),
        "bmiEnterHeight":
            MessageLookupByLibrary.simpleMessage("Please enter height"),
        "bmiEnterWeight":
            MessageLookupByLibrary.simpleMessage("Please enter weight"),
        "bmiHeight": MessageLookupByLibrary.simpleMessage("Height"),
        "bmiHeightInFt": MessageLookupByLibrary.simpleMessage("in ft.in*"),
        "bmiInfo": MessageLookupByLibrary.simpleMessage("BMI Info"),
        "bmiNormalWeight": MessageLookupByLibrary.simpleMessage(
            "Normal weight: If BMI is in between 18.5 & 24.9"),
        "bmiObese":
            MessageLookupByLibrary.simpleMessage("Obese: If BMI is over 30"),
        "bmiOverWeight": MessageLookupByLibrary.simpleMessage(
            "Over weight: If BMI is in between 25 & 29"),
        "bmiTitle": MessageLookupByLibrary.simpleMessage("BMI Calculator"),
        "bmiUnderWeight": MessageLookupByLibrary.simpleMessage(
            "Under weight: If BMI is less than 18.4"),
        "bmiWeight": MessageLookupByLibrary.simpleMessage("Weight"),
        "bmiWeightInKg": MessageLookupByLibrary.simpleMessage("in Kg*"),
        "cancelText": MessageLookupByLibrary.simpleMessage("Cancel"),
        "checkupDate": MessageLookupByLibrary.simpleMessage("CHECKUP DATE: "),
        "checkupFollowupDate":
            MessageLookupByLibrary.simpleMessage("FOLLOWUP DATE: "),
        "checkupHistoryTitle":
            MessageLookupByLibrary.simpleMessage("Checkup History"),
        "chooseGlucosePlasma":
            MessageLookupByLibrary.simpleMessage("Choose Plasma Glucose Test"),
        "dateText": MessageLookupByLibrary.simpleMessage("Date:"),
        "department": MessageLookupByLibrary.simpleMessage("DEPARTMENT"),
        "dischargeSummaryTitle":
            MessageLookupByLibrary.simpleMessage("Discharge Summary"),
        "doctor": MessageLookupByLibrary.simpleMessage("DOCTOR"),
        "enterName": MessageLookupByLibrary.simpleMessage("Enter patient name"),
        "enterPassword":
            MessageLookupByLibrary.simpleMessage("Enter your password"),
        "enterUsername":
            MessageLookupByLibrary.simpleMessage("Enter your username"),
        "fastingRange1": MessageLookupByLibrary.simpleMessage("70 - 100"),
        "fastingRange2": MessageLookupByLibrary.simpleMessage("3.9 - 5.5"),
        "fastingText": MessageLookupByLibrary.simpleMessage("Fasting (FBS)"),
        "followUpDate": MessageLookupByLibrary.simpleMessage("FOLLOWUP DATE"),
        "hospital": MessageLookupByLibrary.simpleMessage("HOSPITAL"),
        "loginTitle": MessageLookupByLibrary.simpleMessage("Login"),
        "logoutConfirmText":
            MessageLookupByLibrary.simpleMessage("Do you want to logout?"),
        "logoutTitle": MessageLookupByLibrary.simpleMessage("Logout"),
        "medicationDose": MessageLookupByLibrary.simpleMessage("DOSE: "),
        "medicationDrug": MessageLookupByLibrary.simpleMessage("DRUG: "),
        "medicationFrom": MessageLookupByLibrary.simpleMessage("FROM: "),
        "medicationTime": MessageLookupByLibrary.simpleMessage("TIME: "),
        "medicationTo": MessageLookupByLibrary.simpleMessage("TO: "),
        "nameIsRequired":
            MessageLookupByLibrary.simpleMessage("Please enter patient name"),
        "nameText": MessageLookupByLibrary.simpleMessage("Patient Name"),
        "noInternetConnection":
            MessageLookupByLibrary.simpleMessage("No Internet Connection"),
        "okText": MessageLookupByLibrary.simpleMessage("Ok"),
        "passwordIsRequired":
            MessageLookupByLibrary.simpleMessage("Please enter password"),
        "passwordText": MessageLookupByLibrary.simpleMessage("Password"),
        "pillAddTime": MessageLookupByLibrary.simpleMessage("Add Time"),
        "pillChooseDate": MessageLookupByLibrary.simpleMessage("Select date >"),
        "pillChooseTime":
            MessageLookupByLibrary.simpleMessage("Please choose time"),
        "pillEmptyAddTime":
            MessageLookupByLibrary.simpleMessage("Please add time"),
        "pillEnterMedicine":
            MessageLookupByLibrary.simpleMessage("Please enter medicine name"),
        "pillEnterMedicineDose":
            MessageLookupByLibrary.simpleMessage("Please enter medicine dose"),
        "pillFromDate": MessageLookupByLibrary.simpleMessage("From"),
        "pillMedicineDose":
            MessageLookupByLibrary.simpleMessage("Medicine Dose"),
        "pillMedicineName": MessageLookupByLibrary.simpleMessage("Medicine"),
        "pillReminderTitle":
            MessageLookupByLibrary.simpleMessage("Pill Reminder"),
        "pillSelectFromDate":
            MessageLookupByLibrary.simpleMessage("Please select from date"),
        "pillSelectToDate":
            MessageLookupByLibrary.simpleMessage("Please select to date"),
        "pillToDate": MessageLookupByLibrary.simpleMessage("To"),
        "ppbsRange1": MessageLookupByLibrary.simpleMessage("Less than 140"),
        "ppbsRange2": MessageLookupByLibrary.simpleMessage("3.5 - 7.8"),
        "ppbsText": MessageLookupByLibrary.simpleMessage("PPBS"),
        "randomRange1": MessageLookupByLibrary.simpleMessage("120 mg/dL"),
        "randomRange2": MessageLookupByLibrary.simpleMessage("3.5 - 7.8"),
        "randomText": MessageLookupByLibrary.simpleMessage("Random (RBS)"),
        "reportTitle": MessageLookupByLibrary.simpleMessage("Report"),
        "setPillReminder":
            MessageLookupByLibrary.simpleMessage("Set Pill Reminder"),
        "setSugarLevel":
            MessageLookupByLibrary.simpleMessage("Set Sugar Level"),
        "setText": MessageLookupByLibrary.simpleMessage("Set"),
        "showAll": MessageLookupByLibrary.simpleMessage("SHOW ALL"),
        "signingText": MessageLookupByLibrary.simpleMessage(
            "By signing in, you agree to the terms and conditions."),
        "sugarLevel": MessageLookupByLibrary.simpleMessage("SUGAR LEVEL"),
        "sugarLevelDate": MessageLookupByLibrary.simpleMessage("DATE"),
        "sugarLevelSelectUnit":
            MessageLookupByLibrary.simpleMessage("Select Unit: "),
        "sugarLevelText": MessageLookupByLibrary.simpleMessage("Sugar Level"),
        "sugarLevelTitle": MessageLookupByLibrary.simpleMessage("Sugar Level"),
        "sugarRange": MessageLookupByLibrary.simpleMessage("RANGE"),
        "sugarType": MessageLookupByLibrary.simpleMessage("SUGAR TYPE"),
        "sugarUnit": MessageLookupByLibrary.simpleMessage("UNIT"),
        "title": MessageLookupByLibrary.simpleMessage("Mobile Health Record"),
        "usernameIsRequired":
            MessageLookupByLibrary.simpleMessage("Please enter username"),
        "usernameText": MessageLookupByLibrary.simpleMessage("Username"),
        "uploadReport": MessageLookupByLibrary.simpleMessage("Upload Report"),
        "ward": MessageLookupByLibrary.simpleMessage("WARD")
      };
}
