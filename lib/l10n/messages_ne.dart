// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ne locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'ne';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function>{
        "aboutTitle": MessageLookupByLibrary.simpleMessage("हाम्रो बारेमा"),
        "addSugarLevelText": MessageLookupByLibrary.simpleMessage(
            "रक्तचीनी स्तरको मात्रा लेख्नुहोस्"),
        "admissionDate": MessageLookupByLibrary.simpleMessage("भर्ना मिति: "),
        "admissionHistoryTitle":
            MessageLookupByLibrary.simpleMessage("भर्ना इतिहास"),
        "age": MessageLookupByLibrary.simpleMessage("उमेर: "),
        "bloodPressureTitle": MessageLookupByLibrary.simpleMessage("रक्तचाप"),
        "bmiCalculate": MessageLookupByLibrary.simpleMessage("गणना"),
        "bmiEnterHeight": MessageLookupByLibrary.simpleMessage(
            "कृपया ऊँचाई प्रविष्ट गर्नुहोस्"),
        "bmiEnterWeight": MessageLookupByLibrary.simpleMessage(
            "कृपया वजन प्रविष्ट गर्नुहोस्"),
        "bmiHeight": MessageLookupByLibrary.simpleMessage("उचाइ"),
        "bmiHeightInFt": MessageLookupByLibrary.simpleMessage("फिट. इन्च*"),
        "bmiInfo": MessageLookupByLibrary.simpleMessage("बीएमआई जानकारी"),
        "bmiNormalWeight": MessageLookupByLibrary.simpleMessage(
            "सामान्य वजन: बीएमआई १८.५ र २४.९ बीच छ भने"),
        "bmiObese": MessageLookupByLibrary.simpleMessage(
            "अत्यन्त मोटो: बीएमआई ३० भन्दा बढी छ भने"),
        "bmiOverWeight": MessageLookupByLibrary.simpleMessage(
            "बढी वजन: बीएमआई २५ र २९ बीच छ भने"),
        "bmiTitle": MessageLookupByLibrary.simpleMessage("बीएमआई क्याल्कुलेटर"),
        "bmiUnderWeight": MessageLookupByLibrary.simpleMessage(
            "कम वजन: बीएमआई १८.४ भन्दा कम छ भने"),
        "bmiWeight": MessageLookupByLibrary.simpleMessage("वजन"),
        "bmiWeightInKg": MessageLookupByLibrary.simpleMessage("के.जि*"),
        "cancelText": MessageLookupByLibrary.simpleMessage("रद्द"),
        "checkupDate": MessageLookupByLibrary.simpleMessage("जाँच मिति: "),
        "checkupFollowupDate":
            MessageLookupByLibrary.simpleMessage("पुन:जाँच मिति: "),
        "checkupHistoryTitle":
            MessageLookupByLibrary.simpleMessage("जाँच इतिहास"),
        "chooseGlucosePlasma": MessageLookupByLibrary.simpleMessage(
            "प्लाज्मा ग्लूकोज परीक्षण छनौट गर्नुहोस्"),
        "dateText": MessageLookupByLibrary.simpleMessage("मिति: "),
        "department": MessageLookupByLibrary.simpleMessage("विभाग"),
        "dischargeSummaryTitle":
            MessageLookupByLibrary.simpleMessage("डिस्चार्ज सारांश"),
        "doctor": MessageLookupByLibrary.simpleMessage("डाक्टर"),
        "enterName": MessageLookupByLibrary.simpleMessage("बिरामीको नाम"),
        "enterPassword": MessageLookupByLibrary.simpleMessage(
            "आफ्नो पासवर्ड प्रविष्ट गर्नुहोस्"),
        "enterUsername": MessageLookupByLibrary.simpleMessage(
            "आफ्नो प्रयोगकर्ता नाम प्रविष्ट गर्नुहोस्"),
        "fastingRange1": MessageLookupByLibrary.simpleMessage("७० - १००"),
        "fastingRange2": MessageLookupByLibrary.simpleMessage("३.९ - ५.५"),
        "fastingText": MessageLookupByLibrary.simpleMessage("खाली पेटमा"),
        "followUpDate": MessageLookupByLibrary.simpleMessage("पुन:जाँच मिति"),
        "hospital": MessageLookupByLibrary.simpleMessage("अस्पताल"),
        "loginTitle": MessageLookupByLibrary.simpleMessage("सदस्य लग-इन"),
        "logoutConfirmText": MessageLookupByLibrary.simpleMessage(
            "के तपाईं लग आउट गर्न चाहनुहुन्छ?"),
        "logoutTitle":
            MessageLookupByLibrary.simpleMessage("बाहिर निस्कनुहुन्छ"),
        "medicationDose": MessageLookupByLibrary.simpleMessage("मात्रा: "),
        "medicationDrug": MessageLookupByLibrary.simpleMessage("औषधी: "),
        "medicationFrom": MessageLookupByLibrary.simpleMessage("देखि: "),
        "medicationTime": MessageLookupByLibrary.simpleMessage("समय: "),
        "medicationTo": MessageLookupByLibrary.simpleMessage("सम्म: "),
        "nameIsRequired":
            MessageLookupByLibrary.simpleMessage("बिरामीको नाम आवश्यक छ"),
        "nameText": MessageLookupByLibrary.simpleMessage("बिरामीको नाम"),
        "noInternetConnection":
            MessageLookupByLibrary.simpleMessage("इन्टरनेट उपलब्ध छैन ।"),
        "okText": MessageLookupByLibrary.simpleMessage("ठिक छ"),
        "passwordIsRequired": MessageLookupByLibrary.simpleMessage(
            "कृपया पासवर्ड प्रविष्ट गर्नुहोस्"),
        "passwordText": MessageLookupByLibrary.simpleMessage("पासवर्ड"),
        "pillAddTime": MessageLookupByLibrary.simpleMessage("समय थप गर्नुहोस्"),
        "pillChooseDate":
            MessageLookupByLibrary.simpleMessage("मिति रोज्नुहोस् >"),
        "pillChooseTime": MessageLookupByLibrary.simpleMessage(
            "कृपया औषधीको समय स्पष्ट गर्नुहोस्"),
        "pillEmptyAddTime":
            MessageLookupByLibrary.simpleMessage("कृपया समय थप गर्नुहोस्"),
        "pillEnterMedicine": MessageLookupByLibrary.simpleMessage(
            "कृपया औषधीको नाम स्पष्ट गर्नुहोस्"),
        "pillEnterMedicineDose": MessageLookupByLibrary.simpleMessage(
            "कृपया औषधीको मात्रा स्पष्ट गर्नुहोस्"),
        "pillFromDate": MessageLookupByLibrary.simpleMessage("देखि"),
        "pillMedicineDose": MessageLookupByLibrary.simpleMessage("मात्रा"),
        "pillMedicineName": MessageLookupByLibrary.simpleMessage("औषधी"),
        "pillReminderTitle": MessageLookupByLibrary.simpleMessage("थप औषधी"),
        "pillSelectFromDate": MessageLookupByLibrary.simpleMessage(
            "कृपया मिति स्���ष्ट गर्नुहोस्"),
        "pillSelectToDate":
            MessageLookupByLibrary.simpleMessage("कृपया मिति स्पष्ट गर्नुहोस्"),
        "pillToDate": MessageLookupByLibrary.simpleMessage("सम्म"),
        "ppbsRange1": MessageLookupByLibrary.simpleMessage("१४० भन्दा कम"),
        "ppbsRange2": MessageLookupByLibrary.simpleMessage("३.५ - ७.८"),
        "ppbsText": MessageLookupByLibrary.simpleMessage("खाना पछी"),
        "randomRange1": MessageLookupByLibrary.simpleMessage("१२० mg/dL"),
        "randomRange2": MessageLookupByLibrary.simpleMessage("३.५ - ७.८"),
        "randomText": MessageLookupByLibrary.simpleMessage("अनियमित"),
        "reportTitle": MessageLookupByLibrary.simpleMessage("रिपोर्ट"),
        "setPillReminder":
            MessageLookupByLibrary.simpleMessage("थप औषधी मिलाउने"),
        "setSugarLevel":
            MessageLookupByLibrary.simpleMessage("रक्तचीनी स्तर मिलाउने"),
        "setText": MessageLookupByLibrary.simpleMessage("सेट"),
        "showAll": MessageLookupByLibrary.simpleMessage("सबै हेर्नुहोस्"),
        "signingText": MessageLookupByLibrary.simpleMessage(
            "लग-इन गरेर तपाईं नियम र सर्तहरु सहमत गर्नुहुन्छ ।"),
        "sugarLevel": MessageLookupByLibrary.simpleMessage("रक्तचीनी स्तर"),
        "sugarLevelDate": MessageLookupByLibrary.simpleMessage("मिति"),
        "sugarLevelSelectUnit":
            MessageLookupByLibrary.simpleMessage("यूनिट छनौट गर्नुहोस्: "),
        "sugarLevelText": MessageLookupByLibrary.simpleMessage("रक्तचीनी स्तर"),
        "sugarLevelTitle":
            MessageLookupByLibrary.simpleMessage("रक्तचीनी स्तर"),
        "sugarRange": MessageLookupByLibrary.simpleMessage("सीमा"),
        "sugarType": MessageLookupByLibrary.simpleMessage("रक्तचीनी प्रकार"),
        "sugarUnit": MessageLookupByLibrary.simpleMessage("यूनिट"),
        "title":
            MessageLookupByLibrary.simpleMessage("Electronic Patient Record"),
        "usernameIsRequired": MessageLookupByLibrary.simpleMessage(
            "कृपया प्रयोगकर्ता नाम प्रविष्ट गर्नुहोस्"),
        "usernameText":
            MessageLookupByLibrary.simpleMessage("प्रयोगकर्ताको नाम"),
        "uploadReport": MessageLookupByLibrary.simpleMessage("अपलोड रिपोर्ट"),
        "ward": MessageLookupByLibrary.simpleMessage("वार्ड"),
      };
}
