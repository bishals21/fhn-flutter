import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/database.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/videoItem.dart';
import 'package:fhn_flutter/videoDetailPage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class VideoListPage extends StatefulWidget {
  final String from, accessToken;
  final int recordId;

  VideoListPage(
      {Key key,
      @required this.accessToken,
      @required this.from,
      @required this.recordId})
      : super(key: key);
  @override
  _VideoListPageState createState() => _VideoListPageState();
}

class _VideoListPageState extends State<VideoListPage> {
  var db = DatabaseHelper();

  List<VideoItem> list = new List();

  List<VideoAttachment> videoList = new List();

  Future<Null> _handleVideoListRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    List<VideoItem> listOfItem = new List();
    List<VideoAttachment> videoAttachments = new List();

    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          "http://familyhealthnepal.com/api/" +
              widget.from +
              "/" +
              widget.recordId.toString() +
              "/videos?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"]["_embedded"]["item"];
          deleteFromDB(widget.recordId, widget.from);
          deleteAttachmentFromDB(widget.recordId);
          for (var item in data) {
            listOfItem.add(new VideoItem(
                recordId: widget.recordId,
                from: widget.from,
                investigationId: item["investigation_id"],
                investigationDate: item["investigation_date"],
                hospital: item["hospital"],
                name: item["name"],
                description: item["description"]));

            saveToDB(new VideoItem(
                recordId: widget.recordId,
                from: widget.from,
                investigationId: item["investigation_id"],
                investigationDate: item["investigation_date"],
                hospital: item["hospital"],
                name: item["name"],
                description: item["description"]));

            var videos = item["videos"];
            for (var videoItem in videos) {
              videoAttachments.add(new VideoAttachment(
                recordId: widget.recordId,
                investigationId: item["investigation_id"],
                title: videoItem["title"],
                video: videoItem["video"],
                thumb: videoItem["thumb"],
              ));

              saveAttachmentToDB(new VideoAttachment(
                recordId: widget.recordId,
                investigationId: item["investigation_id"],
                title: videoItem["title"],
                video: videoItem["video"],
                thumb: videoItem["thumb"],
              ));
            }
          }

          if (mounted) {
            this.setState(() {
              list = listOfItem;
              videoList = videoAttachments;
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(AppLocalization.of(context).noInternetConnection),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getFromDB();
      return completer.future;
    }
  }

  Future getFromDB() async {
    list = await db.getVideo(widget.recordId, widget.from);
    this.setState(() {});
  }

  Future<List<VideoAttachment>> getAttachmentFromDB(int investigationId) async {
    List<VideoAttachment> videoList =
        await db.getVideoAttachment(investigationId);
    return videoList;
  }

  Future saveToDB(VideoItem item) async {
    await db.saveVideo(item);
  }

  Future saveAttachmentToDB(VideoAttachment item) async {
    await db.saveVideoAttachment(item);
  }

  Future deleteFromDB(int recordId, String from) async {
    await db.deleteVideo(recordId, from);
  }

  Future deleteAttachmentFromDB(int recordId) async {
    await db.deleteVideoAttachment(recordId);
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  Future<List<VideoAttachment>> _getVideoList(int id) async {
    List<VideoAttachment> list = new List();
    bool isConnected = await checkConnection();
    if (isConnected) {
      list = videoList.where((i) => i.investigationId == id).toList();
    } else {
      list = await getAttachmentFromDB(id);
    }

    return list;
  }

  _navigateWithList(int id, String name) {
    _getVideoList(id).then(
      (list) => Navigator.of(context).push(new MaterialPageRoute(
        builder: (context) => new VideoDetailPage(
          title: name,
          list: list,
        ),
      )),
    );
  }

  @override
  void initState() {
    _handleVideoListRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new RefreshIndicator(
      onRefresh: _handleVideoListRefresh,
      child: new ListView.builder(
          padding: new EdgeInsets.all(8.0),
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            final VideoItem item = list[index];

            return new GestureDetector(
              onTap: () => _navigateWithList(item.investigationId, item.name),
              child: new Card(
                child: new Container(
                  padding: new EdgeInsets.all(8.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        item.name.toUpperCase(),
                        style: new TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 4.0),
                      ),
                      new Divider(
                        height: 10.0,
                        color: Colors.black,
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new Text(
                        item.description,
                        style: new TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new Text(
                        AppLocalization.of(context).dateText +
                            " " +
                            item.investigationDate,
                        style: new TextStyle(
                          color: Colors.black87,
                          fontSize: 14.0,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new Align(
                        alignment: Alignment.centerRight,
                        child: new Text(
                          AppLocalization.of(context).showAll,
                          textAlign: TextAlign.end,
                          style: new TextStyle(
                            color: Colors.blueAccent,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
