import 'dart:async';
import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:fhn_flutter/bloodPressurePage.dart';
import 'package:fhn_flutter/checkupHistoryPage.dart';
import 'package:fhn_flutter/dischargeSummaryPage.dart';
import 'package:fhn_flutter/galleryPage.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/medicationPage.dart';
import 'package:fhn_flutter/reportPage.dart';
import 'package:fhn_flutter/sugarLevelPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'admissionHistoryPage.dart';
import 'bmiPage.dart';
import 'database.dart';
import 'loginPage.dart';
import 'model/menuItem.dart';
import 'model/userAccessToken.dart';
import 'pillReminderPage.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        localizationsDelegates: [
          const AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          const FallbackMaterialLocalisationsDelegate(),
        ],
        supportedLocales: [
          Locale('en', ""),
          Locale('ne', ""),
        ],
        onGenerateTitle: (BuildContext context) =>
            AppLocalization.of(context).title,
        debugShowCheckedModeBanner: false,
        home: MyHomePage(title: 'Mobile Health Record'),
        routes: <String, WidgetBuilder>{
          "/BMI": (BuildContext context) => new BMIPage(),
          "/PillReminder": (BuildContext context) => new Scaffold(
                body: new PillReminderPage(),
              ),
        });
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var db = DatabaseHelper();

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  String pageTitle = "";
  List<MenuItem> menuItemList;
  bool isLoggedIn, isLoaded = false, _isLocaleSelected;

  UserAccessToken userAccessToken;

  final String aboutString = "<html>" +
      "<body>" +
      "<p><b>About:</b>" +
      "<br>Family Health Nepal [FHN] is a service oriented health organization focused towards you and your loved ones personal health. " +
      "FHN is a hub organization that holds many well established specialized as well as general hospitals that will make the health service easily accessible at an affordable cost.</p>" +
      "<br>" +
      "<b>Benefits:</b>" +
      "<br>" +
      "<br>1.) Best treatment in the best hospital from best doctors." +
      "<br>2.) Time saving that reduces the burden of awaiting the date of appointment and availability of doctors." +
      "<br>3.) Reduction in out of pocket payment." +
      "<br>4.) Access to view ones medical record from anywhere at any time." +
      "<br>5.) Free from burden of carrying tremendous past history record for proper diagnosis of disease." +
      "<br><br>" +
      "<b>Contact</b>" +
      "<br>address : Maitighar 11, Kathmandu, Nepal." +
      "<br>" +
      "hot-line :+977-9801203380" +
      "<br>email1 : info@familyhealthnepal.com" +
      "<br>email2 : familyhealthnepal@gmail.com" +
      "</body>" +
      "</html>";

  List<MenuItem> getNormalList() {
    List<MenuItem> menuItemList = new List();
    menuItemList.add(
        new MenuItem("BMI Calculator", "assets/bmiCalculator.png", 0xFF7A7A7B));
    menuItemList
        .add(new MenuItem("Blood Group", "assets/bloodGroup.png", 0xFFD27402));
    menuItemList.add(
        new MenuItem("Pill Reminder", "assets/pillReminder.png", 0xFF017668));
    menuItemList.add(new MenuItem("Report", "assets/report.png", 0xFF0B8043));
    menuItemList
        .add(new MenuItem("Sugar Level", "assets/sugarLevel.png", 0xFFD04132));
    menuItemList.add(new MenuItem("Login", "assets/userLogin.png", 0xFF0B8043));

    return menuItemList;
  }

  List<MenuItem> getLoggedInList() {
    List<MenuItem> menuItemList = new List();
    menuItemList.add(new MenuItem(
        "Admission History", "assets/admissionHistory.png", 0xFF7A7A7B));
    menuItemList
        .add(new MenuItem("Checkup History", "assets/checkup.png", 0xFFD27402));
    menuItemList.add(
        new MenuItem("Pill Reminder", "assets/pillReminder.png", 0xFF017668));
    menuItemList
        .add(new MenuItem("Medication", "assets/medication.png", 0xFF0B8043));
    menuItemList.add(new MenuItem("Gallery", "assets/gallery.png", 0xFFD04132));
    menuItemList.add(new MenuItem(
        "Discharge Summary", "assets/dischargeSummary.png", 0xFF0B8043));

    return menuItemList;
  }

  Future<Null> checkUserSession() async {
    final Completer<Null> completer = new Completer<Null>();
    userAccessToken = await db.getUserAccessToken();
    if (userAccessToken != null) {
      setState(() {
        menuItemList = getLoggedInList();
        isLoggedIn = true;
        isLoaded = true;
        completer.complete(null);
      });
    } else {
      setState(() {
        menuItemList = getNormalList();
        isLoggedIn = false;
        isLoaded = true;
        completer.complete(null);
      });
    }

    return completer.future;
  }

  Future<Null> _changeLocaleValue(bool isChanged) async {
    final Completer<Null> completer = new Completer<Null>();
    final SharedPreferences prefs = await _prefs;
    bool isSuccess = await prefs.setBool('isLocaleSelected', isChanged);

    setState(() {
      if (isSuccess) {
        _isLocaleSelected = isChanged;
        _changeLocale(_isLocaleSelected);
      }

      completer.complete(null);
    });

    return completer.future;
  }

  Future<Null> _getSavedLocaleValue() async {
    final Completer<Null> completer = new Completer<Null>();
    final SharedPreferences prefs = await _prefs;
    setState(() {
      _isLocaleSelected = prefs.getBool('isLocaleSelected') ?? false;
      _changeLocale(_isLocaleSelected);
      completer.complete(null);
    });

    return completer.future;
  }

  _changeLocale(bool isLocaleChanged) {
    if (isLocaleChanged) {
      Locale locale = Locale('ne', '');
      AppLocalization.load(locale);
      setState(() {});
    } else {
      Locale locale = Locale('en', '');
      AppLocalization.load(locale);
      setState(() {});
    }
  }

  @override
  void initState() {
    checkUserSession();
    _getSavedLocaleValue();
    super.initState();
  }

  @override
  void dispose() {
    db.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return isLoaded && _isLocaleSelected != null
        ? Scaffold(
            key: _scaffoldKey,
            drawer: new Drawer(
              child: new ListView(
                children: <Widget>[
                  new Container(
                    height: 120.0,
                    child: new Center(
                      child: new CircleAvatar(
                        backgroundImage: new AssetImage("assets/annapurna.jpg"),
                        radius: 50.0,
                      ),
                    ),
                  ),
                  new ListTile(
                    leading: new Image.asset(
                      "assets/bmiCalculator.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                    title: new Text(
                      AppLocalization.of(context).bmiTitle,
                      style: new TextStyle(
                          fontSize: _isLocaleSelected ? 16.0 : 14.0),
                    ),
                    onTap: () => Navigator.pushNamed(context, "/BMI"),
                  ),
                  new ListTile(
                    leading: new Image.asset(
                      "assets/pillReminder.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                    title: new Text(
                      AppLocalization.of(context).pillReminderTitle,
                      style: new TextStyle(
                          fontSize: _isLocaleSelected ? 16.0 : 14.0),
                    ),
                    onTap: () => Navigator.pushNamed(context, "/PillReminder"),
                  ),
                  new ListTile(
                    leading: new Image.asset(
                      "assets/bloodPressure.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                    title: new Text(
                      AppLocalization.of(context).bloodPressureTitle,
                      style: new TextStyle(
                          fontSize: _isLocaleSelected ? 16.0 : 14.0),
                    ),
                    onTap: () => Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (context) => new BloodPressurePage())),
                  ),
                  new ListTile(
                    leading: new Image.asset(
                      "assets/sugarLevel.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                    title: new Text(
                      AppLocalization.of(context).sugarLevelTitle,
                      style: new TextStyle(
                          fontSize: _isLocaleSelected ? 16.0 : 14.0),
                    ),
                    onTap: () => Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (context) => new SugarLevelPage())),
                  ),
                  new ListTile(
                    leading: new Image.asset(
                      "assets/report.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                    title: new Text(
                      AppLocalization.of(context).reportTitle,
                      style: new TextStyle(
                          fontSize: _isLocaleSelected ? 16.0 : 14.0),
                    ),
                    onTap: () => Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (context) => new ReportPage())),
                  ),

                  new ListTile(
                    leading: new Image.asset(
                      "assets/report.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                    title: new Text(
                      AppLocalization.of(context).uploadReport,
                      style: new TextStyle(
                          fontSize: _isLocaleSelected ? 16.0 : 14.0),
                    ),
                    // onTap: () => Navigator.of(context).push(
                    //     new MaterialPageRoute(
                    //         builder: (context) => new ReportPage())),
                  ),

                  new Divider(
                    color: Colors.grey,
                  ),
                  //   new ListTile(
                  //     leading: new Image.asset(
                  //       "assets/about.png",
                  //       width: 30.0,
                  //       height: 30.0,
                  //     ),
                  //     title: new Text(
                  //       AppLocalization.of(context).aboutTitle,
                  //       style: new TextStyle(
                  //           fontSize: _isLocaleSelected ? 16.0 : 14.0),
                  //     ),
                  //     onTap: () =>
                  //         Navigator.of(context).push(new MaterialPageRoute(
                  //             builder: (context) => new WebviewScaffold(
                  //                   url: new Uri.dataFromString(aboutString,
                  //                           mimeType: 'text/html')
                  //                       .toString(),
                  //                   appBar: new AppBar(
                  //                     backgroundColor: Color(0xFF9E30BF),
                  //                     title: new Text(
                  //                       AppLocalization.of(context).aboutTitle,
                  //                     ),
                  //                   ),
                  //                 ),
                  //             fullscreenDialog: true)),
                  //   ),
                  new SwitchListTile(
                    value: _isLocaleSelected,
                    secondary: new Image.asset(
                      "assets/language.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                    onChanged: (bool value) {
                      _changeLocaleValue(value);
                    },
                    title: new Text(
                      "नेपाली",
                      style: new TextStyle(fontSize: 16.0),
                    ),
                  ),
                  isLoggedIn
                      ? new ListTile(
                          leading: new Image.asset(
                            "assets/userLogout.png",
                            width: 30.0,
                            height: 30.0,
                          ),
                          title: new Text(
                            AppLocalization.of(context).logoutTitle,
                            style: new TextStyle(
                                fontSize: _isLocaleSelected ? 16.0 : 14.0),
                          ),
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (context) {
                                  return new AlertDialog(
                                    title: new Text(AppLocalization.of(context)
                                        .logoutTitle),
                                    content: new Text(
                                        AppLocalization.of(context)
                                            .logoutConfirmText),
                                    actions: <Widget>[
                                      new FlatButton(
                                        child: new Text(
                                          AppLocalization.of(context)
                                              .cancelText,
                                          style: new TextStyle(
                                            color: Colors.red,
                                          ),
                                        ),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                      ),
                                      new FlatButton(
                                        child: new Text(
                                            AppLocalization.of(context).okText),
                                        onPressed: () {
                                          Navigator.pop(context);
                                          _logOut();
                                        },
                                      )
                                    ],
                                  );
                                });
                          },
                        )
                      : new ListTile(
                          leading: new Image.asset(
                            "assets/userLogin.png",
                            width: 30.0,
                            height: 30.0,
                          ),
                          title: new Text(
                            AppLocalization.of(context).loginTitle,
                            style: new TextStyle(
                                fontSize: _isLocaleSelected ? 16.0 : 14.0),
                          ),
                          onTap: () {
                            _navigateToLogin(context);
                          },
                        ),
                ],
              ),
            ),
            body: new Container(
                child: new Column(
              children: <Widget>[
                new SizedBox(
                  height: 210.0,
                  child: new Stack(
                    children: <Widget>[
                      new Container(
                        decoration: new BoxDecoration(
                          gradient: new LinearGradient(
                              begin: const FractionalOffset(0.0, 0.0),
                              end: const FractionalOffset(1.0, 0.0),
                              colors: <Color>[
                                const Color(0xFF9E30BF),
                                const Color(0xFF262BA0)
                              ]),
                        ),
                      ),
                      new Positioned(
                        top: 30.0,
                        left: 0.0,
                        child: new IconButton(
                          icon: new Icon(
                            Icons.menu,
                            size: 28.0,
                            color: Colors.white,
                          ),
                          onPressed: () =>
                              _scaffoldKey.currentState.openDrawer(),
                        ),
                      ),
                      new Positioned(
                        left: 0.0,
                        bottom: 0.0,
                        right: 0.0,
                        child: new Column(
                          //mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CircleAvatar(
                              backgroundImage: userAccessToken != null
                                  ? new NetworkImage(userAccessToken.image)
                                  : new AssetImage("assets/annapurna.jpg"),
                              radius: 50.0,
                              backgroundColor: Colors.grey,
                            ),
                            new Padding(
                              padding:
                                  new EdgeInsets.only(top: 5.0, bottom: 40.0),
                              child: new Text(
                                isLoggedIn
                                    ? userAccessToken.fhnId
                                    : "FREE USER",
                                style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),

                new Expanded(
                  child: CustomScrollView(
                    slivers: <Widget>[
                      new SliverGrid.count(
                        crossAxisCount: 3,
                        childAspectRatio: 0.9,
                        children: new List.generate(6, (index) {
                          return new GestureDetector(
                            onTap: () {
                              if (isLoggedIn) {
                                if (index == 0) {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new AdmissionHistoryPage(
                                                accessToken:
                                                    userAccessToken.accessToken,
                                              )));
                                } else if (index == 1) {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new CheckupHistoryPage(
                                                accessToken:
                                                    userAccessToken.accessToken,
                                              )));
                                } else if (index == 2) {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new PillReminderPage()));
                                } else if (index == 3) {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new MedicationAlertPage(
                                                accessToken:
                                                    userAccessToken.accessToken,
                                              )));
                                } else if (index == 4) {
                                  Navigator.of(context)
                                      .push(new MaterialPageRoute(
                                          builder: (context) => new GalleryPage(
                                                accessToken:
                                                    userAccessToken.accessToken,
                                              )));
                                } else if (index == 5) {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new DischargeSummaryPage(
                                                accessToken:
                                                    userAccessToken.accessToken,
                                              )));
                                }
                              } else {
                                if (index == 0) {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) => new BMIPage()));
                                } else if (index == 1) {
                                  //   Navigator.of(context).push(new MaterialPageRoute(
                                  //       builder: (context) => new CheckupHistoryPage(
                                  //             accessToken:
                                  //                 userAccessToken.accessToken,
                                  //           )));
                                } else if (index == 2) {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new PillReminderPage()));
                                } else if (index == 3) {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new ReportPage()));
                                } else if (index == 4) {
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new SugarLevelPage()));
                                } else if (index == 5) {
                                  _navigateToLogin(context);
                                }
                              }
                            },
                            child: new Card(
                              elevation: 5.0,
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Image.asset(
                                    menuItemList[index].icon,
                                    height: 65.0,
                                    width: 65.0,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.all(8.0),
                                    child: new Text(
                                      menuItemList[index].name,
                                      textAlign: TextAlign.center,
                                      style: new TextStyle(
                                        fontSize: 14.0,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        }),
                      ),
                      new SliverPadding(
                        padding: new EdgeInsets.only(top: 8.0, bottom: 8.0),
                        sliver: new SliverToBoxAdapter(
                          child: new SizedBox(
                            height: 200.0,
                            child: new CarouselSlider(
                              items: [
                                new Container(
                                  margin: new EdgeInsets.symmetric(
                                    horizontal: 5.0,
                                  ),
                                  decoration: new BoxDecoration(
                                      borderRadius: new BorderRadius.all(
                                          new Radius.circular(5.0)),
                                      image: new DecorationImage(
                                          image: new AssetImage(
                                              "assets/medicalImage.jpg"),
                                          fit: BoxFit.cover)),
                                ),
                                new Container(
                                  margin: new EdgeInsets.symmetric(
                                    horizontal: 5.0,
                                  ),
                                  decoration: new BoxDecoration(
                                      borderRadius: new BorderRadius.all(
                                          new Radius.circular(5.0)),
                                      image: new DecorationImage(
                                          image: new AssetImage(
                                              "assets/medicalImage.jpg"),
                                          fit: BoxFit.cover)),
                                ),
                                new Container(
                                  margin: new EdgeInsets.symmetric(
                                    horizontal: 5.0,
                                  ),
                                  decoration: new BoxDecoration(
                                      borderRadius: new BorderRadius.all(
                                          new Radius.circular(5.0)),
                                      image: new DecorationImage(
                                          image: new AssetImage(
                                              "assets/medicalImage.jpg"),
                                          fit: BoxFit.cover)),
                                ),
                              ],
                              height: 200.0,
                              viewportFraction: 0.9,
                              aspectRatio: 2.0,
                              autoPlay: true,
                              autoPlayDuration: new Duration(seconds: 2),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                // new Container(
                //   height: 150.0,
                //   color: Colors.green,
                // )
              ],
            )),
          )
        : new Scaffold(
            body: new Center(
              child: new CircularProgressIndicator(),
            ),
          );
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  _navigateToLogin(BuildContext context) async {
    final result = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new LoginPage(), fullscreenDialog: true));

    if (result == "LoggedIn") {
      checkUserSession();
    }
  }

  _logOut() async {
    await db.deleteUserAccessToken(userAccessToken.id);
    await db.deleteAdmissionHistory();
    await db.deleteCheckupHistory();
    await db.deleteDischargeSummary();
    await db.deleteAllAdmissionDetailItemA();
    await db.deleteAllAdmissionDetailItemC();
    await db.deleteAllLabTest();
    await db.deleteAllFollowUp();
    await db.deleteAllInsideMedication();
    await db.deleteAllVideo();
    await db.deleteAllVideoAttachment();
    await db.deleteAllLabTestDetail();
    await db.deleteAllImagingDetail();
    await db.deleteAllImageAttachment();
    await db.deleteAllFollowUpDetail();
    await db.deleteAllInsideMedication();
    await db.deleteMedicationItem();
    await db.deleteAllDischargeSummaryDetail();
    checkUserSession();
  }
}
