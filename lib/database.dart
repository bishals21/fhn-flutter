import 'dart:async';
import 'dart:io';

import 'package:fhn_flutter/model/admissionDetailItem.dart';
import 'package:fhn_flutter/model/admissionDetailItemC.dart';
import 'package:fhn_flutter/model/admissionHistoryItem.dart';
import 'package:fhn_flutter/model/bloodPressureItem.dart';
import 'package:fhn_flutter/model/checkupHistoryItem.dart';
import 'package:fhn_flutter/model/dischargeItem.dart';
import 'package:fhn_flutter/model/followUpDetailItem.dart';
import 'package:fhn_flutter/model/followUpItem.dart';
import 'package:fhn_flutter/model/imagingDetailItem.dart';
import 'package:fhn_flutter/model/insideMedicationItem.dart';
import 'package:fhn_flutter/model/labTestDetailItem.dart';
import 'package:fhn_flutter/model/labTestItem.dart';
import 'package:fhn_flutter/model/medicationItem.dart';
import 'package:fhn_flutter/model/sugarLevelItem.dart';
import 'package:fhn_flutter/model/videoItem.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:synchronized/synchronized.dart';

import 'model/pillReminderItem.dart';
import 'model/userAccessToken.dart';

class DatabaseHelper {
  static Database _db;
  final _lock = new Lock();

  Future<Database> get db async {
    // if (_db != null) {
    //   return _db;
    // }
    // _db = await initDb();
    // return _db;
    if (_db == null) {
      await _lock.synchronized(() async {
        // Check again once entering the synchronized block
        if (_db == null) {
          _db = await initDb();
        }
      });
    }
    return _db;
  }

  initDb() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentDirectory.path, "fhn.db");
    var db = await openDatabase(path, version: 1, onCreate: _onCreate);
    return db;
  }

  void _onCreate(Database db, int version) async {
    await db.execute("CREATE TABLE PillReminder ("
        "id INTEGER PRIMARY KEY,"
        "${PillReminderItem.dbMedicineName} TEXT,"
        "${PillReminderItem.dbFromDate} TEXT,"
        "${PillReminderItem.dbToDate} TEXT,"
        "${PillReminderItem.dbMedicineDose} TEXT,"
        "${PillReminderItem.dbTime} TEXT"
        ")");

    await db.execute("CREATE TABLE BloodPressure ("
        "id INTEGER PRIMARY KEY,"
        "${BloodPressureItem.dbName} TEXT,"
        "${BloodPressureItem.dbHighSys} TEXT,"
        "${BloodPressureItem.dbLowSys} TEXT,"
        "${BloodPressureItem.dbPulse} TEXT,"
        "${BloodPressureItem.dbDate} TEXT"
        ")");

    await db.execute("CREATE TABLE SugarLevel ("
        "id INTEGER PRIMARY KEY,"
        "${SugarLevelItem.dbName} TEXT,"
        "${SugarLevelItem.dbSugarType} TEXT,"
        "${SugarLevelItem.dbUnit} TEXT,"
        "${SugarLevelItem.dbRange} TEXT,"
        "${SugarLevelItem.dbSugarLevel} REAL,"
        "${SugarLevelItem.dbDate} TEXT"
        ")");

    await db.execute("CREATE TABLE User ("
        "id INTEGER PRIMARY KEY,"
        "${UserAccessToken.dbAccesToken} TEXT,"
        "${UserAccessToken.dbFullName} TEXT,"
        "${UserAccessToken.dbFhnId} TEXT,"
        "${UserAccessToken.dbImage} TEXT"
        ")");

    await db.execute("CREATE TABLE AdmissionHistory ("
        "${AdmissionHistoryItem.dbRecordId} INTEGER,"
        "${AdmissionHistoryItem.dbAdmissionDate} TEXT,"
        "${AdmissionHistoryItem.dbIndexNumber} TEXT,"
        "${AdmissionHistoryItem.dbIpNumber} TEXT,"
        "${AdmissionHistoryItem.dbWard} TEXT,"
        "${AdmissionHistoryItem.dbHospital} TEXT"
        ")");

    await db.execute("CREATE TABLE CheckupHistory ("
        "${CheckupHistoryItem.dbRecordId} INTEGER,"
        "${CheckupHistoryItem.dbCheckupDate} TEXT,"
        "${CheckupHistoryItem.dbFollowupDate} TEXT,"
        "${CheckupHistoryItem.dbIndexNumber} TEXT,"
        "${CheckupHistoryItem.dbDepartment} TEXT,"
        "${CheckupHistoryItem.dbHospital} TEXT,"
        "${CheckupHistoryItem.dbDoctor} TEXT"
        ")");

    await db.execute("CREATE TABLE DischargeSummary ("
        "${DischargeItem.dbId} INTEGER,"
        "${DischargeItem.dbAdmissionDate} TEXT,"
        "${DischargeItem.dbIndexNumber} TEXT,"
        "${DischargeItem.dbIpNumber} TEXT,"
        "${DischargeItem.dbWard} TEXT,"
        "${DischargeItem.dbPatientName} TEXT,"
        "${DischargeItem.dbAge} TEXT,"
        "${DischargeItem.dbHospital} TEXT"
        ")");

    await db.execute("CREATE TABLE AdmissionDetailA ("
        "${AdmissionDetailItem.dbRecordId} INTEGER,"
        "${AdmissionDetailItem.dbAdmissionDate} TEXT,"
        "${AdmissionDetailItem.dbHospital} TEXT,"
        "${AdmissionDetailItem.dbIndexNumber} TEXT,"
        "${AdmissionDetailItem.dbIpNumber} TEXT,"
        "${AdmissionDetailItem.dbWard} TEXT,"
        "${AdmissionDetailItem.dbChiefComplain} TEXT,"
        "${AdmissionDetailItem.dbHistory} TEXT,"
        "${AdmissionDetailItem.dbVitals} TEXT,"
        "${AdmissionDetailItem.dbGeneralExamination} TEXT,"
        "${AdmissionDetailItem.dbInvestigation} TEXT,"
        "${AdmissionDetailItem.dbProvisionalDiagnosis} TEXT,"
        "${AdmissionDetailItem.dbPlan} TEXT,"
        "${AdmissionDetailItem.dbNote} TEXT"
        ")");

    await db.execute("CREATE TABLE AdmissionDetailC ("
        "${AdmissionDetailItemC.dbRecordId} INTEGER,"
        "${AdmissionDetailItemC.dbIndexNumber} TEXT,"
        "${AdmissionDetailItemC.dbFollowUpNote} TEXT,"
        "${AdmissionDetailItemC.dbHospital} TEXT,"
        "${AdmissionDetailItemC.dbDepartment} TEXT,"
        "${AdmissionDetailItemC.dbDoctor} TEXT,"
        "${AdmissionDetailItemC.dbHistory} TEXT,"
        "${AdmissionDetailItemC.dbAdvise} TEXT,"
        "${AdmissionDetailItemC.dbProvisionalDiagnosis} TEXT,"
        "${AdmissionDetailItemC.dbCheckupDate} TEXT,"
        "${AdmissionDetailItemC.dbFollowupDate} TEXT"
        ")");

    await db.execute("CREATE TABLE LabTest ("
        "${LabTestItem.dbRecordId} INTEGER,"
        "${LabTestItem.dbFrom} TEXT,"
        "${LabTestItem.dbType} TEXT,"
        "${LabTestItem.dbInvestigationId} INTEGER,"
        "${LabTestItem.dbTitle} TEXT,"
        "${LabTestItem.dbHospital} TEXT,"
        "${LabTestItem.dbDoctor} TEXT,"
        "${LabTestItem.dbDate} TEXT"
        ")");

    await db.execute("CREATE TABLE FollowUp ("
        "${FollowUpItem.dbRecordId} INTEGER,"
        "${FollowUpItem.dbFrom} TEXT,"
        "${FollowUpItem.dbFollowUpId} INTEGER,"
        "${FollowUpItem.dbFollowUpDate} TEXT,"
        "${FollowUpItem.dbHospital} TEXT,"
        "${FollowUpItem.dbDoctor} TEXT,"
        "${FollowUpItem.dbDepartment} TEXT"
        ")");

    await db.execute("CREATE TABLE InsideMedication ("
        "${InsideMedicationItem.dbRecordId} INTEGER,"
        "${InsideMedicationItem.dbFrom} TEXT,"
        "${InsideMedicationItem.dbMedicationId} INTEGER,"
        "${InsideMedicationItem.dbDrug} TEXT,"
        "${InsideMedicationItem.dbDose} TEXT,"
        "${InsideMedicationItem.dbFrequency} TEXT,"
        "${InsideMedicationItem.dbRoute} TEXT,"
        "${InsideMedicationItem.dbTime} TEXT,"
        "${InsideMedicationItem.dbFromDate} TEXT,"
        "${InsideMedicationItem.dbToDate} TEXT"
        ")");

    await db.execute("CREATE TABLE Video ("
        "${VideoItem.dbRecordId} INTEGER,"
        "${VideoItem.dbFrom} TEXT,"
        "${VideoItem.dbInvestigationId} INTEGER,"
        "${VideoItem.dbInvestigationDate} TEXT,"
        "${VideoItem.dbHospital} TEXT,"
        "${VideoItem.dbName} TEXT,"
        "${VideoItem.dbDescription} TEXT"
        ")");

    await db.execute("CREATE TABLE VideoAttachment ("
        "${VideoAttachment.dbRecordId} INTEGER,"
        "${VideoAttachment.dbInvestigationId} INTEGER,"
        "${VideoAttachment.dbTitle} TEXT,"
        "${VideoAttachment.dbVideoUrl} TEXT,"
        "${VideoAttachment.dbThumb} TEXT"
        ")");

    await db.execute("CREATE TABLE LabTestDetail ("
        "${LabTestDetailItem.dbRecordId} INTEGER,"
        "${LabTestDetailItem.dbFrom} TEXT,"
        "${LabTestDetailItem.dbId} INTEGER,"
        "${LabTestDetailItem.dbTopic} TEXT,"
        "${LabTestDetailItem.dbHospital} TEXT,"
        "${LabTestDetailItem.dbLabTechnician} TEXT,"
        "${LabTestDetailItem.dbLab} TEXT,"
        "${LabTestDetailItem.dbTests} TEXT"
        ")");

    await db.execute("CREATE TABLE ImagingDetail ("
        "${ImagingDetailItem.dbRecordId} INTEGER,"
        "${ImagingDetailItem.dbFrom} TEXT,"
        "${ImagingDetailItem.dbId} INTEGER,"
        "${ImagingDetailItem.dbTopic} TEXT,"
        "${ImagingDetailItem.dbHospital} TEXT,"
        "${ImagingDetailItem.dbLabTechnician} TEXT,"
        "${ImagingDetailItem.dbLab} TEXT,"
        "${ImagingDetailItem.dbImagingType} TEXT"
        ")");

    await db.execute("CREATE TABLE ImageAttachment ("
        "${ImageAttachment.dbRecordId} INTEGER,"
        "${ImageAttachment.dbId} INTEGER,"
        "${ImageAttachment.dbImage} TEXT"
        ")");

    await db.execute("CREATE TABLE FollowUpDetail ("
        "${FollowUpDetailItem.dbRecordId} INTEGER,"
        "${FollowUpDetailItem.dbFrom} TEXT,"
        "${FollowUpDetailItem.dbId} INTEGER,"
        "${FollowUpDetailItem.dbIndexNumber} TEXT,"
        "${FollowUpDetailItem.dbIpNumber} TEXT,"
        "${FollowUpDetailItem.dbHistory} TEXT,"
        "${FollowUpDetailItem.dbHospital} TEXT,"
        "${FollowUpDetailItem.dbDoctor} TEXT,"
        "${FollowUpDetailItem.dbDepartment} TEXT,"
        "${FollowUpDetailItem.dbFollowUpDate} TEXT,"
        "${FollowUpDetailItem.dbNextFollowUp} TEXT,"
        "${FollowUpDetailItem.dbPlan} TEXT,"
        "${FollowUpDetailItem.dbNote} TEXT"
        ")");

    await db.execute("CREATE TABLE MedicationAlert ("
        "${MedicationItem.dbMedicationFrom} TEXT,"
        "${MedicationItem.dbAlertStatus} INTEGER,"
        "${MedicationItem.dbMedicationId} INTEGER,"
        "${MedicationItem.dbDrug} TEXT,"
        "${MedicationItem.dbDose} TEXT,"
        "${MedicationItem.dbFrequency} TEXT,"
        "${MedicationItem.dbRoute} TEXT,"
        "${MedicationItem.dbTime} TEXT,"
        "${MedicationItem.dbFromDate} TEXT,"
        "${MedicationItem.dbToDate} TEXT,"
        "${MedicationItem.dbNote} TEXT"
        ")");

    await db.execute("CREATE TABLE DischargeSummaryDetail ("
        "${DischargeDetailItem.dbId} INTEGER,"
        "${DischargeDetailItem.dbItem} TEXT"
        ")");

  }

  Future close() async => await _db.close();

  //PillReminder
  Future<int> savePillReminder(PillReminderItem pillReminderItem) async {
    var dbClient = await db;
    int res = await dbClient.insert("PillReminder", pillReminderItem.toMap());
    return res;
  }

  Future<int> deletePillReminder(int id) async {
    var dbClient = await db;
    int res =
        await dbClient.delete("PillReminder", where: "id = ?", whereArgs: [id]);
    return res;
  }

  Future<List<PillReminderItem>> getAllPillReminder() async {
    List<PillReminderItem> list = [];

    var dbClient = await db;
    var result = await dbClient.rawQuery('SELECT * FROM PillReminder');
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new PillReminderItem.fromMap(map));
    }
    return list;
  }

  //BloodPressure
  Future<int> saveBloodPressure(BloodPressureItem bloodPressureItem) async {
    var dbClient = await db;
    int res = await dbClient.insert("BloodPressure", bloodPressureItem.toMap());
    return res;
  }

  Future<int> updateBloodPressure(BloodPressureItem bloodPressureItem) async {
    var dbClient = await db;
    int res = await dbClient.update("BloodPressure", bloodPressureItem.toMap(),
        where: "id = ?", whereArgs: [bloodPressureItem.id]);
    return res;
  }

//   Future<int> deletePillReminder(int id) async {
//     var dbClient = await db;
//     int res =
//         await dbClient.delete("BloodPressure", where: "id = ?", whereArgs: [id]);
//     return res;
//   }

  Future<List<BloodPressureItem>> getAllBloodPressure() async {
    List<BloodPressureItem> list = [];

    var dbClient = await db;
    var result = await dbClient.rawQuery('SELECT * FROM BloodPressure');
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new BloodPressureItem.fromMap(map));
    }
    return list;
  }

  //BloodPressure
  Future<int> saveSugarLevel(SugarLevelItem sugarLevelItem) async {
    var dbClient = await db;
    int res = await dbClient.insert("SugarLevel", sugarLevelItem.toMap());
    return res;
  }

  Future<int> updateSugarLevel(SugarLevelItem sugarLevelItem) async {
    var dbClient = await db;
    int res = await dbClient.update("SugarLevel", sugarLevelItem.toMap(),
        where: "id = ?", whereArgs: [sugarLevelItem.id]);
    return res;
  }

//   Future<int> deletePillReminder(int id) async {
//     var dbClient = await db;
//     int res =
//         await dbClient.delete("BloodPressure", where: "id = ?", whereArgs: [id]);
//     return res;
//   }

  Future<List<SugarLevelItem>> getAllSugarLevel() async {
    List<SugarLevelItem> list = [];

    var dbClient = await db;
    var result = await dbClient.rawQuery('SELECT * FROM SugarLevel');
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new SugarLevelItem.fromMap(map));
    }
    return list;
  }

  //UserAccessToken
  Future<int> saveUser(UserAccessToken userAccessToken) async {
    var dbClient = await db;
    int res = await dbClient.insert("User", userAccessToken.toMap());
    return res;
  }

  Future<UserAccessToken> getUserAccessToken() async {
    UserAccessToken userAccessToken;
    var dbClient = await db;
    var result = await dbClient.rawQuery("SELECT * FROM User limit 1");
    if (result.length == 0) return null;

    userAccessToken = new UserAccessToken.fromMap(result.first);
    return userAccessToken;
  }

  Future<int> deleteUserAccessToken(int id) async {
    var dbClient = await db;
    int res = await dbClient.delete("User", where: "id = ?", whereArgs: [id]);
    return res;
  }

//Admission History
  Future<List<AdmissionHistoryItem>> getAdmissionHistory() async {
    List<AdmissionHistoryItem> list = [];

    var dbClient = await db;
    var result = await dbClient.rawQuery('SELECT * FROM AdmissionHistory');
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new AdmissionHistoryItem.fromMap(map));
    }
    return list;
  }

  Future<int> saveAdmissionHistory(
      AdmissionHistoryItem admissionHistoryItem) async {
    var dbClient = await db;
    int res =
        await dbClient.insert("AdmissionHistory", admissionHistoryItem.toMap());
    return res;
  }

  Future<int> deleteAdmissionHistory() async {
    var dbClient = await db;
    int res = await dbClient.delete("AdmissionHistory");
    return res;
  }

  //Checkup History
  Future<List<CheckupHistoryItem>> getCheckupHistory() async {
    List<CheckupHistoryItem> list = [];

    var dbClient = await db;
    var result = await dbClient.rawQuery('SELECT * FROM CheckupHistory');
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new CheckupHistoryItem.fromMap(map));
    }
    return list;
  }

  Future<int> saveCheckupHistory(CheckupHistoryItem checkupHistoryItem) async {
    var dbClient = await db;
    int res =
        await dbClient.insert("CheckupHistory", checkupHistoryItem.toMap());
    return res;
  }

  Future<int> deleteCheckupHistory() async {
    var dbClient = await db;
    int res = await dbClient.delete("CheckupHistory");
    return res;
  }

  //AdmissionDetailA
  Future<AdmissionDetailItem> getAdmissionDetailItemA(int id) async {
    AdmissionDetailItem admissionDetailItem;
    var dbClient = await db;
    var result = await dbClient
        .query("AdmissionDetailA", where: "record_id = ?", whereArgs: [id]);
    if (result.length == 0) return null;

    admissionDetailItem = new AdmissionDetailItem.fromMap(result.first);
    return admissionDetailItem;
  }

  Future<int> saveAdmissionDetailItemA(
      AdmissionDetailItem admissionDetailItem) async {
    var dbClient = await db;
    int res =
        await dbClient.insert("AdmissionDetailA", admissionDetailItem.toMap());
    return res;
  }

  Future<int> deleteAdmissionDetailItemA(int id) async {
    var dbClient = await db;
    int res = await dbClient
        .delete("AdmissionDetailA", where: "record_id = ?", whereArgs: [id]);
    return res;
  }

  Future<int> deleteAllAdmissionDetailItemA() async {
    var dbClient = await db;
    int res = await dbClient.delete("AdmissionDetailA");
    return res;
  }

  //AdmissionDetailC
  Future<AdmissionDetailItemC> getAdmissionDetailItemC(int id) async {
    AdmissionDetailItemC admissionDetailItemC;
    var dbClient = await db;
    var result = await dbClient
        .query("AdmissionDetailC", where: "record_id = ?", whereArgs: [id]);
    if (result.length == 0) return null;

    admissionDetailItemC = new AdmissionDetailItemC.fromMap(result.first);
    return admissionDetailItemC;
  }

  Future<int> saveAdmissionDetailItemC(
      AdmissionDetailItemC admissionDetailItemC) async {
    var dbClient = await db;
    int res =
        await dbClient.insert("AdmissionDetailC", admissionDetailItemC.toMap());
    return res;
  }

  Future<int> deleteAdmissionDetailItemC(int id) async {
    var dbClient = await db;
    int res = await dbClient
        .delete("AdmissionDetailC", where: "record_id = ?", whereArgs: [id]);
    return res;
  }

  Future<int> deleteAllAdmissionDetailItemC() async {
    var dbClient = await db;
    int res = await dbClient.delete("AdmissionDetailC");
    return res;
  }

//Lab Test
  Future<List<LabTestItem>> getLabTest(
      int recordId, String from, String type) async {
    List<LabTestItem> list = [];

    var dbClient = await db;
    var result = await dbClient.query('LabTest',
        where: "record_id = ? AND from_where = ? AND type = ?",
        whereArgs: [recordId, from, type]);
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new LabTestItem.fromMap(map));
    }
    return list;
  }

  Future<int> saveLabTest(LabTestItem labTestItem) async {
    var dbClient = await db;
    int res = await dbClient.insert("LabTest", labTestItem.toMap());
    return res;
  }

  Future<int> deleteLabTest(int recordId, String from, String type) async {
    var dbClient = await db;
    int res = await dbClient.delete("LabTest",
        where: "record_id = ? AND from_where = ? AND type = ?",
        whereArgs: [recordId, from, type]);
    return res;
  }

  Future<int> deleteAllLabTest() async {
    var dbClient = await db;
    int res = await dbClient.delete("LabTest");
    return res;
  }

//FollowUp
  Future<List<FollowUpItem>> getFollowUp(int recordId, String from) async {
    List<FollowUpItem> list = [];

    var dbClient = await db;
    var result = await dbClient.query('FollowUp',
        where: "record_id = ? AND from_where = ?", whereArgs: [recordId, from]);
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new FollowUpItem.fromMap(map));
    }
    return list;
  }

  Future<int> saveFollowUp(FollowUpItem followUpItem) async {
    var dbClient = await db;
    int res = await dbClient.insert("FollowUp", followUpItem.toMap());
    return res;
  }

  Future<int> deleteFollowUp(int recordId, String from) async {
    var dbClient = await db;
    int res = await dbClient.delete("FollowUp",
        where: "record_id = ? AND from_where = ?", whereArgs: [recordId, from]);
    return res;
  }

  Future<int> deleteAllFollowUp() async {
    var dbClient = await db;
    int res = await dbClient.delete("FollowUp");
    return res;
  }

  //InsideMedication
  Future<List<InsideMedicationItem>> getInsideMedication(
      int recordId, String from) async {
    List<InsideMedicationItem> list = [];

    var dbClient = await db;
    var result = await dbClient.query('InsideMedication',
        where: "record_id = ? AND from_where = ?", whereArgs: [recordId, from]);
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new InsideMedicationItem.fromMap(map));
    }
    return list;
  }

  Future<int> saveInsideMedication(
      InsideMedicationItem insideMedicationItem) async {
    var dbClient = await db;
    int res =
        await dbClient.insert("InsideMedication", insideMedicationItem.toMap());
    return res;
  }

  Future<int> deleteInsideMedication(int recordId, String from) async {
    var dbClient = await db;
    int res = await dbClient.delete("InsideMedication",
        where: "record_id = ? AND from_where = ?", whereArgs: [recordId, from]);
    return res;
  }

  Future<int> deleteAllInsideMedication() async {
    var dbClient = await db;
    int res = await dbClient.delete("InsideMedication");
    return res;
  }

//Video
  Future<List<VideoItem>> getVideo(int recordId, String from) async {
    List<VideoItem> list = [];

    var dbClient = await db;
    var result = await dbClient.query('Video',
        where: "record_id = ? AND from_where = ?", whereArgs: [recordId, from]);
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new VideoItem.fromMap(map));
    }
    return list;
  }

  Future<int> saveVideo(VideoItem videoItem) async {
    var dbClient = await db;
    int res = await dbClient.insert("Video", videoItem.toMap());
    return res;
  }

  Future<int> deleteVideo(int recordId, String from) async {
    var dbClient = await db;
    int res = await dbClient.delete("Video",
        where: "record_id = ? AND from_where = ?", whereArgs: [recordId, from]);
    return res;
  }

  Future<int> deleteAllVideo() async {
    var dbClient = await db;
    int res = await dbClient.delete("Video");
    return res;
  }

//VideoAttachment
  Future<List<VideoAttachment>> getVideoAttachment(int investigationId) async {
    List<VideoAttachment> list = [];

    var dbClient = await db;
    var result = await dbClient.query('VideoAttachment',
        where: "investigation_id = ?", whereArgs: [investigationId]);
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new VideoAttachment.fromMap(map));
    }
    return list;
  }

  Future<int> saveVideoAttachment(VideoAttachment videoAttachmentItem) async {
    var dbClient = await db;
    int res =
        await dbClient.insert("VideoAttachment", videoAttachmentItem.toMap());
    return res;
  }

  Future<int> deleteVideoAttachment(int recordId) async {
    var dbClient = await db;
    int res = await dbClient.delete("VideoAttachment",
        where: "record_id = ?", whereArgs: [recordId]);
    return res;
  }

  Future<int> deleteAllVideoAttachment() async {
    var dbClient = await db;
    int res = await dbClient.delete("VideoAttachment");
    return res;
  }

  //LabTestDetail
  Future<LabTestDetailItem> getLabTestDetail(int recordId, int id) async {
    LabTestDetailItem labTestDetailItem;
    var dbClient = await db;
    var result = await dbClient.query("LabTestDetail",
        where: "record_id = ? AND id = ?", whereArgs: [recordId, id]);
    if (result.length == 0) return null;

    labTestDetailItem = new LabTestDetailItem.fromMap(result.first);
    return labTestDetailItem;
  }

  Future<int> saveLabTestDetail(LabTestDetailItem labTestDetailItem) async {
    var dbClient = await db;
    int res = await dbClient.insert("LabTestDetail", labTestDetailItem.toMap());
    return res;
  }

  Future<int> deleteLabTestDetail(int recordId, int id) async {
    var dbClient = await db;
    int res = await dbClient.delete("LabTestDetail",
        where: "record_id = ? AND id = ?", whereArgs: [recordId, id]);
    return res;
  }

  Future<int> deleteAllLabTestDetail() async {
    var dbClient = await db;
    int res = await dbClient.delete("LabTestDetail");
    return res;
  }

  //ImagingDetail
  Future<ImagingDetailItem> getImagingDetail(int recordId, int id) async {
    ImagingDetailItem imagingDetailItem;
    var dbClient = await db;
    var result = await dbClient.query("ImagingDetail",
        where: "record_id = ? AND id = ?", whereArgs: [recordId, id]);
    if (result.length == 0) return null;

    imagingDetailItem = new ImagingDetailItem.fromMap(result.first);
    return imagingDetailItem;
  }

  Future<int> saveImagingDetail(ImagingDetailItem imagingDetailItem) async {
    var dbClient = await db;
    int res = await dbClient.insert("ImagingDetail", imagingDetailItem.toMap());
    return res;
  }

  Future<int> deleteImagingDetail(int recordId, int id) async {
    var dbClient = await db;
    int res = await dbClient.delete("ImagingDetail",
        where: "record_id = ? AND id = ?", whereArgs: [recordId, id]);
    return res;
  }

  Future<int> deleteAllImagingDetail() async {
    var dbClient = await db;
    int res = await dbClient.delete("ImagingDetail");
    return res;
  }

//ImageAttachment
  Future<List<ImageAttachment>> getImageAttachment(int id) async {
    List<ImageAttachment> list = [];

    var dbClient = await db;
    var result = await dbClient
        .query('ImageAttachment', where: "id = ?", whereArgs: [id]);
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new ImageAttachment.fromMap(map));
    }
    return list;
  }

  Future<int> saveImageAttachment(ImageAttachment imageAttachmentItem) async {
    var dbClient = await db;
    int res =
        await dbClient.insert("ImageAttachment", imageAttachmentItem.toMap());
    return res;
  }

  Future<int> deleteImageAttachment(int recordId, int id) async {
    var dbClient = await db;
    int res = await dbClient.delete("ImageAttachment",
        where: "record_id = ? AND id = ?", whereArgs: [recordId, id]);
    return res;
  }

  Future<int> deleteAllImageAttachment() async {
    var dbClient = await db;
    int res = await dbClient.delete("ImageAttachment");
    return res;
  }

  //FollowUpDetail
  Future<FollowUpDetailItem> getFollowUpDetail(int recordId, int id) async {
    FollowUpDetailItem followUpDetailItem;
    var dbClient = await db;
    var result = await dbClient.query("FollowUpDetail",
        where: "record_id = ? AND id = ?", whereArgs: [recordId, id]);
    if (result.length == 0) return null;

    followUpDetailItem = new FollowUpDetailItem.fromMap(result.first);
    return followUpDetailItem;
  }

  Future<int> saveFollowUpDetail(FollowUpDetailItem followUpDetailItem) async {
    var dbClient = await db;
    int res =
        await dbClient.insert("FollowUpDetail", followUpDetailItem.toMap());
    return res;
  }

  Future<int> deleteFollowUpDetail(int recordId, int id) async {
    var dbClient = await db;
    int res = await dbClient.delete("FollowUpDetail",
        where: "record_id = ? AND id = ?", whereArgs: [recordId, id]);
    return res;
  }

  Future<int> deleteAllFollowUpDetail() async {
    var dbClient = await db;
    int res = await dbClient.delete("FollowUpDetail");
    return res;
  }

  //Medication Alert
  Future<List<MedicationItem>> getMedicationItem(String medicationFrom) async {
    List<MedicationItem> list = [];

    var dbClient = await db;
    var result = await dbClient.query("MedicationAlert",
        where: "medication_from = ?", whereArgs: [medicationFrom]);
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new MedicationItem.fromMap(map));
    }
    return list;
  }

  Future<int> saveMedicationItem(MedicationItem medicationItem) async {
    var dbClient = await db;
    int res = await dbClient.insert("MedicationAlert", medicationItem.toMap());
    return res;
  }

  Future<int> deleteMedicationItem() async {
    var dbClient = await db;
    int res = await dbClient.delete("MedicationAlert");
    return res;
  }

  //DischargeSummary
  Future<List<DischargeItem>> getDischargeSummary() async {
    List<DischargeItem> list = [];

    var dbClient = await db;
    var result = await dbClient.rawQuery('SELECT * FROM DischargeSummary');
    if (result.length == 0) return [];

    for (Map<String, dynamic> map in result) {
      list.add(new DischargeItem.fromMap(map));
    }
    return list;
  }

  Future<int> saveDischargeSummary(DischargeItem dischargeItem) async {
    var dbClient = await db;
    int res = await dbClient.insert("DischargeSummary", dischargeItem.toMap());
    return res;
  }

  Future<int> deleteDischargeSummary() async {
    var dbClient = await db;
    int res = await dbClient.delete("DischargeSummary");
    return res;
  }

  //DischargeSummaryDetail
  Future<int> saveDischargeSummaryDetail(DischargeDetailItem item) async {
    var dbClient = await db;
    int res = await dbClient.insert("DischargeSummaryDetail", item.toMap());
    return res;
  }

  Future<DischargeDetailItem> getDischargeSummaryDetail(int id) async {
    DischargeDetailItem dischargeDetailItem;
    var dbClient = await db;
    var result = await dbClient
        .query('DischargeSummaryDetail', where: "id = ?", whereArgs: [id]);
    if (result.length == 0) return null;

    dischargeDetailItem = new DischargeDetailItem.fromMap(result.first);
    return dischargeDetailItem;
  }

  Future<int> deleteDischargeSummaryDetail(int id) async {
    var dbClient = await db;
    int res = await dbClient
        .delete("DischargeSummaryDetail", where: "id = ?", whereArgs: [id]);
    return res;
  }

  Future<int> deleteAllDischargeSummaryDetail() async {
    var dbClient = await db;
    int res = await dbClient.delete("DischargeSummaryDetail");
    return res;
  }
}
