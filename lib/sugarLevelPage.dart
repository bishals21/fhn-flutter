import 'dart:async';

import 'package:fhn_flutter/addSugarLevelPage.dart';
import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/sugarLevelItem.dart';
import 'package:flutter/material.dart';
import 'database.dart';

class SugarLevelPage extends StatefulWidget {
  @override
  _SugarLevelPageState createState() => _SugarLevelPageState();
}

class _SugarLevelPageState extends State<SugarLevelPage> {
  var db = DatabaseHelper();
  List<SugarLevelItem> list = new List();

  Future getListFromDB() async {
    list = await db.getAllSugarLevel();
    this.setState(() {});
  }

  Future updateDB(SugarLevelItem item, int index) async {
    int res = await db.updateSugarLevel(item);
    list.removeAt(index);
    list.insert(index, item);
    this.setState(() {});
  }

  Future saveToDB(SugarLevelItem item) async {
    await db.saveSugarLevel(item);
    list.add(item);
    setState(() {});
  }

  @override
  void initState() {
    getListFromDB();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Column(
        children: <Widget>[
          new GradientAppBarWithBack(
              AppLocalization.of(context).sugarLevelTitle),
          new Expanded(
            child: new Container(
              child: list.isNotEmpty
                  ? new ListView.builder(
                      padding: new EdgeInsets.all(8.0),
                      itemCount: list.length,
                      itemBuilder: (context, index) {
                        final SugarLevelItem item = list[index];

                        return new GestureDetector(
                          onTap: () {
                            _navigateToUpdateSugarLevel(context, index);
                          },
                          child: new Card(
                            elevation: 5.0,
                            child: new Container(
                              padding: new EdgeInsets.all(8.0),
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                    item.name.toUpperCase(),
                                    style: new TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0,
                                    ),
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.only(top: 10.0),
                                  ),
                                  new Table(
                                    children: <TableRow>[
                                      new TableRow(children: <Widget>[
                                        new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Text(
                                            AppLocalization.of(context)
                                                .sugarType,
                                            style: new TextStyle(
                                                //color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15.0),
                                          ),
                                        ),
                                        new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Text(
                                            item.sugarType,
                                            style:
                                                new TextStyle(fontSize: 15.0),
                                          ),
                                        ),
                                      ]),
                                      new TableRow(children: <Widget>[
                                        new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Text(
                                            AppLocalization.of(context)
                                                .sugarUnit,
                                            style: new TextStyle(
                                                //color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15.0),
                                          ),
                                        ),
                                        new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Text(
                                            item.unit,
                                            style:
                                                new TextStyle(fontSize: 15.0),
                                          ),
                                        ),
                                      ]),
                                      new TableRow(children: <Widget>[
                                        new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Text(
                                            AppLocalization.of(context)
                                                .sugarRange,
                                            style: new TextStyle(
                                                //color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15.0),
                                          ),
                                        ),
                                        new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Text(
                                            item.range,
                                            style:
                                                new TextStyle(fontSize: 15.0),
                                          ),
                                        ),
                                      ]),
                                      new TableRow(children: <Widget>[
                                        new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Text(
                                            AppLocalization.of(context)
                                                .sugarLevel,
                                            style: new TextStyle(
                                                //color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15.0),
                                          ),
                                        ),
                                        new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Text(
                                            item.sugarLevel.toString(),
                                            style:
                                                new TextStyle(fontSize: 15.0),
                                          ),
                                        ),
                                      ]),
                                      new TableRow(children: <Widget>[
                                        new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Text(
                                            AppLocalization.of(context)
                                                .sugarLevelDate,
                                            style: new TextStyle(
                                                //color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15.0),
                                          ),
                                        ),
                                        new Padding(
                                          padding: new EdgeInsets.all(4.0),
                                          child: new Text(
                                            item.date,
                                            style:
                                                new TextStyle(fontSize: 15.0),
                                          ),
                                        ),
                                      ]),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    )
                  : new Center(
                      child: new Image.asset(
                        "assets/empty.png",
                        height: 200.0,
                        width: 200.0,
                      ),
                    ),
            ),
          ),
        ],
      ),
      floatingActionButton: new FloatingActionButton(
        child: new Icon(Icons.add),
        backgroundColor: Color(0xFF9E30BF),
        onPressed: () {
          _navigateToAddSugarLevel(context);
        },
      ),
    );
  }

  _navigateToAddSugarLevel(BuildContext context) async {
    final SugarLevelItem result = await Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new AddSugarLevelPage(
          name: "",
          sugarType: AppLocalization.of(context).fastingText,
          unit: "mg/dL",
          range: "",
          sugarLevel: 0.0,
          date: "",
        ),
        fullscreenDialog: true,
      ),
    );

    if (result != null) {
      saveToDB(result);
    }
  }

  _navigateToUpdateSugarLevel(BuildContext context, int index) async {
    SugarLevelItem updateItem = list[index];
    final SugarLevelItem result = await Navigator.push(
      context,
      new MaterialPageRoute(
        builder: (context) => new AddSugarLevelPage(
          name: updateItem.name,
          sugarType: updateItem.sugarType,
          unit: updateItem.unit,
          range: updateItem.range,
          sugarLevel: updateItem.sugarLevel,
          date: updateItem.date,
        ),
        fullscreenDialog: true,
      ),
    );

    if (result != null) {
      SugarLevelItem item = new SugarLevelItem(
          id: updateItem.id,
          name: result.name,
          sugarType: result.sugarType,
          unit: result.unit,
          range: result.range,
          sugarLevel: result.sugarLevel,
          date: updateItem.date);
      updateDB(item, index);
    }
  }
}
