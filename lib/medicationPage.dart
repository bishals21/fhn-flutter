import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/medicationItem.dart';
import 'package:flutter/material.dart';
import 'database.dart';
import 'package:http/http.dart' as http;

class MedicationAlertPage extends StatefulWidget {
  final String accessToken;

  MedicationAlertPage({Key key, @required this.accessToken}) : super(key: key);
  @override
  _MedicationAlertPageState createState() => _MedicationAlertPageState();
}

class _MedicationAlertPageState extends State<MedicationAlertPage> {
  var db = DatabaseHelper();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<MedicationItem> checkupList = new List();
  List<MedicationItem> admissionList = new List();
  List<MedicationItem> surgeryList = new List();
  List<MedicationItem> dischargeList = new List();

  List<String> headerList = ["CHECKUP", "ADMISSION", "SURGERY", "DISCHARGE"];

  Future<Null> _handleRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    List<MedicationItem> checkupListItem = new List();
    List<MedicationItem> admissionListItem = new List();
    List<MedicationItem> surgeryListItem = new List();
    List<MedicationItem> dischargeListItem = new List();

    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get("http://familyhealthnepal.com/api/medication?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"]["record"];
          var checkup = data["checkup"];
          var admission = data["admission"];
          var surgery = data["surgery"];
          var discharge = data["discharge"];
          deleteFromDB();
          if (checkup != null) {
            for (var item in checkup) {
              StringBuffer timeBuilder;
              String timeReady = "";
              timeBuilder = new StringBuffer();
              var timeArray = item["time"];

              for (var timeItem in timeArray) {
                String time = timeItem;
                timeBuilder.write(time + "#");
              }

              if (timeBuilder.toString().length > 0) {
                timeReady = timeBuilder
                    .toString()
                    .substring(0, timeBuilder.toString().lastIndexOf("#"));
              }
              checkupListItem.add(new MedicationItem(
                medicationFrom: item["medication_from"],
                alertStatus: item["alert_status"],
                medicationId: item["medication_id"],
                drug: item["drug"],
                dose: item["dose"],
                frequency: item["frequency"],
                route: item["route"],
                time: timeReady,
                fromDate: item["fromDate"],
                toDate: item["toDate"],
                note: item["note"],
              ));

              saveToDB(new MedicationItem(
                medicationFrom: item["medication_from"],
                alertStatus: item["alert_status"],
                medicationId: item["medication_id"],
                drug: item["drug"],
                dose: item["dose"],
                frequency: item["frequency"],
                route: item["route"],
                time: timeReady,
                fromDate: item["fromDate"],
                toDate: item["toDate"],
                note: item["note"],
              ));
            }
          }

          if (admission != null) {
            for (var item in admission) {
              StringBuffer timeBuilder;
              String timeReady = "";
              timeBuilder = new StringBuffer();
              var timeArray = item["time"];

              for (var timeItem in timeArray) {
                String time = timeItem;
                timeBuilder.write(time + "#");
              }

              if (timeBuilder.toString().length > 0) {
                timeReady = timeBuilder
                    .toString()
                    .substring(0, timeBuilder.toString().lastIndexOf("#"));
              }

              admissionListItem.add(new MedicationItem(
                medicationFrom: item["medication_from"],
                alertStatus: item["alert_status"],
                medicationId: item["medication_id"],
                drug: item["drug"],
                dose: item["dose"],
                frequency: item["frequency"],
                route: item["route"],
                time: timeReady,
                fromDate: item["fromDate"],
                toDate: item["toDate"],
                note: item["note"],
              ));

              saveToDB(new MedicationItem(
                medicationFrom: item["medication_from"],
                alertStatus: item["alert_status"],
                medicationId: item["medication_id"],
                drug: item["drug"],
                dose: item["dose"],
                frequency: item["frequency"],
                route: item["route"],
                time: timeReady,
                fromDate: item["fromDate"],
                toDate: item["toDate"],
                note: item["note"],
              ));
            }
          }

          if (surgery != null) {
            for (var item in surgery) {
              StringBuffer timeBuilder;
              String timeReady = "";
              timeBuilder = new StringBuffer();
              var timeArray = item["time"];

              for (var timeItem in timeArray) {
                String time = timeItem;
                timeBuilder.write(time + "#");
              }

              if (timeBuilder.toString().length > 0) {
                timeReady = timeBuilder
                    .toString()
                    .substring(0, timeBuilder.toString().lastIndexOf("#"));
              }

              surgeryListItem.add(new MedicationItem(
                medicationFrom: item["medication_from"],
                alertStatus: item["alert_status"],
                medicationId: item["medication_id"],
                drug: item["drug"],
                dose: item["dose"],
                frequency: item["frequency"],
                route: item["route"],
                time: timeReady,
                fromDate: item["fromDate"],
                toDate: item["toDate"],
                note: item["note"],
              ));

              saveToDB(new MedicationItem(
                medicationFrom: item["medication_from"],
                alertStatus: item["alert_status"],
                medicationId: item["medication_id"],
                drug: item["drug"],
                dose: item["dose"],
                frequency: item["frequency"],
                route: item["route"],
                time: timeReady,
                fromDate: item["fromDate"],
                toDate: item["toDate"],
                note: item["note"],
              ));
            }
          }

          if (discharge != null) {
            for (var item in discharge) {
              StringBuffer timeBuilder;
              String timeReady = "";
              timeBuilder = new StringBuffer();
              var timeArray = item["time"];

              for (var timeItem in timeArray) {
                String time = timeItem;
                timeBuilder.write(time + "#");
              }

              if (timeBuilder.toString().length > 0) {
                timeReady = timeBuilder
                    .toString()
                    .substring(0, timeBuilder.toString().lastIndexOf("#"));
              }

              dischargeListItem.add(new MedicationItem(
                medicationFrom: item["medication_from"],
                alertStatus: item["alert_status"],
                medicationId: item["medication_id"],
                drug: item["drug"],
                dose: item["dose"],
                frequency: item["frequency"],
                route: item["route"],
                time: timeReady,
                fromDate: item["fromDate"],
                toDate: item["toDate"],
                note: item["note"],
              ));

              saveToDB(new MedicationItem(
                medicationFrom: item["medication_from"],
                alertStatus: item["alert_status"],
                medicationId: item["medication_id"],
                drug: item["drug"],
                dose: item["dose"],
                frequency: item["frequency"],
                route: item["route"],
                time: timeReady,
                fromDate: item["fromDate"],
                toDate: item["toDate"],
                note: item["note"],
              ));
            }
          }

          if (mounted) {
            this.setState(() {
              checkupList = checkupListItem;
              admissionList = admissionListItem;
              surgeryList = surgeryListItem;
              dischargeList = dischargeListItem;
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text("No Internet Connection"),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getFromDB("CHECKUP");
      getFromDB("ADMISSION");
      getFromDB("SURGERY");
      getFromDB("DISCHARGE");
      return completer.future;
    }
  }

  Future getFromDB(String medicationFrom) async {
    if (medicationFrom == "CHECKUP") {
      checkupList = await db.getMedicationItem(medicationFrom);
      this.setState(() {});
    } else if (medicationFrom == "ADMISSION") {
      admissionList = await db.getMedicationItem(medicationFrom);
      this.setState(() {});
    } else if (medicationFrom == "SURGERY") {
      surgeryList = await db.getMedicationItem(medicationFrom);
      this.setState(() {});
    } else if (medicationFrom == "DISCHARGE") {
      dischargeList = await db.getMedicationItem(medicationFrom);
      this.setState(() {});
    }
  }

  Future saveToDB(MedicationItem item) async {
    await db.saveMedicationItem(item);
  }

  Future deleteFromDB() async {
    await db.deleteMedicationItem();
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    _handleRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          new GradientAppBarWithBack("Medication Alert"),
          new Expanded(
            child: new RefreshIndicator(
              onRefresh: _handleRefresh,
              child: new ListView.builder(
                itemBuilder: (context, index) => new ExpansionTile(
                  initiallyExpanded: true,
                  title: new Text(headerList[index]),
                  children: index == 0
                      ? checkupList.map((item) => new Text("Checkup")).toList()
                      : index == 1
                          ? admissionList
                              .map((item) => new Text("Admission"))
                              .toList()
                          : index == 2
                              ? surgeryList.map((item) {
                                  final List<String> timeList =
                                      item.time.split("#");
                                  return new Card(
                                    elevation: 5.0,
                                    child: new Container(
                                      padding: new EdgeInsets.all(8.0),
                                      child: new Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              new RichText(
                                                text: new TextSpan(
                                                    children: <TextSpan>[
                                                      new TextSpan(
                                                        text:
                                                            AppLocalization.of(
                                                                    context)
                                                                .medicationFrom,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.black,
                                                        ),
                                                      ),
                                                      new TextSpan(
                                                        text: item.fromDate,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          color: Colors.black,
                                                        ),
                                                      )
                                                    ]),
                                              ),
                                              new RichText(
                                                text: new TextSpan(
                                                    children: <TextSpan>[
                                                      new TextSpan(
                                                        text:
                                                            AppLocalization.of(
                                                                    context)
                                                                .medicationTo,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.black,
                                                        ),
                                                      ),
                                                      new TextSpan(
                                                        text: item.toDate,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          color: Colors.black,
                                                        ),
                                                      )
                                                    ]),
                                              ),
                                            ],
                                          ),
                                          new Padding(
                                            padding:
                                                new EdgeInsets.only(top: 12.0),
                                          ),
                                          new Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              new Text(
                                                AppLocalization.of(context)
                                                    .medicationTime,
                                                style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black,
                                                ),
                                              ),
                                              new Flexible(
                                                child: new Container(
                                                  padding: new EdgeInsets.only(
                                                      left: 8.0),
                                                  child: new Wrap(
                                                    spacing: 5.0,
                                                    runSpacing: 4.0,
                                                    children: timeList
                                                            .isNotEmpty
                                                        ? timeList
                                                            .map((item) =>
                                                                new Container(
                                                                  padding:
                                                                      new EdgeInsets
                                                                              .all(
                                                                          1.0),
                                                                  decoration:
                                                                      new BoxDecoration(
                                                                    border: new Border
                                                                            .all(
                                                                        color: Colors
                                                                            .blueAccent),
                                                                  ),
                                                                  child:
                                                                      new Text(
                                                                    item,
                                                                    style:
                                                                        new TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600,
                                                                      color: Colors
                                                                          .black,
                                                                    ),
                                                                  ),
                                                                ))
                                                            .toList()
                                                        : new Center(),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          new Padding(
                                            padding:
                                                new EdgeInsets.only(top: 12.0),
                                          ),
                                          new RichText(
                                            text: new TextSpan(children: <
                                                TextSpan>[
                                              new TextSpan(
                                                text:
                                                    AppLocalization.of(context)
                                                        .medicationDrug,
                                                style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black,
                                                ),
                                              ),
                                              new TextSpan(
                                                text: item.drug,
                                                style: new TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  color: Colors.black,
                                                ),
                                              )
                                            ]),
                                          ),
                                          new Padding(
                                            padding:
                                                new EdgeInsets.only(top: 12.0),
                                          ),
                                          new RichText(
                                            text: new TextSpan(children: <
                                                TextSpan>[
                                              new TextSpan(
                                                text:
                                                    AppLocalization.of(context)
                                                        .medicationDose,
                                                style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black,
                                                ),
                                              ),
                                              new TextSpan(
                                                text: item.dose,
                                                style: new TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  color: Colors.black,
                                                ),
                                              )
                                            ]),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                }).toList()
                              : index == 3
                                  ? dischargeList.map((item) {
                                      final List<String> timeList =
                                          item.time.split("#");

                                      return new Card(
                                        elevation: 5.0,
                                        child: new Container(
                                          padding: new EdgeInsets.all(8.0),
                                          child: new Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              new Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: <Widget>[
                                                  new RichText(
                                                    text:
                                                        new TextSpan(children: <
                                                            TextSpan>[
                                                      new TextSpan(
                                                        text:
                                                            AppLocalization.of(
                                                                    context)
                                                                .medicationFrom,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.black,
                                                        ),
                                                      ),
                                                      new TextSpan(
                                                        text: item.fromDate,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          color: Colors.black,
                                                        ),
                                                      )
                                                    ]),
                                                  ),
                                                  new RichText(
                                                    text:
                                                        new TextSpan(children: <
                                                            TextSpan>[
                                                      new TextSpan(
                                                        text:
                                                            AppLocalization.of(
                                                                    context)
                                                                .medicationTo,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.black,
                                                        ),
                                                      ),
                                                      new TextSpan(
                                                        text: item.toDate,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          color: Colors.black,
                                                        ),
                                                      )
                                                    ]),
                                                  ),
                                                ],
                                              ),
                                              new Padding(
                                                padding: new EdgeInsets.only(
                                                    top: 12.0),
                                              ),
                                              new Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  new Text(
                                                    AppLocalization.of(context)
                                                        .medicationTime,
                                                    style: new TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                  new Flexible(
                                                    child: new Container(
                                                      padding:
                                                          new EdgeInsets.only(
                                                              left: 8.0),
                                                      child: timeList.isNotEmpty
                                                          ? Wrap(
                                                              spacing: 5.0,
                                                              runSpacing: 4.0,
                                                              children: timeList
                                                                  .map((item) =>
                                                                      new Container(
                                                                        padding:
                                                                            new EdgeInsets.all(1.0),
                                                                        decoration:
                                                                            new BoxDecoration(border: new Border.all(color: Colors.blueAccent)),
                                                                        child:
                                                                            new Text(
                                                                          item,
                                                                          style:
                                                                              new TextStyle(
                                                                            fontWeight:
                                                                                FontWeight.w600,
                                                                            color:
                                                                                Colors.black,
                                                                          ),
                                                                        ),
                                                                      ))
                                                                  .toList(),
                                                            )
                                                          : new Text(""),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              new Padding(
                                                padding: new EdgeInsets.only(
                                                    top: 12.0),
                                              ),
                                              new RichText(
                                                text: new TextSpan(
                                                    children: <TextSpan>[
                                                      new TextSpan(
                                                        text:
                                                            AppLocalization.of(
                                                                    context)
                                                                .medicationDrug,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.black,
                                                        ),
                                                      ),
                                                      new TextSpan(
                                                        text: item.drug,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          color: Colors.black,
                                                        ),
                                                      )
                                                    ]),
                                              ),
                                              new Padding(
                                                padding: new EdgeInsets.only(
                                                    top: 12.0),
                                              ),
                                              new RichText(
                                                text: new TextSpan(
                                                    children: <TextSpan>[
                                                      new TextSpan(
                                                        text:
                                                            AppLocalization.of(
                                                                    context)
                                                                .medicationDose,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.black,
                                                        ),
                                                      ),
                                                      new TextSpan(
                                                        text: item.dose,
                                                        style: new TextStyle(
                                                          fontWeight:
                                                              FontWeight.w600,
                                                          color: Colors.black,
                                                        ),
                                                      )
                                                    ]),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    }).toList()
                                  : new Center(),
                ),
                itemCount: headerList.length,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
