import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/followUpListPage.dart';
import 'package:fhn_flutter/insideMedicationPage.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/videoListPage.dart';
import 'package:flutter/material.dart';

import 'admissionDetailPage.dart';
import 'imagingPage.dart';
import 'labTestPage.dart';

class PagerPage extends StatefulWidget {
  final String from, accessToken;
  final int recordId;
  PagerPage(
      {Key key,
      @required this.recordId,
      @required this.from,
      @required this.accessToken})
      : super(key: key);

  @override
  _PagerPageState createState() => _PagerPageState();
}

class _PagerPageState extends State<PagerPage> {
  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 6,
      child: new Scaffold(
        body: Column(
          children: <Widget>[
            new GradientAppBarWithBack(widget.from == "admission"
                ? AppLocalization.of(context).admissionHistoryTitle
                : AppLocalization.of(context).checkupHistoryTitle),
            new Container(
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                    colors: [const Color(0xFF9E30BF), const Color(0xFF262BA0)],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 0.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp),
              ),
              child: new TabBar(
                isScrollable: true,
                indicatorColor: Colors.white,
                tabs: <Widget>[
                  new Tab(
                    child: new Text("Admission Detail"),
                  ),
                  new Tab(
                    child: new Text("Lab Test"),
                  ),
                  new Tab(
                    child: new Text("Imaging"),
                  ),
                  new Tab(
                    child: new Text("Videos"),
                  ),
                  new Tab(
                    child: new Text("Follow Up"),
                  ),
                  new Tab(
                    child: new Text("Medication"),
                  ),
                ],
              ),
            ),
            new Expanded(
              child: new TabBarView(
                children: <Widget>[
                  new Scaffold(
                    body: new AdmissionDetailPage(
                      accessToken: widget.accessToken,
                      from: widget.from,
                      recordId: widget.recordId,
                    ),
                  ),
                  new Scaffold(
                    body: new LabTestPage(
                      accessToken: widget.accessToken,
                      from: widget.from,
                      recordId: widget.recordId,
                    ),
                  ),
                  new Scaffold(
                    body: new ImagingPage(
                      accessToken: widget.accessToken,
                      from: widget.from,
                      recordId: widget.recordId,
                    ),
                  ),
                  new Scaffold(
                    body: new VideoListPage(
                      accessToken: widget.accessToken,
                      from: widget.from,
                      recordId: widget.recordId,
                    ),
                  ),
                  new Scaffold(
                    body: new FollowUpListPage(
                      accessToken: widget.accessToken,
                      from: widget.from,
                      recordId: widget.recordId,
                    ),
                  ),
                  new Scaffold(
                    body: new InsideMedicationPage(
                      accessToken: widget.accessToken,
                      from: widget.from,
                      recordId: widget.recordId,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
