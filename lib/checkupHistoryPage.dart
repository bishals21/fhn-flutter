import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/database.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/checkupHistoryItem.dart';
import 'package:fhn_flutter/pagerPage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CheckupHistoryPage extends StatefulWidget {
  final String accessToken;

  CheckupHistoryPage({Key key, @required this.accessToken}) : super(key: key);

  @override
  _CheckupHistoryPageState createState() => _CheckupHistoryPageState();
}

class _CheckupHistoryPageState extends State<CheckupHistoryPage> {
  var db = DatabaseHelper();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<CheckupHistoryItem> list = new List();

  Future<Null> _handleRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    List<CheckupHistoryItem> listOfItem = new List();
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get("http://familyhealthnepal.com/api/checkup?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"]["_embedded"]["items"];
          deleteFromDB();
          for (var item in data) {
            listOfItem.add(new CheckupHistoryItem(
              recordId: item["record_id"],
              checkupDate: item["checkup_date"],
              followupDate: item["followup_date"],
              indexNumber: item["index_number"],
              department: item["Department"],
              hospital: item["hospital"],
              doctor: item["doctor"],
            ));

            saveToDB(new CheckupHistoryItem(
              recordId: item["record_id"],
              checkupDate: item["checkup_date"],
              followupDate: item["followup_date"],
              indexNumber: item["index_number"],
              department: item["Department"],
              hospital: item["hospital"],
              doctor: item["doctor"],
            ));
          }

          if (mounted) {
            this.setState(() {
              list = listOfItem;
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(AppLocalization.of(context).noInternetConnection),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getFromDB();
      return completer.future;
    }
  }

  Future getFromDB() async {
    list = await db.getCheckupHistory();
    this.setState(() {});
  }

  Future saveToDB(CheckupHistoryItem item) async {
    await db.saveCheckupHistory(item);
  }

  Future deleteFromDB() async {
    await db.deleteCheckupHistory();
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    _handleRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: Column(
          children: <Widget>[
            new GradientAppBarWithBack(
                AppLocalization.of(context).checkupHistoryTitle),
            new Expanded(
              child: new RefreshIndicator(
                onRefresh: _handleRefresh,
                child: new ListView.builder(
                  padding: new EdgeInsets.all(8.0),
                  itemCount: list.length,
                  itemBuilder: (BuildContext context, int index) {
                    final CheckupHistoryItem item = list[index];
                    return new GestureDetector(
                      onTap: () =>
                          Navigator.of(context).push(new MaterialPageRoute(
                              builder: (context) => new PagerPage(
                                    from: "checkup",
                                    recordId: item.recordId,
                                    accessToken: widget.accessToken,
                                  ))),
                      child: new Card(
                        elevation: 5.0,
                        child: new Container(
                          padding: new EdgeInsets.all(8.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(
                                item.hospital.toUpperCase(),
                                style: new TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              new Padding(
                                padding: new EdgeInsets.only(top: 4.0),
                              ),
                              new Text(
                                item.department,
                                style: new TextStyle(
                                  color: Colors.black,
                                  fontSize: 15.0,
                                ),
                              ),
                              new Padding(
                                padding: new EdgeInsets.only(top: 4.0),
                                child: new Divider(
                                  height: 10.0,
                                  color: Colors.black,
                                ),
                              ),
                              new RichText(
                                text: new TextSpan(children: <TextSpan>[
                                  new TextSpan(
                                    text:
                                        AppLocalization.of(context).checkupDate,
                                    style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  new TextSpan(
                                    text: item.checkupDate,
                                    style: new TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black,
                                    ),
                                  )
                                ]),
                              ),
                              new Padding(
                                padding: new EdgeInsets.only(top: 4.0),
                              ),
                              new RichText(
                                text: new TextSpan(children: <TextSpan>[
                                  new TextSpan(
                                    text: AppLocalization.of(context)
                                        .checkupFollowupDate,
                                    style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                    ),
                                  ),
                                  new TextSpan(
                                    text: item.followupDate,
                                    style: new TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black,
                                    ),
                                  )
                                ]),
                              ),
                              new Padding(
                                padding: new EdgeInsets.only(top: 8.0),
                              ),
                              new Align(
                                alignment: Alignment.centerRight,
                                child: new Text(
                                  AppLocalization.of(context).showAll,
                                  textAlign: TextAlign.end,
                                  style: new TextStyle(
                                    color: Colors.blueAccent,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ));
  }
}
