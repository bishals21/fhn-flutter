import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/model/dischargeItem.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'database.dart';

class DischargeSummaryDetailPage extends StatefulWidget {
  final String accessToken;
  final int id;

  DischargeSummaryDetailPage(
      {Key key, @required this.accessToken, @required this.id})
      : super(key: key);
  @override
  _DischargeSummaryDetailPageState createState() =>
      _DischargeSummaryDetailPageState();
}

class _DischargeSummaryDetailPageState
    extends State<DischargeSummaryDetailPage> {
  var db = DatabaseHelper();
  DischargeDetailItem dischargeDetailItem;

  Future<Null> fetchData() async {
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          "http://familyhealthnepal.com/api/discharge/" +
              widget.id.toString() +
              "/show?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          String item = responseBody["data"]["_embedded"]["item"];
          deleteFromDB();
          saveToDB(new DischargeDetailItem(id: widget.id, item: item));
          setState(() {
            dischargeDetailItem =
                new DischargeDetailItem(id: widget.id, item: item);
          });
        }
      });
    } else {
      getFromDB();
    }
  }

  Future getFromDB() async {
    dischargeDetailItem = await db.getDischargeSummaryDetail(widget.id);
    if (dischargeDetailItem != null) {
      this.setState(() {});
    } else {
      Navigator.pop(context);
    }
  }

  Future saveToDB(DischargeDetailItem item) async {
    await db.saveDischargeSummaryDetail(item);
  }

  Future deleteFromDB() async {
    await db.deleteDischargeSummaryDetail(widget.id);
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    fetchData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Center(
          child: dischargeDetailItem != null
              ? new RaisedButton(
                  child: const Text('View Discharge Summary'),
                  onPressed: () {
                    Navigator.of(context).push(new MaterialPageRoute(
                        builder: (context) => new WebviewScaffold(
                              url: new Uri.dataFromString(
                                      dischargeDetailItem.item,
                                      mimeType: 'text/html')
                                  .toString(),
                              appBar: new AppBar(
                                title: new Text("Discharge Summary Detail"),
                              ),
                            ),
                        fullscreenDialog: true));
                  },
                )
              : new CircularProgressIndicator()),
    );
  }
}
