import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/model/labTestDetailItem.dart';
import 'package:flutter/material.dart';

import 'database.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class LabTestDetailPage extends StatefulWidget {
  final String title, from, accessToken;
  final int recordId, investigationId;

  LabTestDetailPage(
      {Key key,
      @required this.accessToken,
      @required this.title,
      @required this.from,
      @required this.recordId,
      @required this.investigationId})
      : super(key: key);
  @override
  _LabTestDetailPageState createState() => _LabTestDetailPageState();
}

class _LabTestDetailPageState extends State<LabTestDetailPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var db = DatabaseHelper();

  LabTestDetailItem labTestDetailItem;

  Future<Null> _handleLabTestDetailRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          "http://familyhealthnepal.com/api/" +
              widget.from +
              "/" +
              widget.recordId.toString() +
              "/investigation/lab/" +
              widget.investigationId.toString() +
              "?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);

          var item = responseBody["data"]["_embedded"]["item"];
          deleteFromDB(widget.recordId, widget.investigationId);

          saveToDB(new LabTestDetailItem(
            recordId: widget.recordId,
            from: widget.from,
            id: item["id"],
            topic: item["topic"],
            hospital: item["hospital"],
            labTechnician: item["lab_technician"],
            lab: item["lab"],
            tests: item["tests"],
          ));
          if (mounted) {
            this.setState(() {
              labTestDetailItem = new LabTestDetailItem(
                recordId: widget.recordId,
                from: widget.from,
                id: item["id"],
                topic: item["topic"],
                hospital: item["hospital"],
                labTechnician: item["lab_technician"],
                lab: item["lab"],
                tests: item["tests"],
              );

              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text("No Internet Connection"),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getFromDB();
      return completer.future;
    }
  }

  Future getFromDB() async {
    labTestDetailItem =
        await db.getLabTestDetail(widget.recordId, widget.investigationId);
    this.setState(() {});
  }

  Future saveToDB(LabTestDetailItem item) async {
    await db.saveLabTestDetail(item);
  }

  Future deleteFromDB(int recordId, int id) async {
    await db.deleteLabTestDetail(recordId, id);
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    _handleLabTestDetailRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: _scaffoldKey,
        body: new Column(
          children: <Widget>[
            new GradientAppBarWithBack(widget.title),
            new Expanded(
              child: labTestDetailItem != null
                  ? Container(
                      padding: new EdgeInsets.all(8.0),
                      child: new Column(
                        children: <Widget>[
                          new Card(
                            elevation: 5.0,
                            child: new Container(
                              padding: new EdgeInsets.all(8.0),
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                    labTestDetailItem.topic.toUpperCase(),
                                    style: new TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.only(top: 16.0),
                                  ),
                                  new Table(
                                    children: <TableRow>[
                                      new TableRow(children: <Widget>[
                                        new Text(
                                          "ID:",
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        new Text(
                                          labTestDetailItem.id.toString(),
                                          style: new TextStyle(
                                            color: Colors.black,
                                          ),
                                        )
                                      ]),
                                      new TableRow(children: <Widget>[
                                        new Text(
                                          "Hospital:",
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        new Text(
                                          labTestDetailItem.hospital,
                                          style: new TextStyle(
                                            color: Colors.black,
                                          ),
                                        )
                                      ]),
                                      new TableRow(children: <Widget>[
                                        new Text(
                                          "Lab:",
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        new Text(
                                          labTestDetailItem.lab.toString(),
                                          style: new TextStyle(
                                            color: Colors.black,
                                          ),
                                        )
                                      ]),
                                      new TableRow(children: <Widget>[
                                        new Text(
                                          "Lab Technician:",
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        new Text(
                                          labTestDetailItem.labTechnician
                                              .toString(),
                                          style: new TextStyle(
                                            color: Colors.black,
                                          ),
                                        )
                                      ]),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(top: 24.0),
                          ),
                          new RaisedButton(
                            onPressed: () {
                              Navigator.of(context).push(new MaterialPageRoute(
                                  builder: (context) => new WebviewScaffold(
                                        url: new Uri.dataFromString(
                                                labTestDetailItem.tests,
                                                mimeType: 'text/html')
                                            .toString(),
                                        appBar: new AppBar(
                                          backgroundColor: Color(0xFF9E30BF),
                                          title: new Text("Lab Test Detail"),
                                        ),
                                      ),
                                  fullscreenDialog: true));
                            },
                            child: new Text("View Lab Report"),
                          ),
                        ],
                      ),
                    )
                  : new Center(
                      child: new CircularProgressIndicator(),
                    ),
            ),
          ],
        ));
  }
}
