import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/model/imagingDetailItem.dart';
import 'package:flutter/material.dart';

import 'database.dart';
import 'imageViewerPage.dart';
import 'package:http/http.dart' as http;

class ImagingDetailPage extends StatefulWidget {
  final String title, from, accessToken;
  final int recordId, investigationId;

  ImagingDetailPage(
      {Key key,
      @required this.accessToken,
      @required this.title,
      @required this.from,
      @required this.recordId,
      @required this.investigationId})
      : super(key: key);

  @override
  _ImagingDetailPageState createState() => _ImagingDetailPageState();
}

class _ImagingDetailPageState extends State<ImagingDetailPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var db = DatabaseHelper();

  ImagingDetailItem imagingDetailItem;
  List<ImageAttachment> imageAttachmentList = new List();

  Future<Null> _handleImagingDetailRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    List<ImageAttachment> list = new List();

    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          "http://familyhealthnepal.com/api/" +
              widget.from +
              "/" +
              widget.recordId.toString() +
              "/investigation/imaging/" +
              widget.investigationId.toString() +
              "?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);

          var item = responseBody["data"]["_embedded"]["item"];
          deleteDetailFromDB(widget.recordId, widget.investigationId);
          deleteAttachmentFromDB(widget.recordId, widget.investigationId);

          for (var image in item["attachments"]) {
            list.add(new ImageAttachment(
              recordId: widget.recordId,
              id: item["id"],
              image: image["image"],
            ));

            saveAttachmentToDB(new ImageAttachment(
              recordId: widget.recordId,
              id: item["id"],
              image: image["image"],
            ));
          }

          saveDetailToDB(new ImagingDetailItem(
              recordId: widget.recordId,
              from: widget.from,
              id: item["id"],
              topic: item["topic"],
              hospital: item["hospital"],
              labTechnician: item["lab_technician"],
              lab: item["lab"],
              imagingType: item["imaging_type"]));

          if (mounted) {
            this.setState(() {
              imagingDetailItem = new ImagingDetailItem(
                  recordId: widget.recordId,
                  from: widget.from,
                  id: item["id"],
                  topic: item["topic"],
                  hospital: item["hospital"],
                  labTechnician: item["lab_technician"],
                  lab: item["lab"],
                  imagingType: item["imaging_type"]);

              this.imageAttachmentList = list;

              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text("No Internet Connection"),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getDetailFromDB();
      getAttachmentFromDB();
      return completer.future;
    }
  }

  Future getDetailFromDB() async {
    imagingDetailItem =
        await db.getImagingDetail(widget.recordId, widget.investigationId);
    this.setState(() {});
  }

  Future getAttachmentFromDB() async {
    imageAttachmentList = await db.getImageAttachment(widget.investigationId);
    this.setState(() {});
  }

  Future saveDetailToDB(ImagingDetailItem item) async {
    await db.saveImagingDetail(item);
  }

  Future saveAttachmentToDB(ImageAttachment item) async {
    await db.saveImageAttachment(item);
  }

  Future deleteDetailFromDB(int recordId, int id) async {
    await db.deleteImagingDetail(recordId, id);
  }

  Future deleteAttachmentFromDB(int recordId, int id) async {
    await db.deleteImageAttachment(recordId, id);
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    _handleImagingDetailRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: _scaffoldKey,
        body: Column(
          children: <Widget>[
            new GradientAppBarWithBack(widget.title),
            new Expanded(
              child: imagingDetailItem != null
                  ? new Container(
                      padding: new EdgeInsets.all(8.0),
                      child: new Column(
                        children: <Widget>[
                          new Card(
                            elevation: 5.0,
                            child: new Container(
                              padding: new EdgeInsets.all(8.0),
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Text(
                                    imagingDetailItem.topic.toUpperCase(),
                                    style: new TextStyle(
                                        color: Colors.black,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.only(top: 4.0),
                                  ),
                                  new Divider(
                                    height: 10.0,
                                    color: Colors.black,
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.only(top: 8.0),
                                  ),
                                  new Text(
                                    imagingDetailItem.hospital.toUpperCase(),
                                    style: new TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  new Padding(
                                    padding: new EdgeInsets.only(top: 16.0),
                                  ),
                                  new Align(
                                    alignment: Alignment.centerRight,
                                    child: new Text(
                                      "Lab Technician: " +
                                          imagingDetailItem.labTechnician
                                              .toString(),
                                      textAlign: TextAlign.end,
                                      style: new TextStyle(
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          new Expanded(
                            flex: 1,
                            child: new Card(
                              child: imageAttachmentList != null
                                  ? new GridView.builder(
                                      padding: new EdgeInsets.all(8.0),
                                      gridDelegate:
                                          new SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 3,
                                        crossAxisSpacing: 8.0,
                                        mainAxisSpacing: 8.0,
                                      ),
                                      itemCount: imageAttachmentList.length,
                                      itemBuilder: (context, index) {
                                        return new GestureDetector(
                                          onTap: () => Navigator.of(context)
                                              .push(new MaterialPageRoute(
                                                  builder: (context) =>
                                                      new ImageViewerPage(
                                                        position: index,
                                                        list:
                                                            imageAttachmentList,
                                                      ),
                                                  fullscreenDialog: true)),
                                          child: new Container(
                                            child: new Hero(
                                              tag: "imaging" +
                                                  imageAttachmentList[index]
                                                      .id
                                                      .toString() +
                                                  index.toString(),
                                              child: new Image.network(
                                                imageAttachmentList[index]
                                                    .image,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                        );
                                      },
                                    )
                                  : new Center(),
                            ),
                          )
                        ],
                      ),
                    )
                  : new Center(
                      child: new CircularProgressIndicator(),
                    ),
            ),
          ],
        ));
  }
}
