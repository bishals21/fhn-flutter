import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/labTestItem.dart';
import 'package:flutter/material.dart';

import 'database.dart';
import 'labTestDetailPage.dart';
import 'package:http/http.dart' as http;

class LabTestPage extends StatefulWidget {
  final String from, accessToken;
  final int recordId;

  LabTestPage(
      {Key key,
      @required this.accessToken,
      @required this.from,
      @required this.recordId})
      : super(key: key);

  @override
  _LabTestPageState createState() => _LabTestPageState();
}

class _LabTestPageState extends State<LabTestPage> {
  var db = DatabaseHelper();

  List<LabTestItem> list = new List();

  Future<Null> _handleLabTestRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    List<LabTestItem> listOfItem = new List();
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          "http://familyhealthnepal.com/api/" +
              widget.from +
              "/" +
              widget.recordId.toString() +
              "/investigation/lab?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"]["_embedded"]["items"];
          deleteFromDB(widget.recordId, widget.from);
          for (var item in data) {
            listOfItem.add(new LabTestItem(
                recordId: widget.recordId,
                from: widget.from,
                type: "lab",
                investigationId: item["investigation_id"],
                title: item["title"],
                hospital: item["hospital"],
                doctor: item["doctor"],
                date: item["date"]));

            saveToDB(new LabTestItem(
                recordId: widget.recordId,
                from: widget.from,
                type: "lab",
                investigationId: item["investigation_id"],
                title: item["title"],
                hospital: item["hospital"],
                doctor: item["doctor"],
                date: item["date"]));
          }

          if (mounted) {
            this.setState(() {
              list = listOfItem;
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(AppLocalization.of(context).noInternetConnection),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getFromDB();
      return completer.future;
    }
  }

  Future getFromDB() async {
    list = await db.getLabTest(widget.recordId, widget.from, "lab");
    this.setState(() {});
  }

  Future saveToDB(LabTestItem item) async {
    await db.saveLabTest(item);
  }

  Future deleteFromDB(int recordId, String from) async {
    await db.deleteLabTest(recordId, from, "lab");
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    _handleLabTestRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new RefreshIndicator(
      onRefresh: _handleLabTestRefresh,
      child: new ListView.builder(
          padding: new EdgeInsets.all(8.0),
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            final LabTestItem item = list[index];

            return new GestureDetector(
              onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                builder: (context) => new LabTestDetailPage(
                  accessToken: widget.accessToken,
                  title: item.title,
                  from: widget.from,
                  recordId: widget.recordId,
                  investigationId: item.investigationId,
                ),
              )),
              child: new Card(
                elevation: 5.0,
                child: new Container(
                  padding: new EdgeInsets.all(8.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        item.title,
                        style: new TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 4.0),
                      ),
                      new Divider(
                        height: 10.0,
                        color: Colors.black,
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new Text(
                        item.hospital,
                        style: new TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new Text(
                        AppLocalization.of(context).dateText + " " + item.date,
                        style: new TextStyle(
                          color: Colors.black87,
                          fontSize: 14.0,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new Align(
                        alignment: Alignment.centerRight,
                        child: new Text(
                          AppLocalization.of(context).showAll,
                          textAlign: TextAlign.end,
                          style: new TextStyle(
                            color: Colors.blueAccent,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
