import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/admissionHistoryItem.dart';
import 'package:flutter/material.dart';

import 'database.dart';
import 'package:http/http.dart' as http;

import 'pagerPage.dart';

class AdmissionHistoryPage extends StatefulWidget {
  final String accessToken;

  AdmissionHistoryPage({Key key, @required this.accessToken}) : super(key: key);

  @override
  _AdmissionHistoryPageState createState() => _AdmissionHistoryPageState();
}

class _AdmissionHistoryPageState extends State<AdmissionHistoryPage> {
  var db = DatabaseHelper();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<AdmissionHistoryItem> list = new List();

  Future<Null> _handleRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    List<AdmissionHistoryItem> listOfItem = new List();
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get("http://familyhealthnepal.com/api/admission?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"]["_embedded"]["items"];
          deleteFromDB();
          for (var item in data) {
            listOfItem.add(new AdmissionHistoryItem(
                recordId: item["record_id"],
                admissionDate: item["admission_date"],
                indexNumber: item["index_number"],
                ipNumber: item["ip_number"],
                ward: item["ward"],
                hospital: item["hospital"]));
            saveToDB(new AdmissionHistoryItem(
                recordId: item["record_id"],
                admissionDate: item["admission_date"],
                indexNumber: item["index_number"],
                ipNumber: item["ip_number"],
                ward: item["ward"],
                hospital: item["hospital"]));
          }

          if (mounted) {
            this.setState(() {
              list = listOfItem;
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(AppLocalization.of(context).noInternetConnection),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getFromDB();
      return completer.future;
    }
  }

  Future getFromDB() async {
    list = await db.getAdmissionHistory();
    this.setState(() {});
  }

  Future saveToDB(AdmissionHistoryItem item) async {
    await db.saveAdmissionHistory(item);
  }

  Future deleteFromDB() async {
    await db.deleteAdmissionHistory();
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    _handleRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: new Column(
        children: <Widget>[
          new GradientAppBarWithBack(
              AppLocalization.of(context).admissionHistoryTitle),
          new Expanded(
            child: new RefreshIndicator(
              onRefresh: _handleRefresh,
              child: new ListView.builder(
                padding: new EdgeInsets.all(8.0),
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  final AdmissionHistoryItem item = list[index];
                  return new GestureDetector(
                    onTap: () =>
                        Navigator.of(context).push(new MaterialPageRoute(
                            builder: (context) => new PagerPage(
                                  from: "admission",
                                  recordId: item.recordId,
                                  accessToken: widget.accessToken,
                                ))),
                    child: new Card(
                      elevation: 5.0,
                      child: new Container(
                          padding: new EdgeInsets.all(8.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Padding(
                                padding: new EdgeInsets.all(4.0),
                                child: new RichText(
                                  text: new TextSpan(children: <TextSpan>[
                                    new TextSpan(
                                      text: AppLocalization.of(context)
                                          .admissionDate,
                                      style: new TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    ),
                                    new TextSpan(
                                      text: item.admissionDate,
                                      style: new TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.w600,
                                        color: Colors.black,
                                      ),
                                    )
                                  ]),
                                ),
                              ),
                              new Divider(
                                color: Colors.black,
                                height: 10.0,
                              ),
                              new Table(
                                columnWidths: const <int, TableColumnWidth>{
                                  0: const FixedColumnWidth(100.0),
                                },
                                children: <TableRow>[
                                  new TableRow(children: <Widget>[
                                    new Padding(
                                      padding: new EdgeInsets.all(4.0),
                                      child: new Text(
                                        AppLocalization.of(context).hospital,
                                        style: new TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15.0),
                                      ),
                                    ),
                                    new Padding(
                                      padding: new EdgeInsets.all(4.0),
                                      child: new Text(
                                        item.hospital,
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                  ]),
                                  new TableRow(children: <Widget>[
                                    new Padding(
                                      padding: new EdgeInsets.all(4.0),
                                      child: new Text(
                                        AppLocalization.of(context).ward,
                                        style: new TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15.0),
                                      ),
                                    ),
                                    new Padding(
                                      padding: new EdgeInsets.all(4.0),
                                      child: new Text(
                                        item.ward,
                                        style: new TextStyle(fontSize: 15.0),
                                      ),
                                    ),
                                  ]),
                                  new TableRow(children: <Widget>[
                                    new Padding(
                                      padding: new EdgeInsets.only(top: 8.0),
                                      child: new Text(
                                        "",
                                      ),
                                    ),
                                    new Padding(
                                      padding: new EdgeInsets.only(top: 8.0),
                                      child: new Text(
                                        AppLocalization.of(context).showAll,
                                        textAlign: TextAlign.end,
                                        style: new TextStyle(
                                          color: Colors.blueAccent,
                                        ),
                                      ),
                                    ),
                                  ])
                                ],
                              ),
                            ],
                          )),
                    ),
                  );
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
