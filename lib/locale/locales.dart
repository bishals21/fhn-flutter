import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:async';
import 'package:fhn_flutter/l10n/messages_all.dart';

class AppLocalization {
  static Future<AppLocalization> load(Locale locale) {
    final String name =
        locale.countryCode.isEmpty ? locale.languageCode : locale.toString();

    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;

      return AppLocalization();
    });
  }

  static AppLocalization of(BuildContext context) {
    return Localizations.of<AppLocalization>(context, AppLocalization);
  }

  String get title {
    return Intl.message('Mobile Health Record',
        name: 'title', desc: 'The title of the application');
  }

  String get admissionHistoryTitle {
    return Intl.message('Admission History',
        name: 'admissionHistoryTitle', desc: 'Admission History title');
  }

  String get checkupHistoryTitle {
    return Intl.message('Checkup History',
        name: 'checkupHistoryTitle', desc: 'Checkup History title');
  }

  String get dischargeSummaryTitle {
    return Intl.message('Discharge Summary',
        name: 'dischargeSummaryTitle', desc: 'Discharge Summary title');
  }

  String get bmiTitle {
    return Intl.message('BMI Calculator',
        name: 'bmiTitle', desc: 'Bmi Calculator title');
  }

  String get pillReminderTitle {
    return Intl.message('Pill Reminder',
        name: 'pillReminderTitle', desc: 'Pill Reminder title');
  }

  String get bloodPressureTitle {
    return Intl.message('Blood Pressure',
        name: 'bloodPressureTitle', desc: 'Blood Pressure title');
  }

  String get sugarLevelTitle {
    return Intl.message('Sugar Level',
        name: 'sugarLevelTitle', desc: 'Sugar Level title');
  }

  String get reportTitle {
    return Intl.message('Report', name: 'reportTitle', desc: 'Report title');
  }

  String get aboutTitle {
    return Intl.message('About', name: 'aboutTitle', desc: 'About title');
  }

  String get loginTitle {
    return Intl.message('Login', name: 'loginTitle', desc: 'Login title');
  }

  String get logoutTitle {
    return Intl.message('Logout', name: 'logoutTitle', desc: 'Logout title');
  }

  String get bmiWeight {
    return Intl.message('Weight', name: 'bmiWeight', desc: 'Bmi Weight');
  }

  String get bmiWeightInKg {
    return Intl.message('in Kg*',
        name: 'bmiWeightInKg', desc: 'Bmi Weight in Kg');
  }

  String get bmiHeight {
    return Intl.message('Height', name: 'bmiHeight', desc: 'Bmi Height');
  }

  String get bmiHeightInFt {
    return Intl.message('in ft.in*',
        name: 'bmiHeightInFt', desc: 'Bmi Height in Ft in');
  }

  String get bmiCalculate {
    return Intl.message('Calculate',
        name: 'bmiCalculate', desc: 'Bmi Calculate');
  }

  String get bmiInfo {
    return Intl.message('BMI Info', name: 'bmiInfo', desc: 'Bmi Info');
  }

  String get bmiUnderWeight {
    return Intl.message('Under weight: If BMI is less than 18.4',
        name: 'bmiUnderWeight', desc: 'Bmi Underweight');
  }

  String get bmiNormalWeight {
    return Intl.message('Normal weight: If BMI is in between 18.5 & 24.9',
        name: 'bmiNormalWeight', desc: 'Bmi Normalweight');
  }

  String get bmiOverWeight {
    return Intl.message('Over weight: If BMI is in between 25 & 29',
        name: 'bmiOverWeight', desc: 'Bmi Overweight');
  }

  String get bmiObese {
    return Intl.message('Obese: If BMI is over 30',
        name: 'bmiObese', desc: 'Bmi Obese');
  }

  String get bmiEnterWeight {
    return Intl.message('Please enter weight',
        name: 'bmiEnterWeight', desc: 'Bmi Enter Weight');
  }

  String get bmiEnterHeight {
    return Intl.message('Please enter height',
        name: 'bmiEnterHeight', desc: 'Bmi Enter Height');
  }

  String get setPillReminder {
    return Intl.message('Set Pill Reminder',
        name: 'setPillReminder', desc: 'Set Pill Reminder');
  }

  String get pillFromDate {
    return Intl.message('From', name: 'pillFromDate', desc: 'From date');
  }

  String get pillToDate {
    return Intl.message('To', name: 'pillToDate', desc: 'To date');
  }

  String get pillChooseDate {
    return Intl.message('Select date >',
        name: 'pillChooseDate', desc: 'Please choose date');
  }

  String get pillMedicineName {
    return Intl.message('Medicine',
        name: 'pillMedicineName', desc: 'Medicine Name');
  }

  String get pillMedicineDose {
    return Intl.message('Medicine Dose',
        name: 'pillMedicineDose', desc: 'Medicine Dose');
  }

  String get pillAddTime {
    return Intl.message('Add Time', name: 'pillAddTime', desc: 'Add time');
  }

  String get pillEmptyAddTime {
    return Intl.message('Please add time',
        name: 'pillEmptyAddTime', desc: 'Please add time');
  }

  String get pillEnterMedicine {
    return Intl.message('Please enter medicine name',
        name: 'pillEnterMedicine', desc: 'enter medicine name');
  }

  String get pillSelectFromDate {
    return Intl.message('Please select from date',
        name: 'pillSelectFromDate', desc: 'select from date');
  }

  String get pillSelectToDate {
    return Intl.message('Please select to date',
        name: 'pillSelectToDate', desc: 'select to date');
  }

  String get pillEnterMedicineDose {
    return Intl.message('Please enter medicine dose',
        name: 'pillEnterMedicineDose', desc: 'enter medicine dose');
  }

  String get pillChooseTime {
    return Intl.message('Please choose time',
        name: 'pillChooseTime', desc: 'choose time');
  }

  String get setSugarLevel {
    return Intl.message('Set Sugar Level',
        name: 'setSugarLevel', desc: 'Set Sugar Level');
  }

  String get sugarLevel {
    return Intl.message('SUGAR LEVEL', name: 'sugarLevel', desc: 'SUGAR LEVEL');
  }

  String get sugarType {
    return Intl.message('SUGAR TYPE', name: 'sugarType', desc: 'sugar type');
  }

  String get sugarUnit {
    return Intl.message('UNIT', name: 'sugarUnit', desc: 'sugar unit');
  }

  String get sugarRange {
    return Intl.message('RANGE', name: 'sugarRange', desc: 'sugar range');
  }

  String get sugarLevelDate {
    return Intl.message('DATE',
        name: 'sugarLevelDate', desc: 'sugar level date');
  }

  String get chooseGlucosePlasma {
    return Intl.message('Choose Plasma Glucose Test',
        name: 'chooseGlucosePlasma', desc: 'Choose Plasma Glucose Test');
  }

  String get sugarLevelSelectUnit {
    return Intl.message('Select Unit: ',
        name: 'sugarLevelSelectUnit', desc: 'Sugar level select unit');
  }

  String get fastingText {
    return Intl.message('Fasting (FBS)',
        name: 'fastingText', desc: 'Fasting text');
  }

  String get randomText {
    return Intl.message('Random (RBS)',
        name: 'randomText', desc: 'Random text');
  }

  String get ppbsText {
    return Intl.message('PPBS', name: 'ppbsText', desc: 'PPBS text');
  }

  String get fastingRange1 {
    return Intl.message('70 - 100',
        name: 'fastingRange1', desc: 'Fasting range 1');
  }

  String get fastingRange2 {
    return Intl.message('3.9 - 5.5',
        name: 'fastingRange2', desc: 'Fasting range 2');
  }

  String get randomRange1 {
    return Intl.message('120 mg/dL',
        name: 'randomRange1', desc: 'Random range 1');
  }

  String get randomRange2 {
    return Intl.message('3.5 - 7.8',
        name: 'randomRange2', desc: 'Random range 2');
  }

  String get ppbsRange1 {
    return Intl.message('Less than 140',
        name: 'ppbsRange1', desc: 'ppbs range 1');
  }

  String get ppbsRange2 {
    return Intl.message('3.5 - 7.8', name: 'ppbsRange2', desc: 'ppbs range 2');
  }

  String get sugarLevelText {
    return Intl.message('Sugar Level',
        name: 'sugarLevelText', desc: 'sugar level text');
  }

  String get addSugarLevelText {
    return Intl.message('Add Sugar Level',
        name: 'addSugarLevelText', desc: 'add sugar level text');
  }

  String get nameText {
    return Intl.message('Patient Name', name: 'nameText', desc: 'name text');
  }

  String get enterName {
    return Intl.message('Enter patient name',
        name: 'enterName', desc: 'enter your patient name text');
  }

  String get nameIsRequired {
    return Intl.message('Please enter patient name',
        name: 'nameIsRequired', desc: 'name is required text');
  }

  String get cancelText {
    return Intl.message('Cancel', name: 'cancelText', desc: 'Cancel Text');
  }

  String get okText {
    return Intl.message('Ok', name: 'okText', desc: 'Ok Text');
  }

  String get setText {
    return Intl.message('Set', name: 'setText', desc: 'Set Text');
  }

  String get dateText {
    return Intl.message('Date:', name: 'dateText', desc: 'date Text');
  }

  String get usernameText {
    return Intl.message('Username',
        name: 'usernameText', desc: 'Username Text');
  }

  String get passwordText {
    return Intl.message('Password',
        name: 'passwordText', desc: 'Password Text');
  }

  String get usernameIsRequired {
    return Intl.message('Please enter username',
        name: 'usernameIsRequired', desc: 'Username is required');
  }

  String get passwordIsRequired {
    return Intl.message('Please enter password',
        name: 'passwordIsRequired', desc: 'Password is required');
  }

  String get enterUsername {
    return Intl.message('Enter your username',
        name: 'enterUsername', desc: 'Enter your username');
  }

  String get enterPassword {
    return Intl.message('Enter your password',
        name: 'enterPassword', desc: 'Enter your password');
  }

  String get signingText {
    return Intl.message('By signing in, you agree to the terms and conditions.',
        name: 'signingText', desc: 'Signing text');
  }

  String get noInternetConnection {
    return Intl.message('No Internet Connection',
        name: 'noInternetConnection', desc: 'No Internet Connection text');
  }

  String get showAll {
    return Intl.message('SHOW ALL', name: 'showAll', desc: 'Show All text');
  }

  String get admissionDate {
    return Intl.message('ADMISSION DATE: ',
        name: 'admissionDate', desc: 'Admission History text');
  }

  String get hospital {
    return Intl.message('HOSPITAL', name: 'hospital', desc: 'Hospital text');
  }

  String get ward {
    return Intl.message('WARD', name: 'ward', desc: 'Ward text');
  }

  String get followUpDate {
    return Intl.message('FOLLOWUP DATE',
        name: 'followUpDate', desc: 'Followup date text');
  }

  String get doctor {
    return Intl.message('DOCTOR', name: 'doctor', desc: 'doctor text');
  }

  String get department {
    return Intl.message('DEPARTMENT',
        name: 'department', desc: 'department text');
  }

  String get medicationFrom {
    return Intl.message('FROM: ',
        name: 'medicationFrom', desc: 'Medication from text');
  }

  String get medicationTo {
    return Intl.message('TO: ',
        name: 'medicationTo', desc: 'Medication to text');
  }

  String get medicationTime {
    return Intl.message('TIME: ',
        name: 'medicationTime', desc: 'Medication time text');
  }

  String get medicationDrug {
    return Intl.message('DRUG: ',
        name: 'medicationDrug', desc: 'Medication drug text');
  }

  String get medicationDose {
    return Intl.message('DOSE: ',
        name: 'medicationDose', desc: 'Medication dose text');
  }

  String get checkupDate {
    return Intl.message('CHECKUP DATE: ',
        name: 'checkupDate', desc: 'Checkup date text');
  }

  String get checkupFollowupDate {
    return Intl.message('FOLLOWUP DATE: ',
        name: 'checkupFollowupDate', desc: 'Followup date text');
  }

  String get age {
    return Intl.message('AGE: ', name: 'age', desc: 'Age text');
  }

  String get logoutConfirmText {
    return Intl.message('Do you want to logout?',
        name: 'logoutConfirmText', desc: 'Logout confirm text');
  }

  String get uploadReport {
    return Intl.message('Upload Report',
        name: 'uploadReport', desc: 'Upload report text');
  }
}

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalization> {
  const AppLocalizationsDelegate();
  @override
  bool isSupported(Locale locale) {
    return ['en', 'ne'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalization> load(Locale locale) {
    return AppLocalization.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalization> old) {
    return false;
  }
}

class FallbackMaterialLocalisationsDelegate
    extends LocalizationsDelegate<MaterialLocalizations> {
  const FallbackMaterialLocalisationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<MaterialLocalizations> load(Locale locale) =>
      DefaultMaterialLocalizations.load(locale);

  @override
  bool shouldReload(FallbackMaterialLocalisationsDelegate old) => false;
}
