import 'package:flutter/material.dart';

class GradientAppBarWithCross extends StatelessWidget {
  final String title;
  final bool opacity;
  final double barHeight = 66.0;

  GradientAppBarWithCross(this.title, this.opacity);

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    return new Column(
      children: <Widget>[
        new Container(
          //margin: new EdgeInsets.only(left: 10.0),
          padding: new EdgeInsets.only(top: statusBarHeight),
          height: statusBarHeight + barHeight,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new IconButton(
                color: Colors.white,
                icon: new Icon(Icons.close),
                onPressed: () => Navigator.maybePop(context),
              ),
              new Container(
                padding: const EdgeInsets.symmetric(horizontal: 25.0),
                child: new Text(
                  title,
                  style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 20.0),
                ),
              ),
            ],
          ),
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
                colors: opacity
                    ? [const Color(0xB39E30BF), const Color(0xB3262BA0)]
                    : [const Color(0xFF9E30BF), const Color(0xFF262BA0)],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(1.0, 0.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
        ),
      ],
    );
  }
}
