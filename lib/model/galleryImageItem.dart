import 'package:meta/meta.dart';

class ImageItem {
  static final dbJsonString = "json";

  String json;

  ImageItem({
    @required this.json,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbJsonString] = json;
    return map;
  }

  ImageItem.fromMap(Map<String, dynamic> map)
      : this(
          json: map[dbJsonString],
        );
}

class GalleryImageItem {
  GalleryImageItem(this.albumName, this.galleryImages);
  String albumName;
  List<GalleryImages> galleryImages;
}

class GalleryImages {
  GalleryImages(this.id, this.hospital, this.date, this.title, this.image);
  int id;
  String hospital;
  String date;
  String title;
  String image;
}
