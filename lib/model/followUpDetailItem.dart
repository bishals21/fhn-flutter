import 'package:meta/meta.dart';

class FollowUpDetailItem {
  static final dbRecordId = "record_id";
  static final dbId = "id";
  static final dbFrom = "from_where";
  static final dbIndexNumber = "index_number";
  static final dbIpNumber = "ip_number";
  static final dbHistory = "patient_history";
  static final dbHospital = "hospital";
  static final dbDoctor = "doctor";
  static final dbDepartment = "department";
  static final dbFollowUpDate = "followup_date";
  static final dbNextFollowUp = "next_followup";
  static final dbPlan = "plan";
  static final dbNote = "note";

  String from,
      indexNumber,
      ipNumber,
      history,
      hospital,
      doctor,
      department,
      followUpDate,
      nextFollowUp,
      plan,
      note;

  int id, recordId;

  FollowUpDetailItem({
    @required this.recordId,
    @required this.from,
    @required this.id,
    @required this.indexNumber,
    @required this.ipNumber,
    @required this.history,
    @required this.hospital,
    @required this.doctor,
    @required this.department,
    @required this.followUpDate,
    @required this.nextFollowUp,
    @required this.plan,
    @required this.note,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbFrom] = from;
    map[dbId] = id;
    map[dbIndexNumber] = indexNumber;
    map[dbIpNumber] = ipNumber;
    map[dbHistory] = history;
    map[dbHospital] = hospital;
    map[dbDepartment] = department;
    map[dbFollowUpDate] = followUpDate;
    map[dbNextFollowUp] = nextFollowUp;
    map[dbPlan] = plan;
    map[dbNote] = note;

    return map;
  }

  FollowUpDetailItem.fromMap(Map<String, dynamic> map)
      : this(
          recordId: map[dbRecordId],
          from: map[dbFrom],
          id: map[dbId],
          indexNumber: map[dbIndexNumber],
          ipNumber: map[dbIpNumber],
          history: map[dbHistory],
          hospital: map[dbHospital],
          doctor: map[dbDoctor],
          department: map[dbDepartment],
          followUpDate: map[dbFollowUpDate],
          nextFollowUp: map[dbNextFollowUp],
          plan: map[dbPlan],
          note: map[dbNote],
        );
}
