import 'package:meta/meta.dart';

class UserAccessToken {
  static final dbAccesToken = "access_token";
  static final dbFullName = "full_name";
  static final dbFhnId = "fhn_id";
  static final dbImage = "image";

  String accessToken, fullName, fhnId, image;
  int id;

  UserAccessToken({
    this.id,
    @required this.accessToken,
    @required this.fullName,
    @required this.fhnId,
    @required this.image,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbAccesToken] = accessToken;
    map[dbFullName] = fullName;
    map[dbFhnId] = fhnId;
    map[dbImage] = image;
    return map;
  }

  UserAccessToken.fromMap(Map<String, dynamic> map)
      : this(
          id: map["id"],
          accessToken: map[dbAccesToken],
          fullName: map[dbFullName],
          fhnId: map[dbFhnId],
          image: map[dbImage],
        );
}
