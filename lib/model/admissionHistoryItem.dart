import 'package:meta/meta.dart';

class AdmissionHistoryItem {
  static final dbRecordId = "record_id";
  static final dbAdmissionDate = "admission_date";
  static final dbIndexNumber = "index_number";
  static final dbIpNumber = "ip_number";
  static final dbWard = "ward";
  static final dbHospital = "hospital";

  int recordId;
  String admissionDate, indexNumber, ipNumber, ward, hospital;

  AdmissionHistoryItem({
    @required this.recordId,
    @required this.admissionDate,
    @required this.indexNumber,
    @required this.ipNumber,
    @required this.ward,
    @required this.hospital,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbAdmissionDate] = admissionDate;
    map[dbIndexNumber] = indexNumber;
    map[dbIpNumber] = ipNumber;
    map[dbWard] = ward;
    map[dbHospital] = hospital;
    return map;
  }

  AdmissionHistoryItem.fromMap(Map<String, dynamic> map)
      : this(
          recordId: map[dbRecordId],
          admissionDate: map[dbAdmissionDate],
          indexNumber: map[dbIndexNumber],
          ipNumber: map[dbIpNumber],
          ward: map[dbWard],
          hospital: map[dbHospital],
        );
}
