import 'package:meta/meta.dart';

class LabTestItem {
  static final dbRecordId = "record_id";
  static final dbFrom = "from_where";
  static final dbType = "type";
  static final dbInvestigationId = "investigation_id";
  static final dbTitle = "title";
  static final dbHospital = "hospital";
  static final dbDoctor = "doctor";
  static final dbDate = "date";

  int investigationId, recordId;
  String from, type, title, hospital, doctor, date;

  LabTestItem({
    @required this.recordId,
    @required this.from,
    @required this.type,
    @required this.investigationId,
    @required this.title,
    @required this.hospital,
    @required this.doctor,
    @required this.date,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbFrom] = from;
    map[dbType] = type;
    map[dbInvestigationId] = investigationId;
    map[dbTitle] = title;
    map[dbHospital] = hospital;
    map[dbDoctor] = doctor;
    map[dbDate] = date;
    return map;
  }

  LabTestItem.fromMap(Map<String, dynamic> map)
      : this(
          recordId: map[dbRecordId],
          from: map[dbFrom],
          type: map[dbType],
          investigationId: map[dbInvestigationId],
          title: map[dbTitle],
          hospital: map[dbHospital],
          doctor: map[dbDoctor],
          date: map[dbDate],
        );
}
