import 'package:meta/meta.dart';

class CheckupHistoryItem {
  static final dbRecordId = "record_id";
  static final dbCheckupDate = "checkup_date";
  static final dbFollowupDate = "followup_date";
  static final dbIndexNumber = "index_number";
  static final dbDepartment = "department";
  static final dbHospital = "hospital";
  static final dbDoctor = "doctor";

  int recordId;
  String checkupDate, followupDate, indexNumber, department, hospital, doctor;

  CheckupHistoryItem({
    @required this.recordId,
    @required this.checkupDate,
    @required this.followupDate,
    @required this.indexNumber,
    @required this.department,
    @required this.hospital,
    @required this.doctor,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbCheckupDate] = checkupDate;
    map[dbFollowupDate] = followupDate;
    map[dbIndexNumber] = indexNumber;
    map[dbDepartment] = department;
    map[dbHospital] = hospital;
    map[dbDoctor] = doctor;
    return map;
  }

  CheckupHistoryItem.fromMap(Map<String, dynamic> map)
      : this(
          recordId: map[dbRecordId],
          checkupDate: map[dbCheckupDate],
          followupDate: map[dbFollowupDate],
          indexNumber: map[dbIndexNumber],
          department: map[dbDepartment],
          hospital: map[dbHospital],
          doctor: map[dbDoctor],
        );
}
