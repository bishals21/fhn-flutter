import 'package:meta/meta.dart';

class VideoItem {
  static final dbJsonString = "json";

  String json;

  VideoItem({
    @required this.json,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbJsonString] = json;
    return map;
  }

  VideoItem.fromMap(Map<String, dynamic> map)
      : this(
          json: map[dbJsonString],
        );
}

class GalleryVideoItem {
  GalleryVideoItem(this.investigateDate, this.hospital, this.name,
      this.description, this.galleryVideos);
  String investigateDate;
  String hospital;
  String name;
  String description;
  List<GalleryVideo> galleryVideos;
}

class GalleryVideo {
  GalleryVideo(this.video, this.title, this.thumb);
  String video;
  String title;
  String thumb;
}
