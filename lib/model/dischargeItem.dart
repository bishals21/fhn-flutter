import 'package:meta/meta.dart';

class DischargeItem {
  static final dbId = "id";
  static final dbIndexNumber = "index_number";
  static final dbIpNumber = "ip_number";
  static final dbWard = "ward";
  static final dbPatientName = "patient_name";
  static final dbAge = "age";
  static final dbAdmissionDate = "admission_date";
  static final dbHospital = "hospital";

  int id;
  String admissionDate, indexNumber, ipNumber, ward, patientName, age, hospital;

  DischargeItem({
    @required this.id,
    @required this.indexNumber,
    @required this.ipNumber,
    @required this.ward,
    @required this.patientName,
    @required this.age,
    @required this.hospital,
    @required this.admissionDate,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbId] = id;
    map[dbIndexNumber] = indexNumber;
    map[dbIpNumber] = ipNumber;
    map[dbWard] = ward;
    map[dbPatientName] = patientName;
    map[dbAge] = age;
    map[dbAdmissionDate] = admissionDate;
    map[dbHospital] = hospital;
    return map;
  }

  DischargeItem.fromMap(Map<String, dynamic> map)
      : this(
          id: map[dbId],
          indexNumber: map[dbIndexNumber],
          ipNumber: map[dbIpNumber],
          ward: map[dbWard],
          patientName: map[dbPatientName],
          age: map[dbAge],
          admissionDate: map[dbAdmissionDate],
          hospital: map[dbHospital],
        );
}

class DischargeDetailItem {
  static final dbId = "id";
  static final dbItem = "item";

  int id;
  String item;

  DischargeDetailItem({
    @required this.id,
    @required this.item,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();

    map[dbId] = id;
    map[dbItem] = item;
    return map;
  }

  DischargeDetailItem.fromMap(Map<String, dynamic> map)
      : this(id: map[dbId], item: map[dbItem]);
}
