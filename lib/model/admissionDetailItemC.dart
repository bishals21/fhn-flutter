import 'package:meta/meta.dart';

class AdmissionDetailItemC {
  static final dbRecordId = "record_id";
  static final dbIndexNumber = "index_number";
  static final dbFollowUpNote = "followup_note";
  static final dbHospital = "hospital";
  static final dbDepartment = "department";
  static final dbDoctor = "doctor";
  static final dbHistory = "patient_history";
  static final dbAdvise = "advise";
  static final dbProvisionalDiagnosis = "provisional_diagnosis";
  static final dbCheckupDate = "checkup_date";
  static final dbFollowupDate = "followup_date";

  String indexNumber,
      followupNote,
      hospital,
      department,
      history,
      doctor,
      advise,
      provisionalDiagnosis,
      checkupDate,
      followupDate;
  int recordId;

  AdmissionDetailItemC({
    @required this.recordId,
    @required this.indexNumber,
    @required this.followupNote,
    @required this.hospital,
    @required this.department,
    @required this.history,
    @required this.doctor,
    @required this.advise,
    @required this.provisionalDiagnosis,
    @required this.checkupDate,
    @required this.followupDate,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbIndexNumber] = indexNumber;
    map[dbFollowUpNote] = followupNote;
    map[dbHospital] = hospital;
    map[dbDepartment] = department;
    map[dbDoctor] = doctor;
    map[dbHistory] = history;
    map[dbAdvise] = advise;
    map[dbProvisionalDiagnosis] = provisionalDiagnosis;
    map[dbCheckupDate] = checkupDate;
    map[dbFollowupDate] = followupDate;

    return map;
  }

  AdmissionDetailItemC.fromMap(Map<String, dynamic> map)
      : this(
          recordId: map[dbRecordId],
          indexNumber: map[dbIndexNumber],
          followupNote: map[dbFollowUpNote],
          hospital: map[dbHospital],
          department: map[dbDepartment],
          doctor: map[dbDoctor],
          history: map[dbHistory],
          advise: map[dbAdvise],
          provisionalDiagnosis: map[dbProvisionalDiagnosis],
          checkupDate: map[dbCheckupDate],
          followupDate: map[dbFollowupDate],
        );
}
