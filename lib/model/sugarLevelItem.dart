import 'package:meta/meta.dart';

class SugarLevelItem {
  static final dbName = "name";
  static final dbSugarType = "sugar_type";
  static final dbUnit = "unit";
  static final dbRange = "range";
  static final dbSugarLevel = "sugar_level";
  static final dbDate = "date";

  String name, sugarType, unit, range, date;
  int id;
  num sugarLevel;
  SugarLevelItem({
    this.id,
    @required this.name,
    @required this.sugarType,
    @required this.unit,
    @required this.range,
    @required this.sugarLevel,
    @required this.date,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbName] = name;
    map[dbSugarType] = sugarType;
    map[dbUnit] = unit;
    map[dbRange] = range;
    map[dbSugarLevel] = sugarLevel;
    map[dbDate] = date;
    return map;
  }

  SugarLevelItem.fromMap(Map<String, dynamic> map)
      : this(
          id: map["id"],
          name: map[dbName],
          sugarType: map[dbSugarType],
          unit: map[dbUnit],
          range: map[dbRange],
          sugarLevel: map[dbSugarLevel],
          date: map[dbDate],
        );
}
