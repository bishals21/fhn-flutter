import 'package:meta/meta.dart';

class AdmissionDetailItem {
  static final dbRecordId = "record_id";
  static final dbAdmissionDate = "admission_date";
  static final dbHospital = "hospital";
  static final dbIndexNumber = "index_number";
  static final dbIpNumber = "ip_number";
  static final dbWard = "ward";
  static final dbChiefComplain = "chief_complain";
  static final dbHistory = "history";
  static final dbVitals = "vitals";
  static final dbGeneralExamination = "general_examination";
  static final dbInvestigation = "investigation";
  static final dbProvisionalDiagnosis = "provisional_diagnosis";
  static final dbPlan = "plan";
  static final dbNote = "note";

  String admissionDate,
      indexNumber,
      ipNumber,
      ward,
      hospital,
      chiefComplain,
      history,
      vitals,
      generalExamination,
      investigation,
      provisionalDiagnosis,
      plan,
      note;

  int recordId;

  AdmissionDetailItem({
    @required this.recordId,
    @required this.admissionDate,
    @required this.hospital,
    @required this.indexNumber,
    @required this.ipNumber,
    @required this.ward,
    @required this.chiefComplain,
    @required this.history,
    @required this.vitals,
    @required this.generalExamination,
    @required this.investigation,
    @required this.provisionalDiagnosis,
    @required this.plan,
    @required this.note,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbAdmissionDate] = admissionDate;
    map[dbHospital] = hospital;
    map[dbIndexNumber] = indexNumber;
    map[dbIpNumber] = ipNumber;
    map[dbWard] = ward;
    map[dbChiefComplain] = chiefComplain;
    map[dbHistory] = history;
    map[dbVitals] = vitals;
    map[dbGeneralExamination] = generalExamination;
    map[dbInvestigation] = investigation;
    map[dbProvisionalDiagnosis] = provisionalDiagnosis;
    map[dbPlan] = plan;
    map[dbNote] = note;

    return map;
  }

  AdmissionDetailItem.fromMap(Map<String, dynamic> map)
      : this(
          recordId: map[dbRecordId],
          admissionDate: map[dbAdmissionDate],
          hospital: map[dbHospital],
          indexNumber: map[dbIndexNumber],
          ipNumber: map[dbIpNumber],
          ward: map[dbWard],
          chiefComplain: map[dbChiefComplain],
          history: map[dbHistory],
          vitals: map[dbVitals],
          generalExamination: map[dbGeneralExamination],
          investigation: map[dbInvestigation],
          provisionalDiagnosis: map[dbProvisionalDiagnosis],
          plan: map[dbPlan],
          note: map[dbNote],
        );
}
