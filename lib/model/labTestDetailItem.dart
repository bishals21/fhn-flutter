import 'package:meta/meta.dart';

class LabTestDetailItem {
  static final dbRecordId = "record_id";
  static final dbFrom = "from_where";
  static final dbId = "id";
  static final dbTopic = "topic";
  static final dbHospital = "hospital";
  static final dbLabTechnician = "lab_technician";
  static final dbLab = "lab";
  static final dbTests = "tests";

  int id, recordId;
  String from, topic, hospital, labTechnician, lab, tests;

  LabTestDetailItem({
    @required this.recordId,
    @required this.from,
    @required this.id,
    @required this.topic,
    @required this.hospital,
    @required this.labTechnician,
    @required this.lab,
    @required this.tests,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbFrom] = from;
    map[dbId] = id;
    map[dbTopic] = topic;
    map[dbHospital] = hospital;
    map[dbLabTechnician] = labTechnician;
    map[dbLab] = lab;
    map[dbTests] = tests;
    return map;
  }

  LabTestDetailItem.fromMap(Map<String, dynamic> map)
      : this(
          recordId: map[dbRecordId],
          from: map[dbFrom],
          id: map[dbId],
          topic: map[dbTopic],
          hospital: map[dbHospital],
          labTechnician: map[dbLabTechnician],
          lab: map[dbLab],
          tests: map[dbTests],
        );
}
