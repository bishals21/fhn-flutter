import 'package:meta/meta.dart';

class ImagingDetailItem {
  static final dbRecordId = "record_id";
  static final dbFrom = "from_where";
  static final dbId = "id";
  static final dbTopic = "topic";
  static final dbHospital = "hospital";
  static final dbLabTechnician = "lab_technician";
  static final dbLab = "lab";
  static final dbImagingType = "imaging_type";

  int id, recordId;
  String from, topic, hospital, labTechnician, lab, imagingType;

  ImagingDetailItem({
    @required this.recordId,
    @required this.from,
    @required this.id,
    @required this.topic,
    @required this.hospital,
    @required this.labTechnician,
    @required this.lab,
    @required this.imagingType,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbFrom] = from;
    map[dbId] = id;
    map[dbTopic] = topic;
    map[dbHospital] = hospital;
    map[dbLabTechnician] = labTechnician;
    map[dbLab] = lab;
    map[dbImagingType] = imagingType;
    return map;
  }

  ImagingDetailItem.fromMap(Map<String, dynamic> map)
      : this(
          recordId: map[dbRecordId],
          from: map[dbFrom],
          id: map[dbId],
          topic: map[dbTopic],
          hospital: map[dbHospital],
          labTechnician: map[dbLabTechnician],
          lab: map[dbLab],
          imagingType: map[dbImagingType],
        );
}

class ImageAttachment {
  static final dbRecordId = "record_id";
  static final dbId = "id";
  static final dbImage = "image";

  int id, recordId;
  String image;

  ImageAttachment({
    @required this.recordId,
    @required this.id,
    @required this.image,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbId] = id;
    map[dbImage] = image;
    return map;
  }

  ImageAttachment.fromMap(Map<String, dynamic> map)
      : this(recordId: map[dbRecordId], id: map[dbId], image: map[dbImage]);
}
