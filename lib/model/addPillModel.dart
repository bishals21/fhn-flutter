class AddPillItem {
  AddPillItem(this.medicine_name, this.from_date, this.to_date,
      this.medicine_dose, this.timeList);
  String medicine_name;
  String medicine_dose;
  String from_date;
  String to_date;
  List<String> timeList;
}
