import 'package:meta/meta.dart';

class FollowUpItem {
  static final dbRecordId = "record_id";
  static final dbFrom = "from_where";
  static final dbFollowUpId = "followup_id";
  static final dbFollowUpDate = "followup_date";
  static final dbHospital = "hospital";
  static final dbDoctor = "doctor";
  static final dbDepartment = "department";

  int followUpId, recordId;
  String from, followUpDate, doctor, department, hospital;

  FollowUpItem({
    @required this.recordId,
    @required this.from,
    @required this.followUpId,
    @required this.followUpDate,
    @required this.hospital,
    @required this.doctor,
    @required this.department,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbFrom] = from;
    map[dbFollowUpId] = followUpId;
    map[dbFollowUpDate] = followUpDate;
    map[dbHospital] = hospital;
    map[dbDoctor] = doctor;
    map[dbDepartment] = department;
    return map;
  }

  FollowUpItem.fromMap(Map<String, dynamic> map)
      : this(
          recordId: map[dbRecordId],
          from: map[dbFrom],
          followUpId: map[dbFollowUpId],
          followUpDate: map[dbFollowUpDate],
          hospital: map[dbHospital],
          doctor: map[dbDoctor],
          department: map[dbDepartment],
        );
}
