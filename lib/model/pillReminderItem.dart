import 'package:meta/meta.dart';

class PillReminderItem {
  static final dbMedicineName = "medicine_name";
  static final dbFromDate = "from_date";
  static final dbToDate = "to_date";
  static final dbMedicineDose = "medicine_dose";
  static final dbTime = "time";

  String medicineName, fromDate, toDate, medicineDose, time;
  int id;

  PillReminderItem({
    this.id,
    @required this.medicineName,
    @required this.fromDate,
    @required this.toDate,
    @required this.medicineDose,
    @required this.time,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbMedicineName] = medicineName;
    map[dbFromDate] = fromDate;
    map[dbToDate] = toDate;
    map[dbMedicineDose] = medicineDose;
    map[dbTime] = time;
    return map;
  }

  PillReminderItem.fromMap(Map<String, dynamic> map)
      : this(
          id: map["id"],
          medicineName: map[dbMedicineName],
          fromDate: map[dbFromDate],
          toDate: map[dbToDate],
          medicineDose: map[dbMedicineDose],
          time: map[dbTime],
        );
}
