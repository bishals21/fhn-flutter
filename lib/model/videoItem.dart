import 'package:meta/meta.dart';

class VideoItem {
  static final dbRecordId = "record_id";
  static final dbFrom = "from_where";
  static final dbInvestigationId = "investigation_id";
  static final dbInvestigationDate = "investigation_date";
  static final dbHospital = "hospital";
  static final dbName = "name";
  static final dbDescription = "description";

  int investigationId, recordId;
  String from, name, investigationDate, hospital, description;

  VideoItem({
    @required this.recordId,
    @required this.from,
    @required this.investigationId,
    @required this.investigationDate,
    @required this.hospital,
    @required this.name,
    @required this.description,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbFrom] = from;
    map[dbInvestigationId] = investigationId;
    map[dbInvestigationDate] = investigationDate;
    map[dbHospital] = hospital;
    map[dbName] = name;
    map[dbDescription] = description;

    return map;
  }

  VideoItem.fromMap(Map<String, dynamic> map)
      : this(
            recordId: map[dbRecordId],
            from: map[dbFrom],
            investigationId: map[dbInvestigationId],
            investigationDate: map[dbInvestigationDate],
            hospital: map[dbHospital],
            name: map[dbName],
            description: map[dbDescription]);
}

class VideoAttachment {
  static final dbRecordId = "record_id";
  static final dbInvestigationId = "investigation_id";
  static final dbVideoUrl = "video";
  static final dbTitle = "title";
  static final dbThumb = "thumb";

  int investigationId, recordId;
  String title, video, thumb;

  VideoAttachment({
    @required this.recordId,
    @required this.investigationId,
    @required this.title,
    @required this.video,
    @required this.thumb,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbInvestigationId] = investigationId;
    map[dbTitle] = title;
    map[dbVideoUrl] = video;
    map[dbThumb] = thumb;
    return map;
  }

  VideoAttachment.fromMap(Map<String, dynamic> map)
      : this(
            recordId: map[dbRecordId],
            investigationId: map[dbInvestigationId],
            title: map[dbTitle],
            video: map[dbVideoUrl],
            thumb: map[dbThumb]);
}
