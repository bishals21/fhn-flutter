import 'package:meta/meta.dart';

class InsideMedicationItem {
  static final dbRecordId = "record_id";
  static final dbFrom = "from_where";
  static final dbMedicationId = "medication_id";
  static final dbDrug = "drug";
  static final dbDose = "dose";
  static final dbFrequency = "frequency";
  static final dbRoute = "route";
  static final dbTime = "time";
  static final dbFromDate = "fromDate";
  static final dbToDate = "toDate";

  String from, drug, dose, frequency, route, time, fromDate, toDate;
  int medicationId, recordId;

  InsideMedicationItem({
    @required this.recordId,
    @required this.from,
    @required this.medicationId,
    @required this.drug,
    @required this.dose,
    @required this.frequency,
    @required this.route,
    @required this.time,
    @required this.fromDate,
    @required this.toDate,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbRecordId] = recordId;
    map[dbFrom] = from;
    map[dbMedicationId] = medicationId;
    map[dbDrug] = drug;
    map[dbDose] = dose;
    map[dbFrequency] = frequency;
    map[dbRoute] = route;
    map[dbTime] = time;
    map[dbFromDate] = fromDate;
    map[dbToDate] = toDate;
    return map;
  }

  InsideMedicationItem.fromMap(Map<String, dynamic> map)
      : this(
          recordId: map[dbRecordId],
          from: map[dbFrom],
          medicationId: map[dbMedicationId],
          drug: map[dbDrug],
          dose: map[dbDose],
          frequency: map[dbFrequency],
          route: map[dbRoute],
          time: map[dbTime],
          fromDate: map[dbFromDate],
          toDate: map[dbToDate],
        );
}
