import 'package:meta/meta.dart';

class MedicationItem {
  static final dbMedicationFrom = "medication_from";
  static final dbAlertStatus = "alert_status";
  static final dbMedicationId = "medication_id";
  static final dbDrug = "drug";
  static final dbDose = "dose";
  static final dbFrequency = "frequency";
  static final dbRoute = "route";
  static final dbTime = "time";
  static final dbFromDate = "fromDate";
  static final dbToDate = "toDate";
  static final dbNote = "note";

  String drug,
      medicationFrom,
      dose,
      frequency,
      route,
      time,
      fromDate,
      toDate,
      note;
  int medicationId;
  bool alertStatus;

  MedicationItem({
    @required this.medicationFrom,
    @required this.alertStatus,
    @required this.medicationId,
    @required this.drug,
    @required this.dose,
    @required this.frequency,
    @required this.route,
    @required this.time,
    @required this.fromDate,
    @required this.toDate,
    @required this.note,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbMedicationFrom] = medicationFrom;
    map[dbAlertStatus] = alertStatus == true ? 1 : 0;
    map[dbMedicationId] = medicationId;
    map[dbDrug] = drug;
    map[dbDose] = dose;
    map[dbFrequency] = frequency;
    map[dbRoute] = route;
    map[dbTime] = time;
    map[dbFromDate] = fromDate;
    map[dbToDate] = toDate;
    map[dbNote] = note;
    return map;
  }

  MedicationItem.fromMap(Map<String, dynamic> map)
      : this(
          medicationFrom: map[dbMedicationFrom],
          alertStatus: map[dbAlertStatus] == 1,
          medicationId: map[dbMedicationId],
          drug: map[dbDrug],
          dose: map[dbDose],
          frequency: map[dbFrequency],
          route: map[dbRoute],
          time: map[dbTime],
          fromDate: map[dbFromDate],
          toDate: map[dbToDate],
          note: map[dbNote],
        );
}
