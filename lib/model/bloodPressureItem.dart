import 'package:meta/meta.dart';

class BloodPressureItem {
  static final dbName = "name";
  static final dbHighSys = "high_sys";
  static final dbLowSys = "low_sys";
  static final dbPulse = "pulse";
  static final dbDate = "date";

  String name, highSys, lowSys, pulse, date;
  int id;
  BloodPressureItem({
    this.id,
    @required this.name,
    @required this.highSys,
    @required this.lowSys,
    @required this.pulse,
    @required this.date,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map[dbName] = name;
    map[dbHighSys] = highSys;
    map[dbLowSys] = lowSys;
    map[dbPulse] = pulse;
    map[dbDate] = date;
    return map;
  }

  BloodPressureItem.fromMap(Map<String, dynamic> map)
      : this(
          id: map["id"],
          name: map[dbName],
          highSys: map[dbHighSys],
          lowSys: map[dbLowSys],
          pulse: map[dbPulse],
          date: map[dbDate],
        );
}
