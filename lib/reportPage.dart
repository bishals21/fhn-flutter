import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/bloodReportPage.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/sugarLevelReport.dart';
import 'package:flutter/material.dart';

class ReportPage extends StatefulWidget {
  @override
  _ReportPageState createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage> {
  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 2,
      child: new Scaffold(
        body: Column(
          children: <Widget>[
            new GradientAppBarWithBack(AppLocalization.of(context).reportTitle),
            new Container(
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                    colors: [const Color(0xFF9E30BF), const Color(0xFF262BA0)],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 0.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp),
              ),
              child: new TabBar(
                indicatorColor: Colors.white,
                tabs: <Widget>[
                  new Tab(
                    child: new Text(
                        AppLocalization.of(context).bloodPressureTitle),
                  ),
                  new Tab(
                    child:
                        new Text(AppLocalization.of(context).sugarLevelTitle),
                  ),
                ],
              ),
            ),
            new Expanded(
              child: new TabBarView(
                children: <Widget>[
                  new BloodReportPage(),
                  new SugarLevelReport(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
