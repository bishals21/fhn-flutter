import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/model/galleryVideoItem.dart';
import 'package:flutter/material.dart';

class GalleryVideoDetailPage extends StatefulWidget {
  final List<GalleryVideo> videoItem;
  final String name;
  GalleryVideoDetailPage(
      {Key key, @required this.name, @required this.videoItem})
      : super(key: key);
  @override
  _GalleryVideoDetailPageState createState() => _GalleryVideoDetailPageState();
}

class _GalleryVideoDetailPageState extends State<GalleryVideoDetailPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Column(
      children: <Widget>[
        new GradientAppBarWithBack(widget.name),
        new Expanded(
          child: new Container(
            child: new GridView.builder(
              padding: new EdgeInsets.all(8.0),
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                crossAxisSpacing: 8.0,
                mainAxisSpacing: 8.0,
              ),
              itemCount: widget.videoItem.length,
              itemBuilder: (context, index) {
                return new GestureDetector(
                  onTap: () {},
                  child: new Container(
                    color: Colors.grey,
                  ),
                );
              },
            ),
          ),
        ),
      ],
    ));
  }
}
