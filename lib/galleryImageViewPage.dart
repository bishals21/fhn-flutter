import 'package:fhn_flutter/model/galleryImageItem.dart';
import 'package:flutter/material.dart';

class GalleryImageViewerPage extends StatefulWidget {
  final List<GalleryImages> list;
  final int position;

  GalleryImageViewerPage(
      {Key key, @required this.list, @required this.position})
      : super(key: key);

  @override
  _GalleryImageViewerPageState createState() => _GalleryImageViewerPageState();
}

class _GalleryImageViewerPageState extends State<GalleryImageViewerPage> {
  PageController _pageController;

  @override
  void initState() {
    _pageController = new PageController(initialPage: widget.position);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.black,
      appBar: new AppBar(
        backgroundColor: Colors.black,
      ),
      body: new PageView.builder(
        controller: _pageController,
        itemCount: widget.list.length,
        itemBuilder: (context, index) {
          // return new Center(
          //   child: new Hero(
          //       tag: "galleryImage" +
          //           widget.list[index].id.toString() +
          //           index.toString(),
          //       child: new Image.network(widget.list[index].image)),
          // );
          return new Stack(
            children: <Widget>[
              new Positioned(
                top: 0.0,
                right: 0.0,
                bottom: 0.0,
                left: 0.0,
                child: new Hero(
                  tag: "galleryImage" +
                      widget.list[index].id.toString() +
                      index.toString(),
                  child: new Image.network(
                    widget.list[index].image,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              new Positioned(
                bottom: 0.0,
                left: 0.0,
                right: 0.0,
                child: new Container(
                    padding: new EdgeInsets.all(8.0),
                    color: Colors.black54,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          widget.list[index].hospital,
                          style: new TextStyle(
                              color: Colors.white, fontSize: 15.0),
                        ),
                        new Text(
                          widget.list[index].date,
                          style: new TextStyle(
                              color: Colors.white, fontSize: 15.0),
                        ),
                      ],
                    )),
              )
            ],
          );
        },
      ),
    );
  }
}
