import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/appbar/gradientAppBarWithBack.dart';
import 'package:fhn_flutter/dischargeSummaryDetailPage.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/dischargeItem.dart';
import 'package:flutter/material.dart';

import 'database.dart';
import 'package:http/http.dart' as http;

class DischargeSummaryPage extends StatefulWidget {
  final String accessToken;

  DischargeSummaryPage({Key key, @required this.accessToken}) : super(key: key);

  @override
  _DischargeSummaryPageState createState() => _DischargeSummaryPageState();
}

class _DischargeSummaryPageState extends State<DischargeSummaryPage> {
  var db = DatabaseHelper();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<DischargeItem> list = new List();

  String test = "";

  Future<Null> _handleRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    List<DischargeItem> listOfItem = new List();
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get("http://familyhealthnepal.com/api/discharge?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"]["_embedded"]["items"];
          deleteFromDB();
          for (var item in data) {
            listOfItem.add(new DischargeItem(
                id: item["id"],
                indexNumber: item["index_number"],
                ipNumber: item["ip_number"],
                ward: item["ward"],
                patientName: item["patient_name"],
                age: item["age"],
                admissionDate: item["admission_date"],
                hospital: item["hospital"]));
            saveToDB(new DischargeItem(
                id: item["id"],
                indexNumber: item["index_number"],
                ipNumber: item["ip_number"],
                ward: item["ward"],
                patientName: item["patient_name"],
                age: item["age"],
                admissionDate: item["admission_date"],
                hospital: item["hospital"]));
          }

          if (mounted) {
            this.setState(() {
              list = listOfItem;
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
        content: new Text(AppLocalization.of(context).noInternetConnection),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getFromDB();
      return completer.future;
    }
  }

  Future getFromDB() async {
    list = await db.getDischargeSummary();
    this.setState(() {});
  }

  Future saveToDB(DischargeItem item) async {
    await db.saveDischargeSummary(item);
  }

  Future deleteFromDB() async {
    await db.deleteAdmissionHistory();
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    _handleRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          new GradientAppBarWithBack(
              AppLocalization.of(context).dischargeSummaryTitle),
          new Expanded(
            child: new RefreshIndicator(
              onRefresh: _handleRefresh,
              child: new ListView.builder(
                padding: new EdgeInsets.all(8.0),
                itemCount: list.length,
                itemBuilder: (BuildContext context, int index) {
                  final DischargeItem item = list[index];
                  return new GestureDetector(
                    onTap: () => Navigator.of(context).push(
                        new MaterialPageRoute(
                            builder: (context) =>
                                new DischargeSummaryDetailPage(
                                  accessToken: widget.accessToken,
                                  id: item.id,
                                ))),
                    child: new Card(
                      elevation: 5.0,
                      child: new Container(
                          padding: new EdgeInsets.all(8.0),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Text("INDEX NUMBER: " + item.indexNumber),
                                  new Text("IP NUMBER: " + item.ipNumber),
                                ],
                              ),
                              new Padding(
                                padding: new EdgeInsets.only(top: 8.0),
                              ),
                              new Text(
                                item.hospital,
                                style: new TextStyle(
                                  color: Colors.black,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              new Padding(
                                padding:
                                    new EdgeInsets.only(top: 4.0, bottom: 4.0),
                                child: new Divider(
                                  color: Colors.black,
                                  height: 10.0,
                                ),
                              ),
                              new Text(
                                item.patientName.toUpperCase(),
                                style: new TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              new Text(
                                AppLocalization.of(context).age + item.age,
                                style: new TextStyle(fontSize: 13.0),
                              ),
                              new Text(
                                AppLocalization.of(context).admissionDate +
                                    item.admissionDate,
                                style: new TextStyle(fontSize: 13.0),
                              ),
                            ],
                          )),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
