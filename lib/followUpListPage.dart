import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:fhn_flutter/database.dart';
import 'package:fhn_flutter/followUpDetailPage.dart';
import 'package:fhn_flutter/locale/locales.dart';
import 'package:fhn_flutter/model/followUpItem.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class FollowUpListPage extends StatefulWidget {
  final String from, accessToken;
  final int recordId;

  FollowUpListPage(
      {Key key,
      @required this.accessToken,
      @required this.from,
      @required this.recordId})
      : super(key: key);

  @override
  _FollowUpListPageState createState() => _FollowUpListPageState();
}

class _FollowUpListPageState extends State<FollowUpListPage> {
  var db = DatabaseHelper();

  List<FollowUpItem> list = new List();

  Future<Null> _handleRefresh() async {
    final Completer<Null> completer = new Completer<Null>();
    List<FollowUpItem> listOfItem = new List();

    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          "http://familyhealthnepal.com/api/" +
              widget.from +
              "/" +
              widget.recordId.toString() +
              "/followup?_format=json",
          headers: {
            "api-key": "testapikey",
            "access-token": widget.accessToken
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"]["_embedded"]["items"];
          deleteFromDB(widget.recordId, widget.from);
          for (var item in data) {
            listOfItem.add(new FollowUpItem(
                recordId: widget.recordId,
                from: widget.from,
                followUpId: item["followup_id"],
                followUpDate: item["followup_date"],
                hospital: item["hospital"],
                doctor: item["doctor"],
                department: item["department"]));

            saveToDB(new FollowUpItem(
                recordId: widget.recordId,
                from: widget.from,
                followUpId: item["followup_id"],
                followUpDate: item["followup_date"],
                hospital: item["hospital"],
                doctor: item["doctor"],
                department: item["department"]));
          }

          if (mounted) {
            this.setState(() {
              list = listOfItem;
              completer.complete(null);
            });
          }

          return completer.future;
        }
      });
    } else {
      Scaffold.of(context).showSnackBar(new SnackBar(
        content: new Text(AppLocalization.of(context).noInternetConnection),
      ));

      if (mounted) {
        this.setState(() {
          completer.complete(null);
        });
      }

      getFromDB();
      return completer.future;
    }
  }

  Future getFromDB() async {
    list = await db.getFollowUp(widget.recordId, widget.from);
    this.setState(() {});
  }

  Future saveToDB(FollowUpItem item) async {
    await db.saveFollowUp(item);
  }

  Future deleteFromDB(int recordId, String from) async {
    await db.deleteFollowUp(recordId, from);
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        connected = true;
      }
    } on SocketException catch (_) {
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    _handleRefresh();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new RefreshIndicator(
      onRefresh: _handleRefresh,
      child: list.isNotEmpty
          ? ListView.builder(
              padding: new EdgeInsets.all(8.0),
              itemCount: list.length,
              itemBuilder: (BuildContext context, int index) {
                final FollowUpItem item = list[index];

                return new GestureDetector(
                  onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                      builder: (context) => new FollowUpDetailPage(
                            accessToken: widget.accessToken,
                            from: widget.from,
                            recordId: widget.recordId,
                            followUpId: item.followUpId,
                          ),
                      fullscreenDialog: true)),
                  child: new Card(
                    elevation: 5.0,
                    child: new Container(
                      padding: new EdgeInsets.all(8.0),
                      child: new Table(
                        children: <TableRow>[
                          new TableRow(children: <Widget>[
                            new Text(
                              AppLocalization.of(context).followUpDate,
                              style: new TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            new Text(
                              item.followUpDate,
                              style: new TextStyle(
                                color: Colors.black87,
                              ),
                            ),
                          ]),
                          new TableRow(children: <Widget>[
                            new Padding(
                              padding: new EdgeInsets.only(top: 8.0),
                              child: new Text(
                                AppLocalization.of(context).hospital,
                                style: new TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            new Padding(
                              padding: new EdgeInsets.only(top: 8.0),
                              child: new Text(
                                item.hospital,
                                style: new TextStyle(
                                  color: Colors.black87,
                                ),
                              ),
                            ),
                          ]),
                          new TableRow(children: <Widget>[
                            new Padding(
                              padding: new EdgeInsets.only(top: 8.0),
                              child: new Text(
                                AppLocalization.of(context).doctor,
                                style: new TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            new Padding(
                              padding: new EdgeInsets.only(top: 8.0),
                              child: new Text(
                                item.doctor,
                                style: new TextStyle(
                                  color: Colors.black87,
                                ),
                              ),
                            )
                          ]),
                          new TableRow(children: <Widget>[
                            new Padding(
                              padding: new EdgeInsets.only(top: 8.0),
                              child: new Text(
                                AppLocalization.of(context).department,
                                style: new TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            new Padding(
                              padding: new EdgeInsets.only(top: 8.0),
                              child: new Text(
                                item.department,
                                style: new TextStyle(
                                  color: Colors.black87,
                                ),
                              ),
                            ),
                          ]),
                          new TableRow(children: <Widget>[
                            new Padding(
                              padding: new EdgeInsets.only(top: 8.0),
                              child: new Text(
                                "",
                              ),
                            ),
                            new Padding(
                              padding: new EdgeInsets.only(top: 8.0),
                              child: new Text(
                                AppLocalization.of(context).showAll,
                                textAlign: TextAlign.end,
                                style: new TextStyle(
                                  color: Colors.blueAccent,
                                ),
                              ),
                            ),
                          ])
                        ],
                      ),
                    ),
                  ),
                );
              })
          : new Center(),
    );
  }
}
