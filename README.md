# fhn_flutter

A new Flutter project.

Mobile Health Record application
Product of Family Health Nepal.
FHN is a service oriented health organization that aims to provide the quality health care through scheduled health monitoring of the patient, providing assistance to them as per their need and coordinating with the patient as well as the family so as to maintain the optimum health status of the patient.
Family Health Nepal focuses towards educating public about investment on personal health which results in reduction on out of pocket payment system which is widely accepted in our country. This organization provides quality of care by deducting all the medical dilemmas and malpractices that might occur while delivering health services.


BENEFITS

Best treatment in the best hospital from best doctors.
Time saving that reduces the burden of awaiting the date of appointment and availability of doctors.
Reduction in out of pocket payment.
Access to view ones medical record from anywhere at any time.
Free from burden of carrying tremendous past history record for proper diagnosis of disease.

WHAT DO YOU GET FROM FHN

FHN is a hub organization that holds many well established specialized as well as general hospitals that will make the health service easily accessible at an affordable cost.
FHN provides health service package that provides preventive care of early diagnosis of disease and appropriate treatment
FHN operates on cost sharing module that temps to lower the financial burden among general public.
Home care services after discharge is provided till the patient is completely well.
Patient empowerment on quality of health care they deserve.

http://familyhealthnepal.com


## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).
